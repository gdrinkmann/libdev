/**
 * gg CORE
 * global js
 */

// --------- jQuery ready() ---------
$(function() {
    
    // enable Bootstrap tooltip functionality
    // https://getbootstrap.com/docs/4.5/components/tooltips/
    //$('[data-toggle="tooltip"]').tooltip();
    // https://getbootstrap.com/docs/5.3/components/tooltips/
    $('[data-bs-toggle="tooltip"]').tooltip();
    
    // auto height for textarea with class autoheight
    $('textarea.autoheight').on('input keyup', function(e) {
        var offset = this.offsetHeight - this.clientHeight;
        $(this).css('height', this.scrollHeight + offset);
    });
    
    // toggle text visibility of password input build by gg form class
    $('.gg-togglepw').on('click', function(e) {
        var input = $(this).closest('.gg-input').find('input[name]');
        if (input.prop('type') === 'password') {
            input.prop('type', 'text');
        } else {
            input.prop('type', 'password');
        }
        input.focus();
    });
    
    // form eventhandler: validation before submit
    $('form.gg-dovalidate').on('submit', function(e) {
        if (!$(this).hasClass('gg-validatedasvalid')) {
            $(this).find('.gg-input').ggInputValidity(true);
            console.log('gg validation: checking submit');
            e.preventDefault();
            e.stopPropagation();
        } else {
            // test!!!
            console.log('test log: submit');
            e.preventDefault();
            e.stopPropagation();
        }
    });
    
    // input eventhandler: instant validation
    // only if gg input has rules and if form already was globally validated
    $('form.gg-dovalidate .gg-input').find('input[name], textarea[name], select[name]').on('input', function(e) {
        var gginput = $(this).closest('.gg-input');
        if (gginput.data('rules')) {
            var form = $(this).closest('form');
            if (form.hasClass('gg-validated')) {
                form.find('.gg-input').ggInputValidity(false);
            }
        }
    });
    
});

// --------- gg jQuery plugins ---------
(function($) {
    
    // ggItemState:
    // set enabled or disabled state for gg control and attach tooltip
    // itemState = enable, disable, unchanged / tooltipOptions = attach tooltip with given options (esp. title)
    // https://getbootstrap.com/docs/4.5/components/tooltips/#options
    $.fn.ggItemState = function(itemState, tooltipOptions) {
        var base = this;
        var state = (typeof itemState !== 'undefined') ? itemState : null;
        var tooltipSettings = (typeof tooltipOptions !== 'undefined') ? tooltipOptions : null;
        
        return base.filter('input, textarea, select, button, .btn').each(function() {
            if (state === 'enable') {
                $(this).parent('.gg-itemwrap, .gg-tooltip').tooltip('dispose');
                $(this).removeClass('disabled').prop('disabled', false).unwrap('.gg-itemwrap');
                if (tooltipSettings) {
                    $(this).tooltip(tooltipSettings);
                } else {
                    $(this).tooltip('dispose');
                }
                $(this).parent('.input-group').find('.gg-togglepw').removeClass('disabled').prop('disabled', false).css('pointer-events','');
            } else if (state === 'disable') {
                $(this).addClass('disabled').prop('disabled',true);
                if (tooltipSettings) {
                    if ($(this).is('button, .btn')) {
                        $(this).wrap('<span class="d-inline-block gg-itemwrap" tabindex="0"></span>');
                        $(this).parent('.gg-itemwrap').tooltip(tooltipSettings);
                    } else {
                        $(this).parent().addClass('gg-tooltip').tooltip(tooltipSettings);
                    }
                }
                $(this).parent('.input-group').find('.gg-togglepw').addClass('disabled').prop('disabled',true).css('pointer-events','none');
            } else if (state === 'unchanged') {
                if ($(this).parent('.gg-itemwrap, .gg-tooltip').length > 0) {
                    var tooltipElement = $(this).parent('.gg-itemwrap, .gg-tooltip');
                } else {
                    var tooltipElement = $(this);
                }
                if (tooltipSettings) {
                    tooltipElement.tooltip(tooltipSettings);
                } else {
                    tooltipElement.tooltip('dispose');
                }
            }
        });
    };
    
    // ggVal:
    // get value of gg control
    // returns input value(s) or undefined
    // or set value of gg control
    $.fn.ggVal = function(optionalvalue) {
        var item = this.first();
        if (item.is('.gg-input')) {
            if (typeof optionalvalue === 'undefined') {
                var retArr = [];
                var controls = item.find('input[name], textarea[name], select[name]');
                if (controls.is('input[type="file"]')) {
                    $.each(controls[0].files, function(i,fileobj) {
                        var tmpobj = {};
                        tmpobj.name = fileobj.name;
                        tmpobj.size = fileobj.size;
                        retArr.push(tmpobj);
                    });
                    return retArr;
                }
                if (controls.is('input[type="checkbox"], input[type="radio"]')) {
                    var type = controls.attr('type');
                    var name = controls.attr('name');
                    controls.filter('input[type="' + type + '"][name="' + name + '"]').each(function(i) {
                        retArr[i] = $(this).is(':checked') ? $(this).val() : null;
                    });
                    return retArr;
                }
                return controls.val();
            } else {
                // todo set value
            }
        }
    };
    
    // ggName:
    // get the name of gg control
    // returns input name
    $.fn.ggName = function() {
        var controls = this.find('input[name]:enabled, textarea[name]:enabled, select[name]:enabled');
        return controls.attr('name');
    };
    
    // ggInputValidity:
    // checks validity of a set of gg inputs by rules,
    // sets appropriate classes and data attributes
    // and optionally submit form
    // see core/_validate.c.php for JSON format of query data and response
    $.fn.ggInputValidity = function(dosubmit) {
        const RULE_URL = '/core/_validate.html';
        
        var items = this.filter('.gg-input');
        
        var itemsCollect = {};
        items.each(function() {
            var item = $(this);
            var name = item.ggName();
            if (typeof name !== "undefined" && name) {
                var value = item.ggVal();
                var ruleString = item.data('rules');
                var rulesCollect = {};
                if (ruleString) {
                    var rules = ruleString.split(/(?<!#)[|]/); // separator is | but not #|
                    var rulesArray = [];
                    $.each(rules, function(i, rule) {
                        var options = rule.split(/(?<!#)[:]/); // separator is : but not #:
                        var ruleName = options.shift();
                        var optionsCollect = {};
                        $.each(options, function(i, option) {
                            var optionArr = option.split(/(?<!#)[=]/); // separator is = but not #=
                            // replace {{name}} of an input by its value:
                            var optionVal = optionArr[1].replace(/{{(.*?)}}/g, function(match, p1, offset, value) {
                                var replval = items.parents('form').find('[name="' + p1 + '"]').closest('.gg-input').ggVal();
                                return replval;
                            });
                            optionsCollect[optionArr[0]] = optionVal;
                        });
                        rulesArray[i] = [ruleName, optionsCollect];
                    });
                    rulesCollect['value'] = value;
                    rulesCollect['rulesArray'] = rulesArray;
                    
                    itemsCollect[name] = rulesCollect;
                }
            }
        });
        
        $.getJSON(RULE_URL, {rulesjson: JSON.stringify(itemsCollect)}).done(function(response) {
            var itemsAreValid = true;
            var counter = 0;
            var scrollitem = false;
            $.each(response, function(inputName, infoObj) {
                var originitems = $('[name="' + inputName + '"]:enabled');
                var gginput = originitems.closest('.gg-input');
                var ruleRes = infoObj['error'] ? '-ERROR-' : infoObj['ruleresult'];
                
                gginput.find('.invalid-feedback').addClass('gg-hide');
                
                if (infoObj['invalid'] === false) {
                    gginput.find('.is-invalid').removeClass('is-invalid');
                    gginput.find('.form-text').removeClass('gg-hide');
                } else {
                    if (!counter++) {
                        scrollitem = gginput;
                    }
                    originitems.addClass('is-invalid').parent('.custom-control, .input-group').addClass('is-invalid');
                    var invalidfeedb = gginput.find('.invalid-feedback[data-ruleref="'+infoObj['invalid']+'"]');
                    if (invalidfeedb.length > 0) {
                        gginput.find('.form-text').addClass('gg-hide');
                    } else {
                        gginput.find('.form-text').removeClass('gg-hide');
                    }
                    invalidfeedb.removeClass('gg-hide');
                }
                gginput.attr('data-ruleresult', ruleRes);
                
                itemsAreValid = itemsAreValid && infoObj['invalid'] === false;
            });
            
            if (dosubmit) {
                items.parents('form').addClass('gg-validated');
                console.log('gg validation: set form gg-validated');
                if (itemsAreValid) {
                    console.log('gg validation: set form gg-validatedasvalid and submit');
                    items.parents('form').addClass('gg-validatedasvalid').submit();
                } else {
                    if (scrollitem) {
                        var scrollinput = scrollitem.find('[name]').first();
                        $('html, body').animate({scrollTop: (scrollinput.offset().top - 65)}, 'slow');
                        scrollinput.focus();
                    }
                }
            }
        }).fail(function(jqxhr, textStatus, error) {
            console.log('gg validation: request failed: ' + textStatus + ', ' + error);
            console.log('jqxhr: ', jqxhr, jqxhr.responseText);
            
        });
        
        return true;
    };
    
    // ggResetInputValidity:
    // reset ggForm with gg Bootstrap 'fail' classes 
    $.fn.ggResetInputValidity = function() {
        this.removeClass('gg-validated');
        this.find('.is-invalid').removeClass('is-invalid');
        this.find('.form-text').removeClass('gg-hide');
        this.find('.invalid-feedback').addClass('gg-hide');
    };
    
}(jQuery));
