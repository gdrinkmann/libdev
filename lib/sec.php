<?php
namespace gg\lib;
/**
 * ggLib CORE
 * security functions in gg namespace
 * 
 * standard included in index.php
 * 
 * gglib sec functions
 * @author Gerd
 */

/**
 * get loggedin user
 * @return object|null
 */
function getCurrentUser() {
    return $_SESSION['user'] ?? null;
}

/**
 * find validated client IP
 * validates IP4 und IP6
 * server variable REMOTE_ADDR always exists,
 * HTTP_X_FORWARDED_FOR optionally exists
 * 
 * @return string IP or empty
 */
function getClientIP() {
    $ipR = filter_var(trim($_SERVER["REMOTE_ADDR"]), FILTER_VALIDATE_IP);

    if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
        $ipArr = explode(",", $_SERVER["HTTP_X_FORWARDED_FOR"]);
        $ip = trim($ipArr[0]);
        if (filter_var($ip, FILTER_VALIDATE_IP) === false) {
            $ip = $ipR;
        }
    } else {
        $ip = $ipR;
    }
    return ($ip === false) ? "" : $ip;
}

/**
 * find proxy state of HTTP client
 * @return bool
 */
function clientProxyUse() {
    return (isset($_SERVER["HTTP_X_FORWARDED_FOR"]));
}

/**
 * escape excel cells
 * @param string $str
 * @return string
 */
function excelEscape($str) {
    return preg_replace("/^=/", "", trim($str));
}

/**
 * escape title-,style-attributes
 * @param string $str
 * @param string $encode
 * @return string
 */
function attEscape($str, $encode = "UTF-8") {
    if (preg_match("/(<script|<iframe)/", $str)) {
        return '';
    }
    return htmlentities($str, ENT_QUOTES | ENT_HTML401, $encode, false);
}


/**
 * create a token 64 chars
 * @return string
 */
function createSToken() {
    return bin2hex(openssl_random_pseudo_bytes(32));
}

/**
 * saves a new synchronizer (CSRF) token in session
 * 
 */
function setSToken() {
    if (!isset($_SESSION['stoken'])) {
        $_SESSION['stoken'] = createSToken();
    }
}

/**
 * get the sToken
 * @return string
 */
function getSToken() {
    if (!isset($_SESSION['stoken'])) {
        setSToken();
    }
    return preg_replace("/[^0-9a-f]/", "", $_SESSION['stoken']);
}

/**
 * checks the synchronizer (CSRF) token,
 * @return bool
 */
function checkSToken() {
    return (isset($_POST[STOKEN_NAME]) && $_POST[STOKEN_NAME] == getSToken());
}

/**
 * check origin of page
 * @param array $allowedOrigins allowed URLs
 * @return bool|string
 */
function verifyOrigin($allowedOrigins = array()) {
    $allowedOrigins[] = CFG_BASEURI;

    $error = null;
    $parsed_origin_url = false;
    $parsed_referer_url = false;

    $headerArr = [];
    foreach ($_SERVER as $name => $value) {
        if (substr($name, 0, 5) == 'HTTP_') {
            $headerArr[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
        }
    }

    if (isset($headerArr['Origin'])) {
        if (filter_var($headerArr['Origin'], FILTER_VALIDATE_URL) === false) {
            $error = "Origin header has no URL format";
        } else {
            $parsed_origin_url = parse_url($headerArr['Origin']);
        }
    }
    if (isset($headerArr['Referer']) && !$error) {
        if (filter_var($headerArr['Referer'], FILTER_VALIDATE_URL) === false) {
            $error = "Referer header has no URL format";
        } else {
            $parsed_referer_url = parse_url($headerArr['Referer']);
        }
    }

    if ($parsed_referer_url && $parsed_origin_url && !$error) {
        if ($parsed_referer_url['scheme'] == $parsed_origin_url['scheme'] && $parsed_referer_url['host'] == $parsed_origin_url['host']) {
            $compareUrl = $parsed_origin_url;
        } else {
            $error = "Origin and Referer header are different";
        }
    } elseif ($parsed_origin_url) {
        $compareUrl = $parsed_origin_url;
    } elseif ($parsed_referer_url) {
        $compareUrl = $parsed_referer_url;
    } else {
        $error = "no Origin or Referer header found";
    }

    if (!$error) {
        foreach ($allowedOrigins as $au) {
            if (filter_var($au, FILTER_VALIDATE_URL) === false) {
                echo "ATTENTION: gg\verifyOrigin: Allowed origin URL has no URL format";
            } else {
                $parsed_au = parse_url($au);
                if ($compareUrl['scheme'] == $parsed_au['scheme'] && $compareUrl['host'] == $parsed_au['host']) {
                    return true;
                }
            }
        }
    }
    if (!$error) {
        $error = "forbidden Origin";
    }
    return $error;
}

/**
 * CSRF-Check for SOP (same origin policy) pages
 */
function checkCSRF() {
    $msg = [];
    $ret1 = verifyOrigin();
    $msg[] = ($ret1 !== true) ? $ret1 : '';
    
    $ret2 = checkSToken();
    $msg[] = ($ret2 !== true) ? 'CRSF-Token fault' : '';
    
    $message = implode(' and ', $msg);

    if ($ret1 !== true || $ret2 !== true) {
        $logObj = new log(true, false, true);
        $logObj->addSourceInfo();
        $logObj->addUserId();
        $logObj->disableDailyFile();
        $logObj->write('security', 'Same Origin Policy Fault:'."\n".$message."\n");
        
        die('gg: Same Origin Policy Fault! See log');
    }
}

/**
 * javascript snippet for converting links with GET parameter to FORMS with hidden inputs,
 * the sToken is added
 * contains javascript function setGETasPOST(url,target = "_self"),
 * jQuery methode clickToPost() 
 * and $(".makepost").click.. for link handling
 * @return string
 */
function getCSRFJS() {
    $stokenname = STOKEN_NAME;
    $stoken = getSToken();

    $jq = <<<EOJS
	function sendGETasPOST(url,target=false) {
            var ha = url.split('#');
            var hr = ha[0].split('?');
            var ta = target;
            var has = "";
            if (typeof ha[1] !== typeof undefined && ha[1] !== false) has = "#"+ha[1];
            var action = hr[0]+has;
	    var params = hr[1].split('&');
	    params.push("$stokenname=$stoken");
	    var form = $(document.createElement('form')).attr('action', action).attr('method','post');
	    if (typeof ta !== typeof undefined && ta !== false) form.attr('target',ta);
	    $('body').append(form);
	    for (var i in params) {
	        var tmp= params[i].split('=');
	        var key = tmp[0];
                var value = "";
                if (typeof tmp[1] !== typeof undefined  && tmp[1] !== false) value = tmp[1];
	        $(document.createElement('input')).attr('type', 'hidden').attr('name', key).attr('value', value).appendTo(form);
	    }
	    $(form).submit().remove();
	}

	$(function() {
            $.fn.clickToPost = function() {
                $(this).click(function(event) {
                    event.preventDefault();
                    var dis = $(this).attr('disabled');
                    if (typeof dis === typeof undefined || dis === false || dis === "false") {
                        sendGETasPOST($(this).attr('data-posturl'), $(this).attr('target'));
                    }
                }).each(function() {
                    $(this).attr("data-posturl", $(this).attr('href')).attr('href',"#");
                });
            }

            $(".makepost").clickToPost();
	});
EOJS;
    return $jq;
}

/**
 * test $val with $regex
 * @param $val
 * @param $regex
 * @param $trim bool 
 * @param $required bool
 * @return bool
 */
function validateValue($val, $regex, $trim = true, $required = false) {
    if (!isset($val)) {
        return !(bool)$required;
    }
    $t = (string)$val;
    $t = ($trim) ? trim($t) : $t;

    if ($t === "") {
        return !(bool)$required;
    }

    return (bool)preg_match($regex, $t);
}

/**
 * htmlentities with exceptions
 * @param string $str
 * @param string $encode "ISO-8859-1" or "UTF-8"
 * @return bool|string
 */
function htmlEntities($str, $encode = "UTF-8") {
    if (preg_match("/(<script|<iframe)/", $str)) {
        return false;
    }
    $ntab = array(
        "<br>" => uniqid("a"),
        "<br/>" => uniqid("a1"),
        "<br />" => uniqid("a2"),
        "<b>" => uniqid("b"),
        "</b>" => uniqid("c"),
        "<i>" => uniqid("d"),
        "</i>" => uniqid("e"),
        "<h1>" => uniqid("f"),
        "</h1>" => uniqid("g"),
        "<h2>" => uniqid("h"),
        "</h2>" => uniqid("i"),
        "<h3>" => uniqid("j"),
        "</h3>" => uniqid("k"),
        "<h4>" => uniqid("l"),
        "</h4>" => uniqid("m"),
        "<code>" => uniqid("n"),
        "</code>" => uniqid("o"),
        "<pre>" => uniqid("p"),
        "</pre>" => uniqid("q"),
        "<strong>" => uniqid("r"),
        "</strong>" => uniqid("s"),
        "<ul>" => uniqid("t"),
        "</ul>" => uniqid("u"),
        "<li>" => uniqid("v"),
        "</li>" => uniqid("x"),
        "<p>" => uniqid("y"),
        "</p>" => uniqid("z"),
        "</a>" => uniqid("ab"),
        "</span>" => uniqid("bc"),
        "</div>" => uniqid("cd"),
        "<title>" => uniqid("de"),
        "</title>" => uniqid("ef")
    );

    $i = 0;
    // <a href target except
    preg_match_all("#<a\s+href=('[^<>]+?'|\"[^<>]+?\")(|\s+target=('[^<>]+?'|\"[^<>]+?\"))\s*>#", $str, $matches);
    foreach ($matches[0] as $ma) {
        $ntab[$ma] = uniqid((string) $i++);
    }

    // <span style title except
    preg_match_all("#<span(|\s+style=('[^<>]+?'|\"[^<>]+?\"))(|\s+title=('[^<>]+?'|\"[^<>]+?\"))\s*>#", $str, $matches);
    foreach ($matches[0] as $ma) {
        $ntab[$ma] = uniqid((string) $i++);
    }

    // <div style title except
    preg_match_all("#<div(|\s+style=('[^<>]+?'|\"[^<>]+?\"))(|\s+title=('[^<>]+?'|\"[^<>]+?\"))\s*>#", $str, $matches);
    foreach ($matches[0] as $ma) {
        $ntab[$ma] = uniqid((string) $i++);
    }

    $ret = str_ireplace(array_keys($ntab), array_values($ntab), $str);

    $ret = htmlentities($ret, ENT_HTML401, $encode, false);

    $ret = str_replace(array_values($ntab), array_keys($ntab), $ret);

    return $ret;
}