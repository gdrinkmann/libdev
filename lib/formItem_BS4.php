<?php
namespace gg\lib;

/**
 * ggLib CORE
 * class gg\lib\formItem_BS4
 * defines a formItem, usually a html-input
 */
class formItem_BS4 extends formItem {
    
    /**
     * get the formItem HTML (here Bootstrap 4)
     * https://getbootstrap.com/docs/4.5/components/forms/
     * @param bool $isSubmitted
     * @param string $layoutCode see formLayout.pdf
     * @param int $layoutGrid
     * @return string HTML
     */
    public function get($isSubmitted, $layoutCode, $layoutGrid, $floatLabel) {
        
        $this->normalizeRules();
        $this->normalizeItem();
        $this->normalizeName();
        $this->storeRules();
        
        $rulesStr = $this->rulesToStr();
        $rulesArr = $this->rules;
        
        $val = $this->initValue;
        if ($isSubmitted) {
            $val = $this->getPost();
        }
        $invalidIndex = $this->invalidIndex;
        $ruleResult = $this->ruleResult;
        
        $type = $this->itemType;
        $id = $this->inputId;
        $name = $this->inputName;
        $label = $this->label;
        $help = $this->help;
        $opts = $this->options;
        
        $class = '';
        if (isset($this->attributes['class'])) {
            $class = ' ' . $this->attributes['class'];
        }
        $attr = '';
        foreach ($this->attributes as $k => $v) {
            if ($k !== 'class') {
                $attr .= ' ' . $k . '="' . $v . '"';
            }
        }
        
        $hideHelp = $invalidIndex !== false && $rulesArr[(int)$invalidIndex][1];
        $infotxt = ($help) ? '<small class="form-text text-muted ml-1'.($hideHelp?' gg-hide':'').'">' . $help . '</small>'."\n" : '';
        foreach ($rulesArr as $index => $rule) {
            $hide = ($index === $invalidIndex) ? '' : ' gg-hide';
            if ($rule[1]) {
                $infotxt .= '<div class="invalid-feedback ml-1' . $hide . '" data-ruleref="' . $index . '">' . $rule[1] . '</div>'."\n";
            }
        }
        
        $invalidclass = ($invalidIndex !== false) ? ' is-invalid' : '';
        
        switch ($type) {
            case 'html':
                $label = str_replace('{grid}', $layoutGrid, $label);
                $label = str_replace('{gridrest}', 12 - $layoutGrid, $label);
                $html = $label."\n";
                break;
            case 'fieldset':
                $html = "\n".'<!-- - - - - - gg FIELDSET START - - - - - -->'."\n";
                $html .= '<fieldset class="form-group' . $class . '"' . $attr . '>'."\n";
                $html .= '<legend>' . $label . '</legend>'."\n";
                break;
            case 'hidden':
                $html = '    <input name="' . $name . '" type="hidden" id="' . $id . '" class="' . $class . '"' . $attr . ' value="' . $val . '">'."\n";
                break;
            case 'text':
            case 'password':
            case 'color': // no IE 11
            case 'date': // no IE 11
            case 'datetime-local': // no FF
            case 'email':
            case 'month': // no FF
            case 'number':
            case 'range':
            case 'search':
            case 'tel':
            case 'time': // no IE 12
            case 'url':
            case 'week': // no FF
                if ($type == 'range') {
                    $inputclass = 'custom-range';
                    $pclass = ' pt-0';
                } else {
                    $inputclass = 'form-control';
                    $pclass = '';
                }
                if ($type == 'password') {
                    $input = '<div class="input-group' . $invalidclass . '">'."\n";
                    $input .= '    <input name="' . $name . '" type="password" id="' . $id . '" class="form-control' . $invalidclass . $class . '"' . $attr . ' value="" autocomplete="off">'."\n";
                    $input .= '    <div class="input-group-append">'."\n";
                    $input .= '        <button class="btn btn-outline-secondary gg-togglepw" type="button"><span class="oi oi-eye"></span></button>'."\n";
                    $input .= '    </div>'."\n";
                    $input .= '</div>'."\n";
                    $input .= $infotxt;
                } else {
                    $input = '<input name="' . $name . '" type="' . $type . '" id="' . $id . '" class="' . $inputclass . $invalidclass . $class . '"' . $attr . ' value="' . $val . '">'."\n";
                    $input .= $infotxt;
                }
                if ($layoutCode == 'A' || $layoutCode == 'B') {
                    $html = '<div class="gg-input form-group" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $label ? '    <label for="' . $id . '">' . $label . '</label>'."\n" : '';
                    $html .= $this->indent($input, 1);
                    $html .= '</div>'."\n";
                } elseif ($layoutCode == 'C' || $layoutCode == 'D') {
                    $html = '<div class="form-group row">'."\n";
                    $html .= '    <label for="' . $id . '" class="col-sm-' . $layoutGrid . ' col-form-label' . $pclass .'">' . $label . '</label>'."\n";
                    $html .= '    <div class="gg-input col-sm-' . (12 - $layoutGrid) . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $this->indent($input, 2);
                    $html .= '    </div>'."\n";
                    $html .= '</div>'."\n";
                } elseif ($layoutCode == 'E' || $layoutCode == 'F') {
                    $html = '<div class="gg-input form-group col-md-' . $layoutGrid . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $label ? '    <label for="' . $id . '">' . $label . '</label>'."\n" : '';
                    $html .= $this->indent($input, 1);
                    $html .= '</div>'."\n";
                } else {
                    $html = $input;
                }
                $html = $this->indent($html, 1);
                break;
            case 'textarea':
                $textarea = '<textarea name="' . $name . '" id="' . $id . '" class="form-control' . $invalidclass . $class . '"' . $attr . '>' . $val . '</textarea>'."\n";
                $textarea .= $infotxt;
                if ($layoutCode == 'A' || $layoutCode == 'B') {
                    $html = '<div class="gg-input form-group" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $label ? '    <label for="' . $id . '">' . $label . '</label>'."\n" : '';
                    $html .= $this->indent($textarea, 1);
                    $html .= '</div>'."\n";
                } elseif ($layoutCode == 'C' || $layoutCode == 'D') {
                    $html = '<div class="form-group row">'."\n";
                    $html .= '    <label for="' . $id . '" class="col-sm-' . $layoutGrid . ' col-form-label  pt-0">' . $label . '</label>'."\n";
                    $html .= '    <div class="gg-input col-sm-' . (12 - $layoutGrid) . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $this->indent($textarea, 2);
                    $html .= '    </div>'."\n";
                    $html .= '</div>'."\n";
                } elseif ($layoutCode == 'E' || $layoutCode == 'F') {
                    $html = '<div class="gg-input form-group col-md-' . $layoutGrid . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $label ? '    <label for="' . $id . '">' . $label . '</label>'."\n" : '';
                    $html .= $this->indent($textarea, 1);
                    $html .= '</div>'."\n";
                } else {
                    $html = $textarea;
                }
                $html = $this->indent($html, 1);
                break;
            case 'select':
                $val = (array)$val;
                $selectOptions = '';
                if (empty($val) && isset($this->attributes['placeholder'])) {
                    $selectOptions .= '    <option value="" hidden="hidden" selected="selected" disabled="disabled">' . $this->attributes['placeholder'] . '</option>'."\n";
                }
                $i = 0;
                foreach ($opts as $optiontext => $v) {
                    $selected = (in_array($v, $val)) ? ' selected="selected" ' : '';
                    $selectOptions .= '    <option id="' . $id . '-' . $i++ . '" value="' . $v . '"' . $selected . '>' . $optiontext . '</option>'."\n";
                }
                $select = '<select name="' . $name . '" id="' . $id . '" class="custom-select' . $invalidclass . $class . '"' . $attr . '>'."\n";
                $select .= $selectOptions;
                $select .= '</select>'."\n";
                $select .= $infotxt;
                if ($layoutCode == 'A' || $layoutCode == 'B') {
                    $html = '<div class="gg-input form-group" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $label ? '    <label for="' . $id . '">' . $label . '</label>'."\n" : '';
                    $html .= $this->indent($select, 1);
                    $html .= '</div>'."\n";
                } elseif ($layoutCode == 'C' || $layoutCode == 'D') {
                    $html = '<div class="form-group row">'."\n";
                    $html .= '    <label for="' . $id . '" class="col-sm-' . $layoutGrid . ' col-form-label">' . $label . '</label>'."\n";
                    $html .= '    <div class="gg-input col-sm-' . (12 - $layoutGrid) . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $this->indent($select, 2);
                    $html .= '    </div>'."\n";
                    $html .= '</div>'."\n";
                } elseif ($layoutCode == 'E' || $layoutCode == 'F') {
                    $html = '<div class="gg-input form-group col-md-' . $layoutGrid . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $label ? '    <label for="' . $id . '">' . $label . '</label>'."\n" : '';
                    $html .= $this->indent($select, 1);
                    $html .= '</div>'."\n";
                } else {
                    $html .= $select;
                }
                $html = $this->indent($html, 1);
                break;
            case 'discreteselect':
                if (is_array($val)) {
                    $controltype = 'checkbox';
                    $controldiv = $controltype;
                    if ($layoutCode == 'A' || $layoutCode == 'C') {
                        $controldiv = 'switch';
                    }
                } else {
                    $controltype = 'radio';
                    $controldiv = $controltype;
                }
                $val = (array)$val;
                $inlineclass = ($layoutCode == 'B' || $layoutCode == 'D' || $layoutCode == 'F') ? ' custom-control-inline' : '';
                $controls = '';
                $i = 0;
                foreach ($opts as $lbl => $v) {
                    $checked = (in_array($v, $val)) ? ' checked="checked"' : '';
                    $controls .= '<div class="custom-control custom-' . $controldiv . $invalidclass . $inlineclass . '">'."\n";
                    $controls .= '    <input name="' . $name . '" type="' . $controltype . '" id="' . $id . '-' . $i . '" class="custom-control-input' . $invalidclass . $class . '"' . $attr . ' value="' . $v . '"' . $checked . '>'."\n";
                    $controls .= '    <label class="custom-control-label" for="' . $id . '-' . $i++ . '">' . $lbl . '</label>'."\n";
                    $controls .= '</div>'."\n";
                }
                $controls .= $infotxt;
                if ($layoutCode == 'C' || $layoutCode == 'D') {
                    $html = '<fieldset class="form-group">'."\n";
                    $html .= '    <div class="row">'."\n";
                    $html .= '        <legend class="col-form-label col-sm-' . $layoutGrid . ' pt-0">' . $label . '</legend>'."\n";
                    $html .= '        <div class="gg-input col-sm-' . (12 - $layoutGrid) . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $this->indent($controls, 3);
                    $html .= '        </div>'."\n";
                    $html .= '    </div>'."\n";
                    $html .= '</fieldset>'."\n";
                } elseif ($layoutCode == 'E') {
                    $html = '<div class="gg-input form-group col-md-' . $layoutGrid . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $this->indent($controls, 1);
                    $html .= '</div>'."\n";
                } else {
                    $html = '<div class="gg-input form-group" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $this->indent($controls, 1);
                    $html .= '</div>'."\n";
                }
                $html = $this->indent($html, 1);
                break;
            case 'file':
                $inputf = '<div class="custom-file">'."\n";
                $inputf .= '    <input name="' . $name . '" type="file" id="' . $id . '" class="custom-file-input' . $invalidclass . $class . '"' . $attr . '>'."\n";
                $inputf .= $label ? '    <label class="custom-file-label" for="' . $id . '">' . $label . '</label>'."\n" : '';
                $inputf .= $infotxt;
                $inputf .= '</div>'."\n";
                if ($layoutCode == 'C' || $layoutCode == 'D') {
                    $html = '<fieldset class="form-group">'."\n";
                    $html .= '    <div class="row">'."\n";
                    $html .= '        <legend class="col-form-label col-sm-' . $layoutGrid . ' pt-0"></legend>'."\n";
                    $html .= '        <div class="gg-input col-sm-' . (12 - $layoutGrid) . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $this->indent($inputf, 3);
                    $html .= '        </div>'."\n";
                    $html .= '    </div>'."\n";
                    $html .= '</fieldset>'."\n";
                } else {
                    $html = '<div class="gg-input form-group" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $this->indent($inputf, 1);
                    $html .= '</div>'."\n";
                }
                $html = $this->indent($html, 1);
                break;
            case 'image': // todo
                $html = '    <input type="image" id="' . $id . '"' . $attr . '>'; // in $_POST name_x and name_y
                break;
            case 'submit':
                $buttoni = '';
                if ($opts) {
                    $buttoni .= '<button type="button" id=' . $id . '-2nd' . '" class="btn ' . $opts['secondClass'] . '"'. $attr . '>' . $opts['secondButton'] . '</button>'."\n";
                }
                $buttoni .= '<button type="submit" id="'. $id . '" class="btn' . $class . '"' . $attr . '>' . $label . '</button>'."\n";
                if ($layoutCode == 'C' || $layoutCode == 'D') {
                    $html = '<fieldset class="form-group">'."\n";
                    $html .= '    <div class="row">'."\n";
                    $html .= '        <legend class="col-form-label col-sm-' . $layoutGrid . ' pt-0"></legend>'."\n";
                    $html .= '        <div class="col-sm-' . (12 - $layoutGrid) . '">'."\n";
                    $html .= $this->indent($buttoni, 3);
                    $html .= '        </div>'."\n";
                    $html .= '    </div>'."\n";
                    $html .= '</fieldset>'."\n";
                } else {
                    $html = '<div class="form-group">'."\n";
                    $html .= $this->indent($buttoni, 1);
                    $html .= '</div>'."\n";
                }
                break;
            case 'button':
                $buttoni = '<button type="button" id="'. $id . '" class="btn' . $class . '"' . $attr . '>' . $label . '</button>'."\n";
                if ($layoutCode == 'C' || $layoutCode == 'D') {
                    $html = '<fieldset class="form-group">'."\n";
                    $html .= '    <div class="row">'."\n";
                    $html .= '        <legend class="col-form-label col-sm-' . $layoutGrid . ' pt-0"></legend>'."\n";
                    $html .= '        <div class="col-sm-' . (12 - $layoutGrid) . '">'."\n";
                    $html .= $this->indent($buttoni, 3);
                    $html .= '        </div>'."\n";
                    $html .= '    </div>'."\n";
                    $html .= '</fieldset>'."\n";
                } else {
                    $html = '<div class="form-group">'."\n";
                    $html .= $this->indent($buttoni, 1);
                    $html .= '</div>'."\n";
                }
        }
        return $html;
    }
}


