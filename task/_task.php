<?php
/**
 * ggLib CORE
 * task wrapper to be called by cron
 * crontab command: "php _task.php taskname"
 * 
 * every task in separate file: taskname.task.php
 * 
 * run states are stored in taskname.state.txt (writeable for php)
 * {lastrun:TS, lastDuration:sec, isRunning:bool}
 */

// ----------------------------------------------------------

$taskname = $argv[1] ?? '-null-';

$runtime = time();

require_once '../config.php';

require_once gg\CFG_WEBROOT . 'vendor/autoload.php';
require_once gg\CFG_LIBDIR . 'tool.php';
require_once gg\CFG_LIBDIR . 'str.php';
require_once gg\CFG_LIBDIR . 'db.php';
require_once gg\CFG_LIBDIR . 'log.php';

try {
    if (is_file(gg\CFG_TASKDIR . $taskname . '.task.php')) {
        if (is_file(gg\CFG_TASKDIR . $taskname . '.state.txt')) {
            $taskStateObj = json_decode(file_get_contents(gg\CFG_TASKDIR . $taskname . '.state.txt'));
        } else {
            $taskStateObj = new stdClass();
            $taskStateObj->lastrun = 0;
            $taskStateObj->lastDuration = 0;
            $taskStateObj->isRunning = false;
        }
        if (!$taskStateObj->isRunning) {
            
            $newStateObj = new stdClass();
            $newStateObj->lastrun = $taskStateObj->lastrun;
            $newStateObj->lastDuration = $taskStateObj->lastDuration;
            $newStateObj->isRunning = true;
            file_put_contents(gg\CFG_TASKDIR . $taskname . '.state.txt', json_encode($newStateObj));
        
            $taskStarttime = microtime(true);
            require_once gg\CFG_TASKDIR . $taskname . '.task.php';

            $newStateObj = new stdClass();
            $newStateObj->lastrun = $runtime;
            $newStateObj->lastDuration = microtime(true) - $taskStarttime;
            $newStateObj->isRunning = false;
            file_put_contents(gg\CFG_TASKDIR . $taskname . '.state.txt', json_encode($newStateObj));
            
        } else {
            throw new Exception('Task ' . $taskname . ' still running');
        }
        
    } else {
        throw new Exception('Task file ' . $taskname . '.task.php not found');
    }
} catch (Exception $e) {
    $logObj = new gg\log(true, false, true);
    $logObj->write('task_error', "TASK fault:\n" . $e->getMessage());
}
