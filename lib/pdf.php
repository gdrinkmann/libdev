<?php
namespace gg\lib;

use setasign\Fpdi\Tcpdf\Fpdi;

/**
 * ggLib CORE
 * wrapper for FPDI (https://www.setasign.com/products/fpdi/about/)
 * in conjunction with TCPDF (https://tcpdf.org/)
 * 
 * take a template pdf and add content
 * 
 * you can use all public methods of obj->tcpdf
 * https://tcpdf.org/docs/srcdoc/TCPDF/class-TCPDF/
 * especially:
 * ->SetXY() [mm]
 * ->SetFont()
 * ->SetTextColorArray()
 * ->MultiCell()
 * ->Line()
 *
 * due to performance problems avoid html(-tables)
 * 
 * class gg\lib\pdf
 * @author Gerd
 */
class pdf {

    private $pagecount = 0;
    private $currentpageno = 0;
    private $fonts = array('times', 'helvetica');
    private $styles = array('', 'B', 'I', 'BI');
    
    /**
     * the TCPDF object to use with original TCPDF methods
     * @var object
     */
    public $tcpdf = null;
    
    /**
     * constructor
     * 
     * @param string $template template pdf (max version 1.4)
     * @param string $creator
     * @param string $author
     * @param string $title
     */
    public function __construct($template, $creator='', $author='', $title='') {
        
        $this->tcpdf = new Fpdi();
        $this->tcpdf->setPrintHeader(false);
        $this->tcpdf->setPrintFooter(false);
        if ($creator) {
            $this->tcpdf->SetCreator($creator);
        }
        if ($author) {
            $this->tcpdf->SetAuthor($author);
        }
        if ($title) {
            $this->tcpdf->SetTitle($title);
        }
        $templatePath = \gg\combinePathFile(CFG_TEMPLATEDIR, $template);
        if (is_file($templatePath)) {
            $this->pagecount = $this->tcpdf->setSourceFile($templatePath);
        } else {
            die('class gg\pdf: template not found');
        }
        $this->tcpdf->setCellPaddings(0);
        $this->nextPage();
    }
    
    /**
     * 
     * @return int pagecount
     */
    public function getPageCount() {
        return $this->pagecount;
    }
    
    /**
     * 
     * @return int currentpageno
     */
    public function nextPage() {
        $tplId = $this->tcpdf->importPage(++$this->currentpageno);
        $this->tcpdf->AddPage();
        $this->tcpdf->useTemplate($tplId, ['adjustPageSize' => true]);
        return $this->currentpageno;
    }
    
    /**
     * 
     * @param int $font 0=times, 1=helvetica
     * @param int $style 0='', 1=B, 2=I, 3=BI
     * @param int $size in pt
     * @param array $textcolorarray array(R,G,B) 0-255
     */
    public function setFont($font, $style = 0, $size = 12, $textcolorarray = [0,0,0]) {
        $this->tcpdf->SetFont($this->fonts[$font], $this->styles[$style], $size);
        $this->tcpdf->SetTextColorArray($textcolorarray);
    }
    
    /**
     * @param int $x [mm] topleft corner of table
     * @param int $y [mm] topleft corner of table
     * @param int $width [mm]
     * @param string $text line break \n
     * @param string $align L, C, R or J
     * @return int textblock height [mm]
     */
    public function addTextBlock($x, $y, $text, $width = 0, $align = 'L', $border = 0) {
        $h = $this->tcpdf->getStringHeight($width, $text);
        $this->tcpdf->setXY((int)$x,(int)$y);
        $this->tcpdf->MultiCell($width, 2, $text, $border, $align);
        return $h;
    }
    
    /**
     * add address block and date
     * reference DIN 5008 (https://de.wikipedia.org/wiki/DIN_5008#Briefgestaltung_(Abschnitt_17))
     * @param string $addressText line break \n
     * @param string $dinType A or B
     * @param bool $foldingmark
     */
    public function addAddressBlock($addressText, $dinType = 'B', $foldingmark = false) {
        if ($dinType == "A") {
            $addressY = 37;
            $dateY = 72;
            $fmarkY = 87;
        } else {
            $addressY = 55;
            $dateY = 90;
            $fmarkY = 105;
        }
        $this->setFont(1);
        $this->addTextBlock(25, $addressY, $addressText);
        $this->addTextBlock(150, $dateY, date('d.m.Y'));
        if ($foldingmark) {
            $lstyle = array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
            $this->tcpdf->Line(2, $fmarkY, 7, $fmarkY, $lstyle);
            $this->tcpdf->Line(2, $fmarkY+105, 7, $fmarkY+105, $lstyle);
        }
    }

    /**
     * @param int $x [mm] topleft corner of table
     * @param int $y [mm] topleft corner of table
     * @param array $columndefs array of column definitions array(width[mm],align)
     * @param array $cells array of sequential cell texts
     * @param int $maxTableHeight
     * @param int $cellpadding
     * @param int $cellBorder
     * @return array rest of $cells array
     */
    public function addTextTable($x, $y, $columndefs, $cells, $maxTableHeight = null, $cellpadding = 1, $cellBorder = 0) {
        $prevCellPaddings = $this->tcpdf->getCellPaddings();
        $this->tcpdf->SetCellPadding($cellpadding);
        $offsetX = $x;
        $offsetY = $y;
        $countCols = count($columndefs);
        $cellPointer = -1;
        $tableH = 0;
        $rowH = 0;
        while (isset($cells[++$cellPointer])) {
            $colPointer = $cellPointer % $countCols;
            $cellWidth = $columndefs[$colPointer][0];
            $cellAlign = $columndefs[$colPointer][1];
            $cellH = $this->addTextBlock($x, $y, $cells[$cellPointer], $cellWidth, $cellAlign, $cellBorder);
            $rowH = max($cellH, $rowH);

            if ($colPointer) {
                $x += $cellWidth;
            } else {
                $x = $offsetX;
                $tableH += $rowH;
                if (is_int($maxTableHeight) && $tableH >= $maxTableHeight) break;
                $rowH = 0;
                $y = $offsetY + $tableH;
            }
        }
        $this->tcpdf->setCellPaddings($prevCellPaddings['L'],$prevCellPaddings['T'],$prevCellPaddings['R'],$prevCellPaddings['B']);
        return array_slice($cells, ++$cellPointer);
    }
    
    /**
     * 
     * @return string|false filename
     */
    public function savePdf() {
        $this->tcpdf->setPDFVersion('1.4');
        $uniqueFile = tempnam(CFG_TEMPDIR, 'ggpdf');
        $this->tcpdf->Output($uniqueFile, 'F');
        if (is_file($uniqueFile)) {
            return $uniqueFile;
        } else {
            return false;
        }
    }
    
}
