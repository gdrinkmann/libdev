<?php
namespace gg\lib;

/**
 * ggLib CORE
 * abstract API (send/respond data in JSON/XML)
 * 
 * class gg\lib\api
 * @author Gerd
 */
class api {

    private $url = '';
    private $user = '';
    private $pass = '';
    private $logDestination = array();
    private $logCategory = '';

    private $dataToSend = false;
    private $contentType = '';

    private $response = 'Nothing sent';
    private $statuscode = -1;
    
    function __construct($url, $user, $pass) {
        $this->url = $url;
        $this->user = $user;
        $this->pass = $pass;
    }
    
    /**
     * @param type $input
     * @param type $encode 'text', 'json, 'jsonencode', 'xml'
     */
    public function setInput($input, $encode = 'text') {
        switch ($encode) {
            case ('json'):
                $this->contentType = 'application/json';
                break;
            case ('jsonencode'):
                $input = json_encode($input);
                $this->contentType = 'application/json';
                break;
            case ('xml'):
                $this->contentType = 'application/xml';
                break;
            default:
                $this->contentType = 'text/plain; charset=utf-8';
        }
        $this->dataToSend = $input;
    }
    
    /**
     * @param bool $toFile
     * @param bool $toDB
     * @param string $category API name
     */
    public function setLog($toFile, $toDB, $category) {
        $this->logDestination = array($toFile, $toDB);
        $this->logCategory = $category;
    }
    
    function send($timeout = 10) {
        if (!empty($this->dataToSend)) {

            $curl = curl_init($this->url);
            curl_setopt($curl, CURLOPT_USERPWD, $this->user . ":" . $this->pass);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($curl, CURLOPT_POSTFIELDS, $this->dataToSend);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $timeout);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-type: ' . $this->contentType,
                'Content-Length: ' . strlen($this->dataToSend)));

            $this->response = curl_exec($curl);
            $this->statuscode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            $curlerrno = curl_errno($curl);
            if ($curlerrno !== 0) {

                $this->statuscode = $curlerrno;
                $this->response = curl_error($curl);
            }

            curl_close($curl);
        }
        if ($this->logCategory) {
            $logObj = new log($this->logDestination[0], $this->logDestination[1], false);
            $msg = 'SEND: ' . print_r($this->dataToSend, true) . "\n";
            $msg .= 'RESPONSE: ' . print_r($this->response, true) . "\n";
            $msg .= 'STATUS: ' . print_r($this->statuscode, true) . "\n";
            $logObj->write($this->logCategory, $msg);
        }
    }

    public function getResponse() {
        return $this->response;
    }
    
    public function getStatus() {
        return $this->statuscode;
    }
}
