<?php
namespace gg;
/**
 * ggLib sample
 */

//$PAGE->requiresRole('admin');

// define blanco.tmpl.php as view template
$PAGE->setViewtemplate('blanco');

$PAGE->setTitle('File Transfer Showcase');

// preconditions
$attachDir = combinePathFile(lib\CFG_WEBROOT."image/");
$attach1 = "testTransferFile.gif";
$attach2 = "testTransferFile.pdf";
$attach3 = "testTransferFile.xlsx";

// gglib transfer class
require_once lib\CFG_LIBDIR . 'transfer.php';
$transferObj = new lib\transfer();

$PAGE->initForm('transfer');

if ($PAGE->form()->isValidSubmit()) {
    
    $transferObj->sendMethod = _post('sendmethod');
    foreach (_post('sendoptions') as $sendoption) {
        $transferObj->enableSendOpt($sendoption);
    }
    if ($transferObj->sendMethod == 'upload') {
        $transferObj->addData('upfile1', lib\CFG_TEMPDIR);
        $transferObj->addData('upfile2', lib\CFG_TEMPDIR);
    } else {
        $filePost = _post('file');
        if (in_array(1, $filePost)) {
            copy($attachDir.$attach1, lib\CFG_TEMPDIR.$attach1);
            $transferObj->addData("renamed_".$attach1, lib\CFG_TEMPDIR.$attach1);
        }
        if (in_array(2, $filePost)) {
            copy($attachDir.$attach2, lib\CFG_TEMPDIR.$attach2);
            $transferObj->addData("renamed_".$attach2, lib\CFG_TEMPDIR.$attach2);
        }
        if (in_array(3, $filePost)) {
            copy($attachDir.$attach3, lib\CFG_TEMPDIR.$attach3);
            $transferObj->addData("renamed_".$attach3, lib\CFG_TEMPDIR.$attach3);
        }
    }
    $transferObj->url = _post('url', '');
    $transferObj->user = _post('user', '');
    $transferObj->pass = _post('pass', '');
    $transferObj->privKeyFile = _post('privKeyFile', '');
    $transferObj->pubKeyFile = _post('pubKeyFile', '');
    $transferObj->passPhrase = _post('passPhrase', '');

    $transferObj->zipFileName = _post('zipname', '');
    $transferObj->zipPass = _post('zippass', '');

    $transferObj->mail_to = _post('to', '');
    $transferObj->mail_bcc = _post('bcc', '');
    $transferObj->mail_subj = _post('subject', '');
    $transferObj->mail_body = _post('body', '');
    
    echo 'haha';
//    if ($transferObj->process()) {
//        _m('FILE(S) SENT', lib\PRIO_SUCCESS);
//    } else {
//        _m('ERROR', lib\PRIO_ERROR);
//    }
    
    //var_dump($transferObj->sendMethod, $transferObj->getSendOpts(), $transferObj->info, $transferObj->fallback, $transferObj->transFiles);
    
}

$PAGE->form()->addFieldsetOpen('<span class="oi oi-dashboard"></span> Transfer mode');
$sendSchemes = $transferObj->getAvailableSendSchemes();
$sendSchemesOpts = [];
foreach ($sendSchemes as $scheme => $v) {
    $sendSchemesOpts[$scheme] = $scheme;
}
$rules = null;
$PAGE->form()->addSelect('sendmethod', $sendSchemesOpts, null, $rules, 'Transfer Method', 'Primary select', ['placeholder' => 'Please choose']);
$sendOptions = $transferObj->getSendOpts();
$sendOptionsOpts = [];
foreach ($sendOptions as $opt => $v) {
    $sendOptionsOpts[$opt] = $opt;
}
$rules = [['required', ['max'=>4], 'Please choose at least one option']];
$PAGE->form()->addSelect('sendoptions', $sendOptionsOpts, [], $rules, 'Transfer Options (multiple)', 'Use Strg key for choosing more than only one option', ['placeholder' => 'Please choose', 'class' => 'sendspec']);
$PAGE->form()->addFieldsetClose();

$PAGE->form()->addFieldsetOpen('<span class="oi oi-folder"></span> Source files for transfer');
$PAGE->form()->addHtml('<div class="form-text text-muted">Files-Original-Source: ' . $attachDir . '</div>');
$PAGE->form()->addHtml('<div class="form-text text-muted mb-3">Files-Source: ' . lib\CFG_TEMPDIR . '</div>');
$rules = [['required', 'Please choose at least one file']];
$PAGE->form()->addDiscreteSelect('file', [$attach1 => 1, $attach2 => 2, $attach3 => 3], [], $rules, 'Files to transfer', null, ['class' => 'sendspec specFile']);
$rules = [['required', 'Please choose a .txt file'],['file', ['types' => '.txt'], 'Please choose a .txt file']];
$PAGE->form()->addUpload('upfile1', $rules, 'Choose upload file 1', null, ['class' => 'sendspec specUfile']);
$PAGE->form()->addUpload('upfile2[]', $rules, 'Choose upload file 2', null, ['class' => 'sendspec specUfile']);
$PAGE->form()->addUpload('upfile2[]', $rules, 'Choose upload file 3', null, ['class' => 'sendspec specUfile']);
$PAGE->form()->addFieldsetClose();

$PAGE->form()->addFieldsetOpen('<span class="oi oi-target"></span> Destination parameter');
$rules = [['required', 'Please fill this field'], ['length', ['min' => 5], 'Specify a URL/path (minimum 5 characters)']];
$PAGE->form()->addText('url', '', $rules, 'URL/Path', null, ['class' => 'sendspec specUrl']);
$rules = [['text', 'Specify a username']];
$PAGE->form()->addText('user', '', $rules, 'Username', null, ['class' => 'sendspec specUrlAuth']);
$rules = [['text', 'Specify a password']];
$PAGE->form()->addPassword('pass', '', $rules, 'Password', null, ['class' => 'sendspec specUrlAuth']);
$rules = [['required', 'Please fill this field'], ['length', ['min' => 3], 'Specify a private key file']];
$PAGE->form()->addText('privKeyFile', '', $rules, 'Private Key File', null, ['class' => 'sendspec specKey']);
$rules = [['text', 'Specify a public key file']];
$PAGE->form()->addText('pubKeyFile', '', $rules, 'Public Key File', null, ['class' => 'sendspec specKey']);
$rules = [['text', 'Specify a key passphrase']];
$PAGE->form()->addPassword('passPhrase', '', $rules, 'Key Passphrase', null, ['class' => 'sendspec specKey']);
$PAGE->form()->addFieldsetClose();

$PAGE->form()->addFieldsetOpen('<span class="oi oi-fullscreen-exit"></span> Zip (used if filled)');
$rules = [['text', 'Specify a zipfile name']];
$PAGE->form()->addText('zipname', '', $rules, 'Zipfile name', null, ['class' => 'sendspec specZip']);
$rules = [['text', 'Specify a zipfile password']];
$PAGE->form()->addPassword('zippass', '', $rules, 'Zipfile passwort', null, ['class' => 'sendspec specZip']);
$PAGE->form()->addFieldsetClose();

$PAGE->form()->addFieldsetOpen('<span class="oi oi-envelope-closed"></span> Email (used if filled, maybe only as infomail)');
$PAGE->form()->addHtml('<div class="form-text text-muted">Mailserver: ' . lib\CFG_SMTPSERVER . '</div>');
$PAGE->form()->addHtml('<div class="form-text text-muted">SMTP-user: ' . lib\CFG_SMTPUSER . '</div>');
$PAGE->form()->addHtml('<div class="form-text text-muted mb-3">Sender: ' . lib\CFG_SMTPFROM . '</div>');
$rules = [['email', 'Specify an email address']];
$PAGE->form()->addEmail('to', '', $rules, 'Address', null, ['class' => 'sendspec specMail']);
$rules = [['inList', ['list' => '{{to}}'], 'repeat the email address']];
$PAGE->form()->addEmail('to2', '', $rules, 'Repeat Address', null, ['class' => 'sendspec specMail']);
$rules = [['email', ['multiple' => true], 'Specify one or more email addresses']];
$PAGE->form()->addEmail('bcc', '', $rules, 'BCC', null, ['class' => 'sendspec specMail']);
$rules = [['required', ['if' => '{{to}}'], 'Specify a subject if Address is filled'], ['text', 'Specify a subject']];
$PAGE->form()->addText('subject', '', $rules, 'Subject', 'must be filled if Address is filled', ['class' => 'sendspec specMail']);
$rules = [['required', ['if' => '{{to}}'], 'Specify a body if Address is filled'], ['text', 'Specify a body text']];
$PAGE->form()->addTextarea('body', '', $rules, 'Body', 'must be filled if Address is filled', ['class' => 'sendspec specMail autoheight']);
$PAGE->form()->addFieldsetClose();

$PAGE->form()->addSubmitButton('Make it so! <span class="oi oi-play-circle"></span>');
