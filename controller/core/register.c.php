<?php
namespace gg;
/**
 * ggLib sample
 */

$PAGE->setTitle(_t('head_register', 'user'));

$PAGE->initForm('register');

if ($PAGE->form()->isValidSubmit()) {
    
}

$rules = [['required', _t('err_usernameRequired', 'user')],
          ['length', _t('err_usernameRequired', 'user')],
          ['text'], _t('err_usernameRequired', 'user')];
$PAGE->form()->addText('username', '', $rules, '<i class="bi bi-tag-fill"></i> '._t('lbl_newusername', 'user'));

$rules = [['required', _t('err_emailRequired', 'user')],
          ['length', _t('err_emailRequired', 'user')],
          ['email', _t('err_emailRequired', 'user')]];
$PAGE->form()->addEmail('email', '', $rules, '<i class="bi bi-envelope-fill"></i> '._t('lbl_email', 'user'));

$PAGE->form()->addSubmitButton(_t('btn_register', 'user'));