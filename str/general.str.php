<?php
/**
 * ggLib CORE
 * 
 * STRINGS
 * required by gg\str
 */

// scope GENERAL

$str['head_startpage'] = array(
    'de' => 'Startseite',
    'en' => 'Startpage'
);

$str['language'] = array(
    'de' => 'Sprache',
    'en' => 'Language'
);

$str['lang_de'] = array(
    'de' => 'Deutsch',
    'en' => 'German'
);

$str['lang_en'] = array(
    'de' => 'Englisch',
    'en' => 'English'
);

$str['btn_send'] = array(
    'de' => 'Absenden',
    'en' => 'Send'
);

$str['btn_cancel'] = array(
    'de' => 'Abbrechen',
    'en' => 'Cancel'
);

$str['msg_nocontroller'] = array(
    'de' => 'HTTP 404: Angeforderte Seite nicht vorhanden!',
    'en' => 'HTTP 404: Requested page not available!'
);

$str['msg_nodatabase'] = array(
    'de' => 'HTTP 500: Datenbankfehler',
    'en' => 'HTTP 500: Error in Database'
);

// formats (required for functionality) ----------------------
$str['format_number'] = array(
    'de' => ',|.',
    'en' => '.|,'
);

$str['format_date'] = array(
    'de' => 'd.m.Y',
    'en' => 'Y-m-d'
);

$str['format_time'] = array(
    'de' => 'H:i',
    'en' => 'H:i'
);

$str['format_timesec'] = array(
    'de' => 'H:i:s',
    'en' => 'H:i:s'
);

$str['format_datetime'] = array(
    'de' => 'd.m.Y H:i',
    'en' => 'Y-m-d H:i'
);

$str['format_datetimesec'] = array(
    'de' => 'd.m.Y H:i:s',
    'en' => 'Y-m-d H:i:s'
);