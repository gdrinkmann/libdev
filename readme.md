namespace:
## gglib
## Simple MVC PHP framework without Schnickschnack for responsive webprojects.

project name:
## LibDev

What does Schnickschnack mean?

RequireJS, all kind of JS framework (except jquery), JS uglifier, SCSS (except in bootstrap), PHP template engine, blinkblink and boomboom

## Features:

- **PAGE-Object** containing all relevant page properties and methods
- **class str** with i18n ability and nested string modules
- **class form** with separating logical elements from output properties of form items, handling clean secure submits and building different **bootstrap** layouts. See **lib/formLayout.pdf**
- **class rule** for client- AND serverside form validation
- **class db** for MySql abstraction with prepared statements and history
- predefined **user management** with groups and roles
- injection- and CSRF-protected
- prepared classes for **tasks** (cron) and **queues**
- some usefull wrapper classes (api, pdf, excel, transfer, log and other)
- all layout/design/theming should be done by bootstrap themes

## MVC Structure:

- central index.php
- **model** = DB-tablename
- **view** (html output) = pagename
- **controller** = pagename

The delivered HTML is created by following abstract process:

- **web root**: main index.php is called
- URL http://somewhat.xyz/ **folder/name**.html is parsed to page->name = **folder/name** 
- folder **controller**: the controller "controllerpath/ **folder/name**.c.php" is processed
- folder **model**: within the controller there may be methods using models (DB access via tablename.m.php)
- folder **template**: within the controller a view template (templatename.tmpl.php) may be defined (usually "default")
- folder **view**: usually the "viewpath/ **folder/name**.v.php" is included in the template (maybe overwritten)
- folder **js**: usually the "jspath/ **folder/name**.js" is included in the template
- folder **css**: usually the "csspath/ **folder/name**.css" is included in the template
- resulting HTML is rendered

There is **no** PHP-Template-System like Smarty, Twig, Blade, Mustache ...

...instead this is used:

- short echo tags **<?=** (always available since PHP 5.4.0) in the HTML flow
- good to know: a php tag has no linebreak at the end
- no semicolon at the end of closed php tags needed https://www.php.net/manual/en/language.basic-syntax.instruction-separation.php
- **$PAGE** is the only variable transfered from **controller** to **view** containing the page object with results of computations within the controller

## Dependencies:

- https://www.php.net/ >7.3 ;)
- https://tcpdf.org/
- https://www.setasign.com/products/fpdi/about/
- https://github.com/PHPMailer/PHPMailer
- https://github.com/phpseclib/phpseclib
- https://github.com/PHPOffice/PhpSpreadsheet
- https://getbootstrap.com/
- https://jquery.com/
- https://popper.js.org/
- https://datatables.net/
- https://useiconic.com/open

# THIS IS UNDER HEAVY CONSTRUCTION ;) !!

## Notes
### MySQL conventions
### naming reference (origin):
e.g. https://github.com/RootSoft/Database-Naming-Convention

### libdev db policies table/field names:
- use english names
- names are singular
- names are lowercase, use underscore for word separating
- avoid abbreviations
- do not use prefixes for table names
- every table has an auto-incremented numeric primary key named "id"
- parent id field is named "id_parent" (INT, NULL)
- table name is never part of its own column names
- datatype bool is TINYINT(1)
- datatype Unix Timestamp is INT(11)
- datatype date and datetime should be used
- use short table aliases in queries
- define foreign keys in db (let constraint names built automatically)
- foreign key name is build by referenced table name followed by "\_id" and may have prefixes "string\_"
- some tables need to have a field called "deleted" to prevent total deletion (TINYINT(1), NOT NULL)
- if item has name and needs to be displayed in different languages, add fields called "name" and "name_xx" with xx language
- n2m tables need to have the "2" in the tablename
- lookup-table names begin with "lu_"
- tables for storing aggregation results (redundant) begin with "agg_"
- sometimes there need to be copied foreign fields in addition to the foreign key for historic purposes, which begin with "cp_"
- indexes are uppercase and begin with "IX_"
- two basic extra tables should exit: "user" and "history"
