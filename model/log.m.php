<?php
namespace gg;
/**
 * ggLib sample
 */

class log extends lib\db {
    
    // field user_id: null = not captured, 0 = not logged in, id = logged in
    public static function writeEntry($category, $payload, $time, $userid, $to, $cc, $bcc) {
        $entryArr = [];
        $entryArr['category'] = $category;
        $entryArr['payload'] = $payload;
        $entryArr['time'] = $time;
        if ($userid !== false) {
            $entryArr['user_id'] = $userid ?? 0;
        }
        if ($to) {
            $entryArr['to'] = $to;
        }
        if ($cc) {
            $entryArr['cc'] = $cc;
        }
        if ($bcc) {
            $entryArr['bcc'] = $bcc;
        }
        $this->saveRecord($entryArr);
    }
    
    public static function cleanup($category, $timelimit) {
        $this->delete($this->tableName, 'category = %s AND time < %i', $category, $timelimit);
    }
}