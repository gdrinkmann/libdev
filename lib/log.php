<?php
namespace gg\lib;

/**
 * ggLib CORE
 * logging to file and/or DB and/or email
 * 
 * class gg\lib\log
 * @author Gerd
 */
class log {
    
    private $time;
    private $destination;
    private $destText;
    private $category;
    private $payload;
    private $maxAge;
    private $emailRecipt;
    private $dailyFile;
    private $sourceinfo;
    private $userid;
    private $usertext;

    /**
     * @param bool $toFile
     * @param bool $toDB
     * @param bool $toEmail
     */
    public function __construct($toFile, $toDB, $toEmail) {
        $this->time = time();
        
        $this->destination = array($toFile, $toDB, $toEmail);
        
        $dest = array();
        if ($toFile) {
            $dest[] = 'Logfile';
        }
        if ($toDB) {
            $dest[] = 'Database';
        }
        if ($toEmail) {
            $dest[] = 'Email';
        }
        $this->destText = implode('/', $dest);
                
        $this->maxAge = 0; // 0 infinite age
        $this->emailRecipt = CFG_WEBMASTEREMAIL;
        $this->dailyFile = true;
        $this->sourceinfo = 'not captured';
        $this->userid = false;
        $this->usertext = 'not captured';
    }
    
    /**
     * overwrite default recipient CFG_WEBMASTEREMAIL
     * @param string $emailAddress maybe several
     */
    public function setEmailRecipt($emailAddress) {
        $this->emailRecipt = $emailAddress;
    }
    
    /**
     * max age of log entries in this category
     * @param int $days
     */
    public function setMaxAge($days) {
        $this->maxAge = (int)$days * 86400; // result in seconds
    }
    
    /**
     * default enabled
     */
    public function disableDailyFile() {
        $this->dailyFile = false;
    }
    
    /**
     * add sourcefile name and line number to log
     */
    public function addSourceInfo() {
        $bt = debug_backtrace();
        $this->sourceinfo = $bt[0]['file'] . ' / L' . $bt[0]['line'];
    }
    
    /**
     * add user ID to log
     */
    public function addUserId() {
        $this->userid = getCurrentUser();
        if (is_null($this->userid)) {
            $this->usertext = 'no active user';
        } else {
            $this->usertext = 'ID ' . $this->userid;
        }
    }

    /**
     * @param string $category logfilename = [date]_<category>.log
     * @param mixed $payload
     * @param string $to
     * @param string $cc
     * @param string $bcc
     */
    public function write($category, $payload, $to = '', $cc = '', $bcc = '') {
        $cat = str_replace(['ä','ö','ü','ß',' ','.'], ['ae','oe','ue','ss','_','_'], strtolower($category));
        $this->category = preg_replace('/[^a-z0-9_-]/', '', $cat);
        
        $this->payload = $payload;
        
        if ($this->destination[0]) {
            $this->toFile($to, $cc, $bcc);
        }
        if ($this->destination[1]) {
            $this->toDB($to, $cc, $bcc);
        }
        if ($this->destination[2]) {
            $this->toEmail();
        }
        $this->cleanup();
    }
    
    private function cleanup() {
        if ($this->maxAge > 0) {
            $timelimit = $this->time - $this->maxAge;
            
            // del db entries
            $logDB = new \gg\log();
            $logDB->cleanup($this->category, $timelimit);
            
            // del logfiles
            $fileArr = dirToArray(CFG_LOGDIR, false, '/'.$this->category.'\.(log|zip)$/');
            foreach ($fileArr as $file) {
                $filetime = filectime(\gg\combinePathFile(CFG_LOGDIR, $file));
                if ($filetime < $timelimit) {
                    @unlink(\gg\combinePathFile(CFG_LOGDIR, $file));
                } else {
                    if (pathinfo($file, PATHINFO_EXTENSION) == 'log') {
                        
                    }
                }
            }
        }
        
        // zip old files
        $fileArr = \gg\dirToArray(CFG_LOGDIR, false, '/^'.$this->category.'\.log$/');
        foreach ($fileArr as $file) {
            $filetime = filectime(\gg\combinePathFile(CFG_LOGDIR, $file));
            if ($filetime < strtotime('today')) {
                $tempzipfile = createZip([$file => \gg\combinePathFile(CFG_LOGDIR, $file)]);
                rename($tempzipfile, \gg\combinePathFile(CFG_LOGDIR, pathinfo($file, PATHINFO_FILENAME)) . '.zip');
            }
        }
    }
    
    private function toFile($to, $cc, $bcc) {
        $lb = "\n";
        
        $addDate = ($this->dailyFile) ? date('Y-m-d', $this->time) . '_' : '';
        $logfilepath = \gg\combinePathFile(CFG_LOGDIR, $addDate . $this->category . '.log');
        
        $content = '*ENTRY: ' . date('Y-m-d H:i:s', $this->time). $lb;
        $content .= '*LOGGED to ' . $this->destText . ' with category ' . $this->category . $lb;
        $content .= '*SOURCE: ' . $this->sourceinfo . $lb;
        $content .= '*USER: ' . $this->usertext . $lb;
        $content .= ($to) ? '*TO: ' . $to . $lb : '';
        $content .= ($cc) ? '*CC: ' . $cc . $lb : '';
        $content .= ($bcc) ? '*BCC: ' . $bcc . $lb : '';
        $content .= '*MESSAGE:' . $lb;
        
        $payload = print_r($this->payload, true);
        $payload = str_replace("\r\n", $lb, $payload);
        
        $content .= $payload . $lb;
        
        file_put_contents($logfilepath, $content, FILE_APPEND);
    }
    private function toDB($to, $cc, $bcc) {
        \gg\log::writeEntry($this->category, $this->payload, $this->time, $this->userid, $to, $cc, $bcc);
    }
    private function toEmail() {
        $lb = "\r\n";
        
        $subject = CFG_PROJECTNAME . '-Log ' . $this->category;
        
        $body = 'This is a message from ' . CFG_PROJECTNAME . '-Log system.' . $lb;
        $body .= 'It\'s logged to ' . $this->destText . ' with category ' . $this->category . $lb;
        $body .= 'Time: ' . date('Y-m-d H:i:s', $this->time) . $lb;
        $body .= 'Source: ' . $this->sourceinfo . $lb;
        $body .= 'User: ' . $this->usertext . $lb;
        $body .= 'Message:' . $lb;
        
        $payload = print_r($this->payload, true);
        $payload = str_replace($lb, "\n", $payload);
        $payload = str_replace("\n", $lb, $payload);
        
        $body .= $payload . $lb;
        
        require_once CFG_LIBDIR . 'email.php';
        $objEmail = new email();
        $objEmail->addTo($this->emailRecipt);
        $objEmail->setLog(false, false, '');
        
        $objEmail->send($subject, $body);
    }
    
    public static function toSAPI($msg) {
        if (CFG_DEVMODUS) {
            error_log('(GG '. date('H:i:s') . ') ' . $msg, 4);
        }
    }
    
}
