<?php
namespace gg\lib;

const PRIO_INFO = 0;
const PRIO_SUCCESS = 1;
const PRIO_WARN = 2;
const PRIO_ERROR = 3;
const PRIO_CLASSES = [
    'alert-info',
    'alert-success',
    'alert-warning',
    'alert-danger'
];

/**
 * ggLib CORE
 * main gglib class
 * page properties and functions
 * 
 * standard included in index.php
 * 
 * class gg\lib\page
 * @author Gerd
 */
class page {
    
    // store results of controller here for use in view
    private $controller;
    
    private $title = ''; // page title
    private $view = '';
    private $viewtemplate = 'default';
    private $viewcontent;
    private $assets = array();
    private $elapsedTime;
    private $forms = array();
    private $form; // current form object if set
    private $request; // webclient request object
    private $user; // user object or null
    
    /**
     * page init
     */
    public function __construct() {
        //ob_start("ob_gzhandler");
        session_start();
        
        // URL INTERPRETATION
        // prerequisite by the webserver: urls must be rewritten to index.php and $_SERVER['REQUEST_URI'] must be filled properly
        $this->request = new request();
        $this->request->readCurrentUrl();
        
        // load submitted rules and fill $this->request->_POST
        $this->request->retrieveRules();
        $this->request->processPost();
        
        // fetch current user if any
        $this->user = getCurrentUser();
        
        // CSS and JS
        $this->assets['css'] = array();
        $this->assets['js'] = array();
        $this->setBasicAssets();
        // auto register correspondig CSS and JS
        $this->registerCSS();
        $this->registerJS();
        
        // save timestamp of page use
        $now = time();
        $this->elapsedTime = (isset($_SESSION['usertimestamp'])) ? $now - $_SESSION['usertimestamp'] : 0;
        $_SESSION['usertimestamp'] = $now;
        
        // initialize empty transfer object for transfering data from controller to view
        $this->controller = new \stdClass();
    }
    
    /**
     * get the pagename
     * @return string
     */
    public function getName() {
        return $this->request->getPageName();
    }
    
    /**
     * set pagetitle
     * @param string $pagetitle
     */
    public function setTitle($pagetitle) {
        $this->title = $pagetitle;
    }
    
    /**
     * get pagetitle
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }
    
    /**
     * basic CSS and JS
     * (has to be adapted when updating node-modules)
     */
    private function setBasicAssets() {
        // include basic bootstrap CSS
        $this->registerCSS('bootstrap/dist/css/bootstrap.min.css');
        
        // include bootstrap icons font CSS
        $this->registerCSS('bootstrap-icons/font/bootstrap-icons.css');
        
        // include open iconic bootstrap icon font CSS
        $this->registerCSS('_open-iconic-master/font/css/open-iconic-bootstrap.css');
        
        // include datatables bs5 responsive CSS
        $this->registerCSS('datatables.net-bs5/css/dataTables.bootstrap5.min.css');
        $this->registerCSS('datatables.net-responsive-bs5/css/responsive.bootstrap5.min.css');
        
        // include basic jquery and bootstrap JS
        $this->registerJS('jquery/dist/jquery.min.js');
        // $this->registerJS('@popperjs/core/dist/umd/popper.min.js');
        // $this->registerJS('bootstrap/dist/js/bootstrap.min.js');
        $this->registerJS('bootstrap/dist/js/bootstrap.bundle.min.js');
        
        // include datatables bs5 responsive JS
        $this->registerJS('datatables.net/js/jquery.dataTables.min.js');
        $this->registerJS('datatables.net-bs5/js/dataTables.bootstrap5.min.js');
        $this->registerJS('datatables.net-responsive/js/dataTables.responsive.min.js');
        $this->registerJS('datatables.net-responsive-bs5/js/responsive.bootstrap5.min.js');
        
        // include gglib specific css and js
        $this->registerCSS('_global.css');
        $this->registerJS('_global.js');
    }
    
    /**
     * optional use everywhere
     * @param string|array $message if array store paras for str class with key and optional scope and/or replaceArray
     * @param int $prio const PRIO_*
     * @param bool $clearBefore
     */
    public static function setMessage($message, $prio = PRIO_INFO, $clearBefore = false) {
        $prio = (int)$prio;
        if ($prio >= count(PRIO_CLASSES)) {
            die('class gg\page: message prio not defined');
        }
        if (!isset($_SESSION['message']) || $clearBefore) {
            self::clearMessages();
        }
        $msg = array();
        $hascontent = false;
        if (is_array($message)) {
            $scope = $message[1] ?? null;
            $replaceArray = $message[2] ?? null;
            $msg['key'] = $message[0];
            $msg['scope'] = $scope;
            $msg['replaceArray'] = $replaceArray;
            $hascontent = (bool)$message[0];
        } else {
            $msg['text'] = (string)$message;
            $hascontent = (bool)$message;
        }
        $msg['prop'] = PRIO_CLASSES[$prio];
        if ($hascontent) {
            $_SESSION['message'][] = $msg;
        }
    }
    
    /**
     * optional use in controller, view or viewtemplate
     * resets the message
     */
    public static function clearMessages() {
        $_SESSION['message'] = array();
    }
    
    /**
     * optional use view or viewtemplate
     * returns the message array
     */
    public static function getMessages() {
        $marr = array();
        if (isset($_SESSION['message'])) {
            foreach ($_SESSION['message'] as $msg) {
                if (isset($msg['text'])) {
                    $marr[] = $msg;
                } else {
                    $marr[] = array(
                        'text' => str::get($msg['key'], $msg['scope'], $msg['replaceArray']),
                        'prop' => $msg['prop']
                    );
                }
            }
        }
        self::clearMessages();
        return array_reverse($marr);
    }
    
    /**
     * write user object into session for login and redirect to referer
     */
    public function loginUser($userObj) {
        $_SESSION['user'] = $userObj;
        setSToken();
        request::redirectToReferer();
    }
    
    /**
     * place at the beginning of a protected controller
     */
    public function requiresAuth() {
        if (is_null($this->user)) {
            self::setMessage(['msg_authrequired', 'user'], PRIO_WARN);
            request::redirect(CFG_LOGINPAGE);
        }
        $this->autoLogoutUser();
    }
    
    /**
     * autologout
     */
    private function autoLogoutUser() {
        if ($this->elapsedTime > CFG_AUTOLOGOUTTIME) {
            self::setMessage(['msg_autologgedout', 'user'], PRIO_ERROR);
            $this->logoutUser(CFG_LOGINPAGE);
        }
    }
    
    /**
     * @return array roles
     */
    public function getRoles() {
        $userObj = $this->user;
        if (is_null($userObj)) {
            return [];
        }
        return \gg\user::getUserRoles($userObj);
    }
    
    /**
     * place at the beginning of a role protected controller
     * @param string $rolename
     */
    public function requiresRole($rolename) {
        $this->requiresAuth();
        $roles = $this->getRoles();
        if (!in_array($rolename, $roles)) {
            $roleDisplayName = \gg\user::getRoleDisplayName($rolename, str::getLang());
            self::setMessage(['msg_rolerequired', 'user', ['rolename' => $roleDisplayName]], PRIO_WARN);
            request::redirectToReferer();
        }
    }
    
    /**
     * end the user session
     * @param string $nextPage
     */
    public function logoutUser($nextPage = CFG_STARTPAGE) {
        unset($_SESSION['user'], $_SESSION['usertimestamp'], $_SESSION['stoken']);
        if ($nextPage === CFG_STARTPAGE) {
            self::setMessage(['msg_loggedout', 'user'], PRIO_INFO);
        }
        request::redirect($nextPage);
    }
    
    /**
     * @param string $asset js or css
     * @param string $name
     */
    private function registerAsset($asset, $name) {
        $name = ($name) ?: $this->request->getPageName() . '.' . $asset;
        $path1 = \gg\combinePathFile(CFG_WEBROOT, $asset, $name);
        $path2 = \gg\combinePathFile(CFG_WEBROOT, 'node_modules', $name);
        if (is_file($path1)) {
            $this->assets[$asset][] = request::pathCombine(CFG_BASEURI, $asset, $name);
        } elseif (is_file($path2)) {
            $this->assets[$asset][] = request::pathCombine(CFG_BASEURI, 'node_modules', $name);
        }
    }
    
    /**
     * @param string $asset
     * @return string
     */
    private function loadAsset($asset) {
        $tags = "";
        foreach ($this->assets[$asset] as $src) {
            if ($asset === 'js') {
                $tags .= '        <script src="' . $src . '"></script>'."\n";
            } elseif ($asset === 'css') {
                $tags .= '        <link rel="stylesheet" href="' . $src . '">'."\n";
            }
        }
        return ltrim($tags);
    }

    /**
     * optional use in controller
     * @param string $jsFilename
     */
    public function registerJS($jsFilename = null) {
        $this->registerAsset('js', $jsFilename);
    }
    
    /**
     * use in view or viewtemplate
     * @return string
     */
    public function loadJS() {
        return $this->loadAsset('js');
    }
    
    /**
     * optional use in controller
     * @param string $cssFilename
     */
    public function registerCSS($cssFilename = null) {
        $this->registerAsset('css', $cssFilename);
    }
    
    /**
     * use in view or viewtemplate
     * @return string
     */
    public function loadCSS() {
        return $this->loadAsset('css');
    }

    /**
     * defines new form object or set existing form object as current
     * @param string $formId
     */
    public function initForm($formId) {
        if (!isset($this->forms[$formId])) {
            $this->forms[$formId] = new form($formId);
        }
        $this->form = $this->forms[$formId];
    }
    
    /**
     * get current form object
     * @return type
     */
    public function form() {
        return $this->form;
    }
    
    /**
     * only used in index.php
     * @return string
     */
    public function getController() {
        $controllerPath = \gg\combinePathFile(CFG_CONTROLLERDIR, $this->request->getPageName() . '.c.php');
        if (is_file($controllerPath)) {
            return $controllerPath;
        } else {
            if (CFG_DEVMODUS) {
                die('class gg\page: Controller not found: ' . $controllerPath . ' | request_uri: ' . $_SERVER[ 'REQUEST_URI' ]);
            } else {
                self::setMessage(['msg_nocontroller', 'general'], PRIO_ERROR, true);
                http_response_code(500);
                request::redirect(CFG_STARTPAGE);
            }
        }
    }
    
    /**
     * optional use in controller
     * overwriting default view
     * @param string $view
     */
    public function setView($view) {
        $this->view = (string)$view;
    }
    
    /**
     * optional use in controller
     * overwriting default viewtemplate
     * @param string $templatename
     */
    public function setViewtemplate($templatename) {
        $this->viewtemplate = (string)$templatename;
    }
    
    /**
     * get viewtemplate
     * @return string
     */
    public function getViewtemplate() {
        return $this->viewtemplate;
    }
    
    /**
     * use within viewtemplate
     * @return string inner view html
     */
    public function getViewContent() {
        return $this->viewcontent;
    }
    
    /**
     * only used in index.php
     * output view
     */
    public function renderView() {
        $view = ($this->view) ?: $this->request->getPageName();
        $viewPath = \gg\combinePathFile(CFG_VIEWDIR, $view . '.v.php');
        if (is_file($viewPath)) {
            $PAGE = $this;
            
            ob_start();
            
            echo "\n".'<!-- - - - - - gg VIEW START name "' . $view . '" - - - - - -->'."\n";
            require $viewPath;
            echo "\n".'<!-- - - - - - gg VIEW END - - - - - -->'."\n";
            
            $PAGE->viewcontent = ob_get_clean();
            
            $viewtemplate = \gg\combinePathFile(CFG_TEMPLATEDIR, $PAGE->viewtemplate . '.tmpl.php');
            if (is_file($viewtemplate)) {
                ob_start();
                
                if (CFG_MAINTENANCE !== false) {
                    echo 'MAINTENANCE MODE !!';
                }
                require $viewtemplate;
                
                $fullViewContent = ob_get_clean();
                $templatestr = "\n".'<!-- - - - - - gg TEMPLATE "' . $PAGE->viewtemplate . '" - - - - - -->';
                $fullViewContent = preg_replace('/(<html .*?>)/', '$1'.$templatestr, $fullViewContent, 1);
                
                $this->request->storeReferer();
                
                // main page output
                echo $fullViewContent;
                
            } else {
                die('class gg\page: View template not found');
            }
        } else {
            die('class gg\page: View not found');
        }
    }
    
    /**
     * build the controller object
     * store results of controller here for use in view
     * @param string $key
     * @param mixed $value
     */
    public function controllerAdd($key, $value) {
        $this->controller->$key = $value;
    }
    
    /**
     * get the controller object
     * @return object
     */
    public function controller() {
        return $this->controller;
    }
    
}
