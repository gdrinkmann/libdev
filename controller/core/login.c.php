<?php
namespace gg;
/**
 * ggLib sample
 */

$PAGE->setViewtemplate('blancoslim');

$PAGE->setTitle(_t('head_login', 'user'));

$PAGE->initForm('login');

if ($PAGE->form()->isValidSubmit()) {
    $user = new user();
    $userObj = $user->verifyLogin(_post('username'), _post('password'));
    if (!$userObj) {
        _m(['msg_wrongcredentials'], lib\PRIO_ERROR, true);
    } else {
        $PAGE->loginUser($userObj);
    }
}

$rules = [['required', _t('err_usernameRequired', 'user')],
          ['length', _t('err_usernameRequired', 'user'), ['min' => 2, 'max' => 20]],
          ['text', _t('err_usernameRequired', 'user')]];
$PAGE->form()->addText('username', '', $rules, '<i class="bi bi-tag-fill"></i> '._t('lbl_username', 'user'));

$rules = [['required', _t('err_passwordRequired', 'user')],
          ['length', _t('err_passwordRequired', 'user'), ['min' => 8, 'max' => 50]],
          ['text', _t('err_passwordRequired', 'user')]];
$PAGE->form()->addPassword('password', '', $rules, '<i class="bi bi-lock-fill"></i> '._t('lbl_password', 'user'));

$PAGE->form()->addSubmitButton(_t('btn_login', 'user'));


