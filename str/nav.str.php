<?php
/**
 * ggLib sample
 */

// scope NAV

$str['start'] = array(
    'de' => 'Start',
    'en' => 'Start'
);

$str['admin'] = array(
    'de' => 'Verwaltung',
    'en' => 'Administration'
);

$str['listuser'] = array(
    'de' => 'Benutzerliste',
    'en' => 'User list'
);

$str['login'] = array(
    'de' => 'Anmelden',
    'en' => 'Login'
);

$str['logout'] = array(
    'de' => 'Abmelden',
    'en' => 'Logout'
);

$str['changepw'] = array(
    'de' => 'Passwort ändern',
    'en' => 'Change password'
);

$str['profile'] = array(
    'de' => 'Profil',
    'en' => 'Profile'
);