<?php
namespace gg\lib;

/**
 * ggLib CORE
 * string (language) class with l18n
 * 
 * standard included in index.php
 * 
 * class gg\lib\str
 * @author Gerd
 */
class str {
    
    const DEFAULT_SCOPE = 'general';

    /**
     * @param string $lang language abbr (i.e. de, en, es) ISO-639-1
     * https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
     * @param string $scope
     * @param int $cookieexpires default 60*60*24*90 (3 months)
     */
    public static function setLang($lang, $cookieexpires = 7776000) {
        $lang = substr($lang, 0, 10);
        $cookieOptions = [
            'expires' => time() + $cookieexpires,
            'path' => '/',
            'domain' => null,
            'secure' => !CFG_DEVMODUS,
            'httponly' => true,
            'samesite' => 'lax'
        ];
        setcookie('lang', $lang, $cookieOptions);
        $_SESSION['lang'] = $lang;
    }

    /**
     * @return string language abbr
     */
    public static function getLang() {
        $querylang = request::getRequest($_GET, 'lang', [['length', ['max'=>3]]], false);
        if ($querylang !== false) {
            self::setLang($querylang);
        }
        $ulang = \gg\user::getCurrentUserLang();
        if ($ulang) {
            return $ulang;
        } elseif (isset($_SESSION['lang'])) {
            return $_SESSION['lang'];
        } elseif (isset($_COOKIE['lang'])) {
            return $_COOKIE['lang'];
        } else {
            return CFG_INITLANGUAGE;
        }
    }
    
    /**
     * @param string $scope
     */
    private static function getStrArray($scope) {
        static $strStore = [];
        $currentScope = $strStore[$scope] ?? null;
        if ($scope !== $currentScope) {
            $strFile = \gg\combinePathFile(CFG_STRDIR, $scope . '.str.php');
            if (is_file($strFile)) {
                $str = [];
                include $strFile;
                $strStore[$scope] = $str;
            } else {
                die('class gg\str: scope not found');
            }
        }
        return $strStore[$scope];
    }
    
    /**
     * the defined string may include:
     * - {{{key name}}} will be replaced by value of argument replaceArray
     * - {{key}} or {{key|scope|lang}} will be replaced by defined str
     * @param string $key
     * @param string $scope null for default scope
     * @param obj $replaceArray
     * @param string $tmplang
     * @param bool $$fallbacks
     * @return string
     */
    public static function get($key, $scope = null, $replaceArray = null, $tmplang = null, $fallbacks = true) {
        $scope = $scope ?? self::DEFAULT_SCOPE;
        $strArr = self::getStrArray($scope);
        $lang = $tmplang ?? self::getLang();
        if (isset($strArr[$key][$lang])) {
            $string = $strArr[$key][$lang];
        } elseif (!$fallbacks) {
            return false;
        } elseif (isset($strArr[$key][CFG_INITLANGUAGE]) && $fallbacks && !CFG_DEVMODUS) {
            $string = $strArr[$key][CFG_INITLANGUAGE];
        } elseif (isset($strArr[$key]) && $fallbacks && !CFG_DEVMODUS) {
            $string = current($strArr[$key]);
        } elseif (CFG_DEVMODUS) {
            die('class gg\str: string key ('. $scope . '/' . $lang . ') "' . $key . '" not found');
        } else {
            $string = '[[' . $key . ']]';
        }
        
        if (preg_match_all('/{{{(\w+?)}}}/', $string, $matches)) {
            foreach ($matches[1] as $replaceStr) {
                if (isset($replaceArray[$replaceStr])) {
                    $string = preg_replace('/{{{'. $replaceStr . '}}}/', $replaceArray[$replaceStr], $string);
                }
            }
        }
        if (preg_match_all('/{{(\w+?)}}/', $string, $matches)) {
            foreach ($matches[1] as $strkey) {
                $paras = explode('|', $strkey);
                if (!isset($paras[1])) {
                    $paras[1] = $scope;
                }
                if (!isset($paras[2])) {
                    $paras[2] = $lang;
                }
                $replacement = self::get($paras[0], $paras[1], $replaceArray, $paras[2]);
                $string = preg_replace('/{{' . $strkey . '}}/', $replacement, $string);
            }
        }
        return $string;
    }
}
