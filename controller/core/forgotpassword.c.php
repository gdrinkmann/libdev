<?php
namespace gg;
/**
 * ggLib sample
 */

$PAGE->setViewtemplate('blancoslim');

$PAGE->setTitle(_t('head_forgotpassword', 'user'));

$PAGE->initForm('forgotpassword');

if ($PAGE->form()->isValidSubmit()) {
    
}

$rules = [['required', _t('err_emailRequired', 'user')],
          ['email', _t('err_emailRequired', 'user')]];
$PAGE->form()->addEmail('email', '', $rules, '<i class="bi bi-envelope-fill"></i> '._t('lbl_email', 'user'));

$PAGE->form()->addSubmitButton(_t('btn_send', 'user'));