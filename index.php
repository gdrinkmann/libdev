<?php
namespace gg\lib;
/**
 * gglib CORE
 * 
 * reserved in the global namespace: $PAGE
 * reserved in cookie: lang
 * reserved in session: user, usertimestamp, rules, stoken, lang, message, referer
 * 
 */

//echo 'DOCUMENT_ROOT: ' . $_SERVER["DOCUMENT_ROOT"];
//echo '<br>';
//echo 'REQUEST_URI: ' . $_SERVER['REQUEST_URI'];
//phpinfo();
//exit();

require_once 'config.php';

// Autoload models
spl_autoload_register(function ($class_name) {
    $classpath = CFG_MODELDIR . str_replace('gg\\', '', $class_name) . '.m.php';
    if (is_file($classpath)) {
        include $classpath;
    }
});

if (CFG_MAINTENANCE !== false) {
    require_once 'maintenance.php';
}
require_once CFG_WEBROOT . 'vendor/autoload.php'; // composer relating
require_once CFG_LIBDIR . 'tool.php'; // helper functions
require_once CFG_LIBDIR . 'sec.php'; // security functions
require_once CFG_LIBDIR . 'log.php'; // logging class
require_once CFG_LIBDIR . 'rule.php'; // validation rules class
require_once CFG_LIBDIR . 'form.php'; // form class, formItem class
require_once CFG_LIBDIR . 'str.php'; // string class
require_once CFG_LIBDIR . 'db.php'; // DB class
require_once CFG_LIBDIR . 'request.php'; // request class
require_once CFG_LIBDIR . 'page.php'; // core page class
require_once CFG_LIBDIR . 'shortcut.php'; // shortcut functions

$PAGE = new page();

ob_start();
include $PAGE->getController(); // process controller
page::setMessage(ob_get_clean());

$PAGE->renderView(); // final view output
