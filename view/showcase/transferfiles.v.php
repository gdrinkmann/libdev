<?php
namespace gg;
/**
 * ggLib sample
 * Bootstrap 5
 */
?>
<h5 class="card-header d-flex">
    <div><?= $PAGE->getTitle() ?></div> <a href="<?= lib\CFG_BASEURI ?>" class="card-link ms-auto"><i class="bi bi-house-door-fill"></i></a>
</h5>
<div class="card-body">
    <div class="col-lg-8">
    <?= $PAGE->form()->getTotalHtml('A', true) ?>
    </div>
</div>
