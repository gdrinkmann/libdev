<?php
namespace gg\lib;

/**
 * ggLib CORE
 * color model conversions
 * between RGB , HSV and HSL
 * https://de.wikipedia.org/wiki/HSV-Farbraum#Transformation_von_HSV/HSL_und_RGB
 * 
 * initially use setter methods and get results out of properties
 * 
 * class gg\lib\color
 * @author Gerd
 */
class color {

    public $R = 0; // red quota 0-255
    public $G = 0; // green quota 0-255
    public $B = 0; // blue quota 0-255
    public $RGB = array(0, 0, 0);
    public $HTMLcol = "#000000"; // HTML hex color (3 or 6 digit with or without #)
    public $H = 0; // color hue 0-360
    public $SV = 0; // saturation at HSV 0-100
    public $V = 0; // value 0-100
    public $HSV = array(0, 0, 0);
    public $SL = 0; // saturation at HSL 0-100
    public $L = 0; // lightness 0-100
    public $HSL = array(0, 0, 0);

    // ---- public methods ----
    public function setR($R) { // set red 0-255
        $this->R = $R;
        $this->RGBtoHSV();
        $this->RGBtoHSL();
    }

    public function setG($G) { // set green 0-255
        $this->G = $G;
        $this->RGBtoHSV();
        $this->RGBtoHSL();
    }

    public function setB($B) { // set blue 0-255
        $this->B = $B;
        $this->RGBtoHSV();
        $this->RGBtoHSL();
    }

    public function setRGB($R, $G, $B) { // set RGB, each 0-255
        $this->R = $R;
        $this->G = $G;
        $this->B = $B;
        $this->RGBtoHSV();
        $this->RGBtoHSL();
    }

    public function setHTMLcol($Ht) { // set HTML hex value
        $hc = $Ht;
        if ($hc[0] == '#') {
            $hc = substr($hc, 1);
        }
        if (strlen($hc) == 3) {
            $hc = $hc[0] . $hc[0] . $hc[1] . $hc[1] . $hc[2] . $hc[2];
        }
        $this->R = hexdec($hc[0] . $hc[1]);
        $this->G = hexdec($hc[2] . $hc[3]);
        $this->B = hexdec($hc[4] . $hc[5]);
        $this->RGBtoHSV();
        $this->RGBtoHSL();
    }

    public function setH($H) { // set hue 0-360
        $this->H = $H;
        $this->HSVtoRGB();
    }

    public function setSV($SV) { // set saturation (HSV) 0-100
        $this->SV = $SV;
        $this->HSVtoRGB();
        $this->RGBtoHSL();
    }

    public function setV($V) { // set value 0-100
        $this->V = $V;
        $this->HSVtoRGB();
        $this->RGBtoHSL();
    }

    public function setHSV($H, $S, $V) { // set HSV 0-360, 0-100, 0-100
        $this->H = $H;
        $this->SV = $S;
        $this->V = $V;
        $this->HSVtoRGB();
        $this->RGBtoHSL();
    }

    public function setSL($SL) { // set satuaration (HSL) 0-100
        $this->SL = $SL;
        $this->HSLtoRGB();
        $this->RGBtoHSV();
    }

    public function setL($L) { // set lightness 0-100
        $this->L = $L;
        $this->HSLtoRGB();
        $this->RGBtoHSV();
    }

    public function setHSL($H, $S, $L) { // set HSL 0-360, 0-100, 0-100
        $this->H = $H;
        $this->SL = $S;
        $this->L = $L;
        $this->HSLtoRGB();
        $this->RGBtoHSV();
    }

    // ---- private methods ----

    private function getHTMLcol() {
        $this->RGB = array($this->R, $this->G, $this->B);
        $this->HTMLcol = "#"
                . str_pad(dechex($this->R), 2, "0", STR_PAD_LEFT)
                . str_pad(dechex($this->G), 2, "0", STR_PAD_LEFT)
                . str_pad(dechex($this->B), 2, "0", STR_PAD_LEFT);
    }

    private function RGBtoHSV() {

        $r = $this->R / 255;
        $g = $this->G / 255;
        $b = $this->B / 255;

        $min = min($r, $g, $b);
        $max = max($r, $g, $b);
        $d = $max - $min;

        // compute H
        if ($d == 0) {
            $H = $this->H;
        } else {
            if ($r == $max) {
                $H = 60 * (($g - $b) / $d);
            } elseif ($g == $max) {
                $H = 60 * (($b - $r) / $d) + 120;
            } elseif ($b == $max) {
                $H = 60 * (($r - $g) / $d) + 240;
            }
            if ($H < 0) {
                $H += 360;
            }
            if ($H > 360) {
                $H -= 360;
            }
        }

        // compute SV
        if ($max == 0) {
            $SV = $this->SV / 100.0;
        } else {
            $SV = $d / $max;
        }

        // compute V
        $V = $max;

        $this->H = round($H);
        $this->SV = round($SV * 100);
        $this->V = round($V * 100);

        $this->HSV = array($this->H, $this->SV, $this->V);

        $this->getHTMLcol();
    }

    private function RGBtoHSL() {

        $r = $this->R / 255;
        $g = $this->G / 255;
        $b = $this->B / 255;

        $min = min($r, $g, $b);
        $max = max($r, $g, $b);
        $d = $max - $min;

        // compute H
        if ($d == 0) {
            $H = $this->H;
        } else {
            if ($r == $max) {
                $H = 60 * (($g - $b) / $d);
            } elseif ($g == $max) {
                $H = 60 * (($b - $r) / $d) + 120;
            } elseif ($b == $max) {
                $H = 60 * (($r - $g) / $d) + 240;
            }
            if ($H < 0) {
                $H += 360;
            }
            if ($H > 360) {
                $H -= 360;
            }
        }

        // compute SL
        if ($max == 0 || $min == 1) {
            $SL = $this->SL / 100.0;
        } else {
            $SL = $d / (1 - abs($max + $min - 1));
        }

        // compute L
        $L = ($max + $min) / 2;

        $this->H = round($H);
        $this->SL = round($SL * 100);
        $this->L = round($L * 100);

        $this->HSL = array($this->H, $this->SL, $this->L);

        $this->getHTMLcol();
    }

    private function HSVtoRGB() {

        $this->HSV = array($this->H, $this->SV, $this->V);

        if ($this->H < 0) {
            $this->H = 0;
        }
        if ($this->H > 360) {
            $this->H = 360;
        }
        if ($this->SV < 0) {
            $this->SV = 0;
        }
        if ($this->SV > 100) {
            $this->SV = 100;
        }
        if ($this->V < 0) {
            $this->V = 0;
        }
        if ($this->V > 100) {
            $this->V = 100;
        }

        $dS = $this->SV / 100.0;
        $dV = $this->V / 100.0;
        $dC = $dV * $dS;
        $dH = $this->H / 60.0;
        $dT = $dH;

        while ($dT >= 2.0) {
            $dT -= 2.0; // php modulo does not work with float
        }
        $dX = $dC * (1 - abs($dT - 1)); // as used in the Wikipedia link
        switch ($dH) {
            case($dH >= 0.0 && $dH < 1.0):
                $dR = $dC;
                $dG = $dX;
                $dB = 0.0;
                break;
            case($dH >= 1.0 && $dH < 2.0):
                $dR = $dX;
                $dG = $dC;
                $dB = 0.0;
                break;
            case($dH >= 2.0 && $dH < 3.0):
                $dR = 0.0;
                $dG = $dC;
                $dB = $dX;
                break;
            case($dH >= 3.0 && $dH < 4.0):
                $dR = 0.0;
                $dG = $dX;
                $dB = $dC;
                break;
            case($dH >= 4.0 && $dH < 5.0):
                $dR = $dX;
                $dG = 0.0;
                $dB = $dC;
                break;
            case($dH >= 5.0 && $dH < 6.0):
                $dR = $dC;
                $dG = 0.0;
                $dB = $dX;
                break;
            default:
                $dR = 0.0;
                $dG = 0.0;
                $dB = 0.0;
                break;
        }
        $dM = $dV - $dC;
        $dR += $dM;
        $dG += $dM;
        $dB += $dM;
        $dR *= 255;
        $dG *= 255;
        $dB *= 255;

        $this->R = round($dR);
        $this->G = round($dG);
        $this->B = round($dB);

        $this->getHTMLcol();
    }

    private function HSLtoRGB() {

        $this->HSL = array($this->H, $this->SL, $this->L);

        if ($this->H < 0) {
            $this->H = 0;
        }
        if ($this->H > 360) {
            $this->H = 360;
        }
        if ($this->SL < 0) {
            $this->SL = 0;
        }
        if ($this->SL > 100) {
            $this->SL = 100;
        }
        if ($this->L < 0) {
            $this->L = 0;
        }
        if ($this->L > 100) {
            $this->L = 100;
        }

        $h = $this->H / 360.0;
        $s = $this->SL / 100.0;
        $l = $this->L / 100.0;

        if ($s == 0) {
            $r = $l;
            $g = $l;
            $b = $l;
        } else {
            if ($l < .5) {
                $t2 = $l * (1.0 + $s);
            } else {
                $t2 = ($l + $s) - ($l * $s);
            }
            $t1 = 2.0 * $l - $t2;

            $rt3 = $h + 1.0 / 3.0;
            $gt3 = $h;
            $bt3 = $h - 1.0 / 3.0;

            if ($rt3 < 0) {
                $rt3 += 1.0;
            }
            if ($rt3 > 1) {
                $rt3 -= 1.0;
            }
            if ($gt3 < 0) {
                $gt3 += 1.0;
            }
            if ($gt3 > 1) {
                $gt3 -= 1.0;
            }
            if ($bt3 < 0) {
                $bt3 += 1.0;
            }
            if ($bt3 > 1) {
                $bt3 -= 1.0;
            }

            if (6.0 * $rt3 < 1) {
                $r = $t1 + ($t2 - $t1) * 6.0 * $rt3;
            } elseif (2.0 * $rt3 < 1) {
                $r = $t2;
            } elseif (3.0 * $rt3 < 2) {
                $r = $t1 + ($t2 - $t1) * ((2.0 / 3.0) - $rt3) * 6.0;
            } else {
                $r = $t1;
            }

            if (6.0 * $gt3 < 1) {
                $g = $t1 + ($t2 - $t1) * 6.0 * $gt3;
            } elseif (2.0 * $gt3 < 1) {
                $g = $t2;
            } elseif (3.0 * $gt3 < 2) {
                $g = $t1 + ($t2 - $t1) * ((2.0 / 3.0) - $gt3) * 6.0;
            } else {
                $g = $t1;
            }

            if (6.0 * $bt3 < 1) {
                $b = $t1 + ($t2 - $t1) * 6.0 * $bt3;
            } elseif (2.0 * $bt3 < 1) {
                $b = $t2;
            } elseif (3.0 * $bt3 < 2) {
                $b = $t1 + ($t2 - $t1) * ((2.0 / 3.0) - $bt3) * 6.0;
            } else {
                $b = $t1;
            }
        }

        $this->R = round(255 * $r);
        $this->G = round(255 * $g);
        $this->B = round(255 * $b);

        $this->getHTMLcol();
    }

}
