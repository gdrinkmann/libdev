<?php
namespace gg;

/**
 * ggLib CORE
 * tool functions in gg namespace
 * 
 * standard included in index.php
 * 
 * gglib tool functions
 * @author Gerd
 */

/**
 * combine paths and filename
 * @param string ...$parts
 * @return string combined path
 */
function combinePathFile(...$parts) {
    return combine(DIRECTORY_SEPARATOR, $parts);
}

/**
 * combine strings with separator
 * and avoid multiple separators in resulting string
 * @param string $separator
 * @param array $parts
 * @return string
 */
function combine($separator, $parts) {
    $parts = array_filter($parts);
    for ($i = 0; $i < count($parts); $i++) {
        if ($i === 0) {
            $parts[$i] = rtrim($parts[$i], ' ' . $separator);
        }
        if ($i > 0 && $i < count($parts) - 1) {
            $parts[$i] = trim($parts[$i], ' ' . $separator);
        }
        if ($i === count($parts) - 1) {
            $parts[$i] = ltrim($parts[$i], ' ' . $separator);
        }
    }
    return implode($separator, $parts);
}

/**
 * @param string $dir
 * @param bool $includeDir
 * @param string $filePattern filter names
 * @return type
 */
function dirToArray($dir, $includeDir = true, $filePattern = '/^.*$/') {
    $result = array();
    $cdir = scandir($dir);
    foreach ($cdir as $value) {
        if (!in_array($value, array('.', '..'))) {
            if (is_dir(combinePathFile($dir, $value) && $includeDir)) {
                // recursion!
                $result[$value] = dirToArray(combinePathFile($dir, $value), $includeDir, $filePattern);
            } else {
                if (preg_match($filePattern, $value)) {
                    $result[] = $value;
                }
            }
        }
    }
    return $result;
}

/**
 * get number string
 * @param float|int $number
 * @param int $decimals
 * @param bool $withseparator
 * @param string $lang
 * @return string
 */
function renderNumber($number, $decimals, $withseparator = false, $lang = null) {
    $lang = $lang ?? lib\str::getLang();
    $type = lib\str::get('format_number', null, null, $lang, false);
    $typearr = explode('|', $type);
    $separator = ($withseparator) ? $typearr[1] : '';
    if (is_float($number) || is_int($number)) {
        return number_format($number, (int)$decimals, $typearr[0], $separator);
    } else {
        return '-';
    }
}

/**
 * get number of numberstring
 * @param int|string $numberstring
 * @param string $lang
 * @return int|decimal
 */
function getNumber($numberstring, $lang = null) {
    if (!is_integer($numberstring) && !is_float($numberstring)) {
        $lang = $lang ?? lib\str::getLang();
        $type = lib\str::get('format_number', null, null, $lang, false);
        $typearr = explode('|', $type);

        $numberstring = str_replace(' ', '', $numberstring);
        $numberstring = str_replace($typearr[1], '', $numberstring);
        $numberstring = str_replace($typearr[0], '.', $numberstring);
    }
    return $numberstring * 1;
}

/**
 * get date string
 * @param string $format
 * @param int|string $date
 * @param string $lang
 * @return string
 */
function getDate($format, $date =null, $lang = null) {
    $date = $date ?? time();
    $datets = is_int($date) ? $date : strtotime($date);
    if ($format == 'toDBdatetime') {
        return date('Y-m-d H:i:s', $datets);
    }
    if ($format == 'toDBdate') {
        return date('Y-m-d', $datets);
    }
    $lang = $lang ?? lib\str::getLang();
    $strformat = lib\str::get($format, null, null, $lang, false);
    if ($strformat !== false) {
        return date($strformat, $datets);
    } else {
        return date($format, $datets);
    }
}

/**
 * string shortening with optional title attribute
 * (also look at bootstrap .text-truncate )
 * @param string $t origin string
 * @param int $n count of shown characters
 * @param boolean $titleatt create title attribute
 * @return string html string
 */
function shorten($t, $n, $titleatt = true) {
    $t = trim($t);
    if ($t) {
        if ($titleatt) {
            $pre = "<span title='$t'>";
            $post = "</span>";
        } else {
            $pre = "";
            $post = "";
        }
        if (strlen($t) > $n) {
            return $pre . substr($t, 0, $n - 2) . ".." . $post;
        } else {
            return $t;
        }
    } else {
        return "n.n.";
    }
}

/**
 * zero prepending
 * @param string $n  origin string
 * @param int $i over all count characters
 * @return string
 */
function preZero($n, $i) {
    return sprintf("%0{$i}s", $n);
}

/**
 * create zip file using PHP class ZipArchive
 * @param array $fileArray nameInZip (opt. with subdir) => full filepath
 * @param bool $delOrigins
 * @param string $password
 * @return boolean|string zip file full path
 */
function createZip($fileArray, $delOrigins = true, $password = null) {
    $zip = new ZipArchive;
    $uniqueZipFile = tempnam(CFG_TEMPDIR, 'ggzip');
    if ($zip->open($uniqueZipFile, ZipArchive::CREATE) === TRUE) {
        foreach ($fileArray as $nameInZip => $filename) {
            if (is_file($filename)) {
                if (!$zip->addFile($filename, $nameInZip)) {
                    return false;
                }
                if (!is_null($password)) {
                    $zip->setEncryptionName($nameInZip, ZipArchive::EM_AES_256, $password);
                }
            } else {
                return false;
            }
        }
    }
    if ($delOrigins) {
        foreach ($fileArray as $nameInZip => $filename) {
            @unlink($filename);
        }
    }
    return $uniqueZipFile;
}

/**
 * search n-dim array
 * @param mixed $needle
 * @param array $haystack
 * @return boolean
 */
function inArray($needle, $haystack) {
    foreach ($haystack as $key => $value) {
        $current_key = $key;
        if ($needle === $value || ( is_array($value) && in_array($needle, $value))) {
            return true;
        }
    }
    return false;
}

/**
 * Phonetik für die deutsche Sprache nach dem Kölner Verfahren
 *
 * Die Kölner Phonetik (auch Kölner Verfahren) ist ein phonetischer Algorithmus,
 * der Wörtern nach ihrem Sprachklang eine Zeichenfolge zuordnet, den phonetischen
 * Code. Ziel dieses Verfahrens ist es, gleich klingenden Wörtern den selben Code
 * zuzuordnen, um bei Suchfunktionen eine Ähnlichkeitssuche zu implementieren. Damit
 * ist es beispielsweise möglich, in einer Namensliste Einträge wie "Meier" auch unter
 * anderen Schreibweisen, wie "Maier", "Mayer" oder "Mayr", zu finden.
 *
 * Die Kölner Phonetik ist, im Vergleich zum bekannteren Russell-Soundex-Verfahren,
 * besser auf die deutsche Sprache abgestimmt. Sie wurde 1969 von Postel veröffentlicht.
 *
 * Infos: http://www.uni-koeln.de/phil-fak/phonetik/Lehre/MA-Arbeiten/magister_wilz.pdf
 *
 * Die Umwandlung eines Wortes erfolgt in drei Schritten:
 *
 * 1. buchstabenweise Codierung von links nach rechts entsprechend der Umwandlungstabelle
 * 2. entfernen aller mehrfachen Codes
 * 3. entfernen aller Codes "0" ausser am Anfang
 *
 * Beispiel  Der Name "Müller-Lüdenscheidt" wird folgendermaßen kodiert:
 *
 * 1. buchstabenweise Codierung: 60550750206880022
 * 2. entfernen aller mehrfachen Codes: 6050750206802
 * 3. entfernen aller Codes "0": 65752682
 *
 * Umwandlungstabelle:
 * ============================================
 * Buchstabe      Kontext                  Code
 * -------------  -----------------------  ----
 * A,E,I,J,O,U,Y                            0
 * H                                        -
 * B                                        1
 * P              nicht vor H               1
 * D,T            nicht vor C,S,Z           2
 * F,V,W                                    3
 * P              vor H                     3
 * G,K,Q                                    4
 * C              im Wortanfang
 *                vor A,H,K,L,O,Q,R,U,X     4
 * C              vor A,H,K,O,Q,U,X
 *                ausser nach S,Z           4
 * X              nicht nach C,K,Q         48
 * L                                        5
 * M,N                                      6
 * R                                        7
 * S,Z                                      8
 * C              nach S,Z                  8
 * C              im Wortanfang ausser vor
 *                A,H,K,L,O,Q,R,U,X         8
 * C              nicht vor A,H,K,O,Q,U,X   8
 * D,T            vor C,S,Z                 8
 * X              nach C,K,Q                8
 * --------------------------------------------
 *
 * ---------------------------------------------------------------------
 * Support/Info/Download: https://github.com/deezaster/germanphonetic
 * ---------------------------------------------------------------------
 *
 * @package    x3m
 * @version    1.2
 * @author     Andy Theiler <andy@x3m.ch>
 * @copyright  Copyright (c) 1996 - 2012, Xtreme Software GmbH, Switzerland (www.x3m.ch)
 * @license    http://www.opensource.org/licenses/bsd-license.php  BSD License
 */
function soundexGer($word) {
    $code = "";
    $word = strtolower($word);

    if (strlen($word) < 1) {
        return "";
    }

    // Umwandlung: v->f, w->f, j->i, y->i, ph->f, ä->a, ö->o, ü->u, ß->ss, é->e, è->e, ê->e, à->a, á->a, â->a, ë->e
    $word = str_replace(array("ç", "v", "w", "j", "y", "ph", "ä", "ö", "ü", "ß", "é", "è", "ê", "à", "á", "â", "ë"), array("c", "f", "f", "i", "i", "f", "a", "o", "u", "ss", "e", "e", "e", "a", "a", "a", "e"), $word);

    // Nur Buchstaben (keine Zahlen, keine Sonderzeichen)
    $word = preg_replace('/[^a-zA-Z]/', '', $word);

    $wordlen = strlen($word);
    $char = str_split($word);

    // Sonderfälle bei Wortanfang (Anlaut)
    if ($char[0] == 'c') {
        // vor a,h,k,l,o,q,r,u,x
        switch ($char[1]) {
            case 'a':
            case 'h':
            case 'k':
            case 'l':
            case 'o':
            case 'q':
            case 'r':
            case 'u':
            case 'x':
                $code = "4";
                break;
            default:
                $code = "8";
                break;
        }
        $x = 1;
    } else {
        $x = 0;
    }

    for (; $x < $wordlen; $x++) {
        switch ($char[$x]) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                $code .= "0";
                break;
            case 'b':
            case 'p':
                $code .= "1";
                break;
            case 'd':
            case 't':
                if ($x + 1 < $wordlen) {
                    switch ($char[$x + 1]) {
                        case 'c':
                        case 's':
                        case 'z':
                            $code .= "8";
                            break;
                        default:
                            $code .= "2";
                            break;
                    }
                } else {
                    $code .= "2";
                }
                break;
            case 'f':
                $code .= "3";
                break;
            case 'g':
            case 'k':
            case 'q':
                $code .= "4";
                break;
            case 'c':
                if ($x + 1 < $wordlen) {
                    switch ($char[$x + 1]) {
                        case 'a':
                        case 'h':
                        case 'k':
                        case 'o':
                        case 'q':
                        case 'u':
                        case 'x':
                            switch ($char[$x - 1]) {
                                case 's':
                                case 'z':
                                    $code .= "8";
                                    break;
                                default:
                                    $code .= "4";
                            }
                            break;
                        default:
                            $code .= "8";
                            break;
                    }
                } else {
                    $code .= "8";
                }
                break;
            case 'x':
                if ($x > 0) {
                    switch ($char[$x - 1]) {
                        case 'c':
                        case 'k':
                        case 'q':
                            $code .= "8";
                        default:
                            $code .= "48";
                    }
                } else {
                    $code .= "48";
                }
                break;
            case 'l':
                $code .= "5";
                break;
            case 'm':
            case 'n':
                $code .= "6";
                break;
            case 'r':
                $code .= "7";
                break;
            case 's':
            case 'z':
                $code .= "8";
                break;
        }
    }

    // entfernen aller Codes "0" ausser am Anfang
    $codelen = strlen($code);
    $num = array();
    $num = str_split($code);
    $phoneticcode = $num[0];


    for ($x = 1; $x < $codelen; $x++) {
        if ($num[$x] != "0") {
            $phoneticcode .= $num[$x];
        }
    }

    // Mehrfach Codes entfernen und Rückgabe
    // v1.1 (06.08.2010) Thorsten Gottlob <tgottlob@web.de>
    return preg_replace("/(.)\\1+/", "\\1", $phoneticcode);
}

/**
 * Measures the Damerau-Levenshtein distance of two words
 * 
 * @param string $str1
 * @param string $str2
 * @return number
 */
function damerauLevenshtein($str1, $str2) {
    $d = Array();

    $lenStr1 = strlen($str1);
    $lenStr2 = strlen($str2);

    if ($lenStr1 == 0)
        return $lenStr2;

    if ($lenStr2 == 0)
        return $lenStr1;

    for ($i = 0; $i <= $lenStr1; $i++) {
        $d[$i] = Array();
        $d[$i][0] = $i;
    }

    for ($j = 0; $j <= $lenStr2; $j++)
        $d[0][$j] = $j;

    for ($i = 1; $i <= $lenStr1; $i++) {
        for ($j = 1; $j <= $lenStr2; $j++) {
            $cost = substr($str1, $i - 1, 1) == substr($str2, $j - 1, 1) ? 0 : 1;

            $d[$i][$j] = min(
                    $d[$i - 1][$j] + 1, // deletion
                    $d[$i][$j - 1] + 1, // insertion
                    $d[$i - 1][$j - 1] + $cost          // substitution
            );

            if (
                    $i > 1 &&
                    $j > 1 &&
                    substr($str1, $i - 1, 1) == substr($str2, $j - 2, 1) &&
                    substr($str1, $i - 2, 1) == substr($str2, $j - 1, 1)
            )
                $d[$i][$j] = min(
                        $d[$i][$j], $d[$i - 2][$j - 2] + $cost          // transposition
                );
        }
    }
    return $d[$lenStr1][$lenStr2];
}

/**
 * brightness of color (RGB hex)
 * 
 * @param int $r
 * @param int $g
 * @param int $b
 * @return number 0 bis 255
 */
function brightness($r, $g, $b) {
    $r = hexdec($r);
    $g = hexdec($g);
    $b = hexdec($b);
    return (0.3 * $r + 0.59 * $g + 0.11 * $b);
}

/**
 * bright or dark font color infront of background
 * 
 * @param string $bgCol background color
 * @param string $darkCol dark font color as alternative
 * @param string $brightCol bright font color as alternative
 * @param int $border brightness border
 * @return $darkCol|$brightCol
 */
function fontColor($bgCol, $darkCol, $brightCol, $border = 122) {
    $bgCol = str_replace("#", "", $bgCol);
    $br = brightness(substr($bgCol, 0, 2), substr($bgCol, 2, 2), substr($bgCol, 4, 2));
    if ($br > $border) {
        return $darkCol;
    } else {
        return $brightCol;
    }
}

/**
 * autolink
 * from http://stackoverflow.com/questions/1959062/how-to-add-anchor-tag-to-a-url-from-text-input
 * 
 * @param string $text URL
 * @return string a-Tag
 */
function autolinktext($text) {
    $pattern = '#\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))#';
    $callback = create_function('$matches', '
   $url       = array_shift($matches);
   $url_parts = parse_url($url);

   $text = parse_url($url, PHP_URL_HOST) . parse_url($url, PHP_URL_PATH);
   $text = preg_replace("/^www./", "", $text);

   $last = -(strlen(strrchr($text, "/"))) + 1;
   if ($last < 0) {
       $text = substr($text, 0, $last) . "&hellip;";
   }

   return sprintf(\'<a href="%s">%s</a>\', $url, $text);
');

    return preg_replace_callback($pattern, $callback, $text);
}

/**
 * geo distance
 * 
 * @param float $lat1
 * @param float $lon1
 * @param float $lat2
 * @param float $lon2
 * @return float in meter
 */
function geoDistance($lat1, $lon1, $lat2, $lon2) {
    $R = 6378.137; // earth radius in Km
    $dLat = ($lat2 - $lat1) * M_PI / 180;
    $dLon = ($lon2 - $lon1) * M_PI / 180;
    $a = sin($dLat / 2) * sin($dLat / 2) + cos($lat1 * M_PI / 180) * cos($lat2 * M_PI / 180) * sin($dLon / 2) * sin($dLon / 2);
    $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
    $d = $R * $c;
    return $d * 1000;
}

/**
 * check for bitcoded value (max 15 digits)
 * 
 * @param int $dec e.g. 10010010
 * @param int $pointer reverse count
 * @return bool
 */
function getBitCodeVal($dec, $pointer) {
    return (int) (bool) ((int) $dec & pow(2, $pointer - 1));
}

/**
 * add a value (0 oder 1) bitcoded
 * 
 * @param int $dec e.g. 10010010
 * @param int $pointer reverse count
 * @param boolean $val
 * @return int
 */
function addBitCodeVal($dec, $pointer, $val) {
    if ($val) {
        return (int) $dec | pow(2, $pointer - 1);
    } else
        return (int) $dec & (65535 - pow(2, $pointer - 1));
}

/**
 * get version of ggLib
 * 
 * @return string
 */
function getversion() {
     return trim(file_get_contents('version'));
}
