-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: dockergg-mariadb
-- Erstellungszeit: 15. Jul 2022 um 19:40
-- Server-Version: 10.8.3-MariaDB-1:10.8.3+maria~jammy
-- PHP-Version: 8.0.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `libdev`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `createtime` datetime NOT NULL,
  `tablename` varchar(150) NOT NULL,
  `fieldname` varchar(150) NOT NULL,
  `refid` int(11) NOT NULL,
  `sequence` int(11) NOT NULL COMMENT 'regarding tablename, fieldname and refid',
  `newvalue` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `category` varchar(45) NOT NULL,
  `createtime` datetime NOT NULL,
  `payload` text DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `to` varchar(255) DEFAULT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lu_usergroup`
--

CREATE TABLE `lu_usergroup` (
  `id` int(11) NOT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lu_userrole`
--

CREATE TABLE `lu_userrole` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `name_en` varchar(45) NOT NULL,
  `name_de` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `preflang` varchar(4) DEFAULT NULL COMMENT 'ISO 639-1 code',
  `token` varchar(255) DEFAULT NULL,
  `tokenvaliduntil` datetime DEFAULT NULL,
  `forcepwchange` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user2group`
--

CREATE TABLE `user2group` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lu_usergroup_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user2role`
--

CREATE TABLE `user2role` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lu_userrole_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IX_USER` (`user_id`),
  ADD KEY `IX_REFID` (`refid`) USING BTREE,
  ADD KEY `IX_FIELD` (`tablename`,`fieldname`,`refid`) USING BTREE;

--
-- Indizes für die Tabelle `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IX_CAT` (`category`) USING BTREE,
  ADD KEY `IX_CAT_USER` (`category`,`user_id`) USING BTREE,
  ADD KEY `IX_CAT_TO` (`category`,`to`) USING BTREE,
  ADD KEY `IX_BCC` (`bcc`) USING BTREE,
  ADD KEY `IX_CC` (`cc`) USING BTREE,
  ADD KEY `IX_TO` (`to`) USING BTREE,
  ADD KEY `IX_USERID` (`user_id`) USING BTREE;

--
-- Indizes für die Tabelle `lu_usergroup`
--
ALTER TABLE `lu_usergroup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IX_IDPARENT` (`id_parent`) USING BTREE;

--
-- Indizes für die Tabelle `lu_userrole`
--
ALTER TABLE `lu_userrole`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `user2group`
--
ALTER TABLE `user2group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IX_USERID` (`user_id`) USING BTREE,
  ADD KEY `IX_GROUPID` (`lu_usergroup_id`) USING BTREE;

--
-- Indizes für die Tabelle `user2role`
--
ALTER TABLE `user2role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IX_USERID` (`user_id`) USING BTREE,
  ADD KEY `IX_ROLEID` (`lu_userrole_id`) USING BTREE;

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `lu_usergroup`
--
ALTER TABLE `lu_usergroup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `lu_userrole`
--
ALTER TABLE `lu_userrole`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `user2group`
--
ALTER TABLE `user2group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `user2role`
--
ALTER TABLE `user2role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints der Tabelle `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints der Tabelle `lu_usergroup`
--
ALTER TABLE `lu_usergroup`
  ADD CONSTRAINT `lu_usergroup_ibfk_1` FOREIGN KEY (`id_parent`) REFERENCES `lu_usergroup` (`id`);

--
-- Constraints der Tabelle `user2group`
--
ALTER TABLE `user2group`
  ADD CONSTRAINT `user2group_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user2group_ibfk_2` FOREIGN KEY (`lu_usergroup_id`) REFERENCES `lu_usergroup` (`id`);

--
-- Constraints der Tabelle `user2role`
--
ALTER TABLE `user2role`
  ADD CONSTRAINT `user2role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user2role_ibfk_2` FOREIGN KEY (`lu_userrole_id`) REFERENCES `lu_userrole` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
