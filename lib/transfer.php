<?php
namespace gg\lib;

use phpseclib\Net\SFTP;
use phpseclib\Crypt\RSA;

/**
 * ggLib CORE
 * files transfer
 * incl. info mail and zip functionality
 * 
 * uses: 7z
 *
 * class gg\transfer
 * @author Gerd
 */
class transfer {

    /**
     * see $sendSchemes
     * @var string
     */
    public $sendMethod = '';

    /**
     * if set send zipfile (optional)
     * @var string
     */
    public $zipFileName = '';

    /**
     * zip password (optional)
     * @var string
     */
    public $zipPass = '';

    /**
     * if set while using other than "emailatt", function as infomail
     * @var string
     */
    public $mail_to = '';

    /**
     * BCC
     * @var string
     */
    public $mail_bcc = '';

    /**
     * mail subject
     * @var string
     */
    public $mail_subj = '';

    /**
     * mail body (HTML with sendopt htmlMail))
     * @var string
     */
    public $mail_body = '';

    /**
     * scheme,host,port,path
     * @var string
     */
    public $url = '';

    /**
     * username
     * @var string
     */
    public $user = '';

    /**
     * password
     * @var string
     */
    public $pass = '';

    /**
     * full path privKeyFile
     * @var string
     */
    public $privKeyFile = '';

    /**
     * full path pubKeyFile
     * @var string
     */
    public $pubKeyFile = '';

    /**
     * passphrase privKeyFile
     * @var string
     */
    public $passPhrase = '';

    /**
     * OUTPUT: info as array
     * @var array
     */
    public $info = array();

    /**
     * OUTPUT: fallback info
     * @var boolean|string
     */
    public $fallback = false;

    /**
     * OUTPUT: transfered files
     * @var array
     * for uploads: array(fieldname => array(index => array("name" => filename, "path" => path)))
     */
    public $transFiles = array();
    
    private $ftpStandardSleep = 200000; // microseconds
    private $fileArray = array(); // filenames for sending (filename for recipient => local filepath)
    private $sendSchemes = array(
        'upload' => array(),
        'download' => array(),
        'move' => array(),
        'emailatt' => array(),
        'ftp' => array('ftp', 21),
        'ftps' => array('ftp', 21),
        'sftp' => array('sftp', 22),
        'sftpkey' => array('sftp', 22),
        'scp' => array('scp', 22),
        'pportal' => array(),
        'httpPut' => array('http', 80),
        'httpPost' => array('http', 80),
        'webDav' => array('dav', 80),
        'rsync' => array('ssh', 22)
    );
    private $curlOpts = array( // https://www.php.net/manual/de/function.curl-setopt.php
        CURLOPT_UPLOAD => 1,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_MAXREDIRS => 2,
        CURLOPT_CONNECTTIMEOUT => 10,
    );
    private $sendOpts = array(
        'filesRemain' => false,
        'downloadForce' => false, // force download dialog (no automatic dealing with mimetype)
        'htmlMail' => false,
        'directMail' => false, // instant mailing (not using DB mailqueue)
        'useCurl' => false,
        'useCmdCurl' => false, // curl on cli
        'passiveFTP' => false,
        'okFile' => false,
        'use7z' => false
    );
    private $urlComponents = array(
        'scheme' => '',
        'host' => '',
        'port' => '',
        ':port' => '',
        'user' => '', // not used
        'pass' => '', // not used
        'path' => '',
        'query' => '',
        'fragment' => ''
    );

    /**
     * @param string $sendMethod s. $sendSchemes
     */
    public function __construct($sendMethod = 'download') {
        
        require_once CFG_LIBDIR . 'email.php';
        
        $this->sendMethod = $sendMethod;
    }

    /**
     * @return array
     */
    public function getAvailableSendSchemes() {
        return $this->sendSchemes;
    }

    /**
     * @return array
     */
    public function getSendOpts() {
        return $this->sendOpts;
    }

    /**
     * @param string $optKey
     */
    public function enableSendOpt($optKey) {
        $this->sendOpts[$optKey] = true;
    }
    
    /**
     * @param string $optkey
     * @param mixed $optvalue
     * @param boolean $overwrite
     */
    public function addCurlOpt($optkey, $optvalue, $overwrite = false) {
        if (!(!$overwrite && isset($this->curlOpts[$optkey])))
            $this->curlOpts[$optkey] = $optvalue;
    }

    /**
     * add data to send list
     * (for upload: $filename = inputname, $data = destination folder)
     * @param string $fileName destination name
     * @param string $data data or full path file
     */
    public function addData($fileName, $data) {
        if (isset($this->sendSchemes[$this->sendMethod])) {
            if (($this->sendMethod != 'upload' && is_file($data)) ||
                    ($this->sendMethod == 'upload' && is_dir($data))) {
                $this->fileArray[$fileName] = $data;
            } elseif ($this->sendMethod != 'upload' && !is_file($data) && $data) {
                $tempFile = tempnam(CFG_TEMPDIR, 'SFS');
                if (file_put_contents($tempFile, $data) !== false) {
                    $this->fileArray[$fileName] = $tempFile;
                }
            }
        }
    }

    /**
     * transfer as defined
     * @return boolean
     */
    public function process() {
        if (!$this->prepareFiles()) {
            return false;
        }

        $this->getUrlComponents();

        $methodname = 'process_' . strtolower($this->sendMethod);

        if (method_exists(__CLASS__, $methodname)) {
            if (!$this->$methodname()) {
                return false;
            }
            $this->delFiles();
        } else {
            $this->delFiles(true);
        }
        return true;
    }

    // ------------ TRANSFER METHODS -------------------
    private function process_upload() {
        if (isset($_FILES)) {
            $i = 0;
            foreach ($_FILES as $inputname => $upArr) {
                if (isset($this->fileArray[$inputname])) {
                    // Input-Array: <input type='file' name='myfile[]'>
                    if (is_array($upArr['error'])) {
                        foreach ($upArr['error'] as $key => $error) {
                            if ($upArr['name'][$key]) {
                                $uq = uniqid($i) . '.';
                                $uFilePath = \gg\combinePathFile($this->fileArray[$inputname], $uq . $upArr['name'][$key]);
                                $this->transFiles[$inputname][$key]['name'] = $upArr['name'][$key];
                                $this->transFiles[$inputname][$key]['path'] = $uFilePath;

                                if ($error == UPLOAD_ERR_OK) {
                                    if (!move_uploaded_file($upArr['tmp_name'][$key], $uFilePath)) {
                                        return $this->fail('Error3b at Upload');
                                    }
                                } else {
                                    return $this->fail('Error2b at Upload (' . $error . ')');
                                }
                                $i++;
                            } else {
                                $this->transFiles[$inputname][$key]['name'] = '';
                                $this->transFiles[$inputname][$key]['path'] = '';
                            }
                        }
                    } elseif ($upArr['error'] == UPLOAD_ERR_OK) {
                        if ($upArr['name']) {
                            $uq = uniqid($i) . '.';
                            $uFilePath = \gg\combinePathFile($this->fileArray[$inputname], $uq . $upArr['name']);
                            $this->transFiles[$inputname][0]['name'] = $upArr['name'];
                            $this->transFiles[$inputname][0]['path'] = $uFilePath;

                            if (!move_uploaded_file($upArr['tmp_name'], $uFilePath)) {
                                return $this->fail('Error3a at Upload');
                            }
                            $i++;
                        } else {
                            $this->transFiles[$inputname][0]['name'] = '';
                            $this->transFiles[$inputname][0]['path'] = '';
                        }
                    } else {
                        return $this->fail('Error2a at Upload ('. $upArr['error'] . ')');
                    }
                } else {
                    return $this->fail('Error1 at Upload');
                }
            }
        } else
            return $this->fail('no data for Upload');

        $this->info[] = 'OK - ' . $i . ' file(s) uploaded';

        if ($this->mail_to) {
            if (!$this->email()) {
                return false;
            }
        }
        return true;
    }

    private function process_download() {
        $more = (count($this->fileArray) > 1);
        if (!$this->prepareFiles($more)) {
            return false;
        }

        $fName = key($this->fileArray);
        $fPath = current($this->fileArray);

        if ($this->mail_to) {
            if (!$this->email()) {
                return false;
            }
        }
        $this->transFiles = $this->fileArray;

        ob_end_clean();

        header('Content-Description: File Transfer');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $finfo = finfo_open(FILEINFO_MIME);
        header('Content-Type: ' . finfo_file($finfo, $fPath));
        //header('Content-Type: application/octet-stream');
        finfo_close($finfo);

        if ($this->sendOptVal('downloadForce')) {
            header('Content-Disposition: attachment; filename="'. $fName . '"');
        } else {
            header('Content-Disposition: inline; filename="' . $fName . '"');
        }

        header('Content-Length: ' . filesize($fPath));

        readfile($fPath);

        $this->delFiles();

        exit;
    }

    private function process_move() {
        foreach ($this->fileArray as $fName => $fPath) {
            if (!copy($fPath, \gg\combinePathFile($this->url, $fName))) {
                return $this->fail('Error at Move');
            }
        }
        $this->delFiles();

        if ($this->mail_to) {
            if (!$this->email()) {
                return false;
            }
        }
        return true;
    }

    private function process_emailatt() {
        if ($this->mail_to) {
            if (!$this->email(true)) {
                return false;
            }
            $this->info[] = 'OK - ' . count($this->fileArray) . ' file(s) mailt';
            $this->transFiles = $this->fileArray;
        } else {
            return $this->fail('no mailto found');
        }
        return true;
    }

    private function process_ftp() {
        foreach ($this->fileArray as $fName => $fPath) {
            if ($this->sendOptVal('useCurl')) {
                $this->addCurlOpt(CURLOPT_PROTOCOLS, CURLPROTO_FTP, true);
                if (!$this->sendCurl($fName, $fPath)) {
                    return false;
                }
            } elseif ($this->sendOptVal('useCmdCurl')) {
                if (!$this->sendCmdCurl($fName, $fPath)) {
                    return false;
                }
            } else {
                $fbacktxt = '';
                $ch = ftp_connect($this->urlComponents['host'], $this->urlComponents['port']);
                if ($ch == false) {
                    return $this->fail('Error at FTP(nativ): no connection');
                }
                if (!ftp_login($ch, $this->user, $this->pass)) {
                    return $this->fail('Error at FTP(nativ): Login fail');
                }
                if ($this->sendOptVal('passiveFTP')) {
                    ftp_pasv($ch, true);
                }
                if ($this->urlComponents['path']) {
                    if (!ftp_chdir($ch, substr($this->urlComponents['path'], 1))) {
                        // Fallback chdir
                        $this->fallback = 'Error at FTP(nativ): chdir error, try fallback';
                        if (!ftp_chdir($ch, $this->urlComponents['path'])) {
                            return $this->fail('Error at FTP(nativ): chdir error');
                        }
                    }
                }
                if (!ftp_put($ch, $fName, $fPath, FTP_BINARY)) {
                    return $this->fail('Error at FTP(nativ): no transfer');
                }
                if ($this->sendOptVal('okFile')) {
                    if (ftp_nb_put($ch, $fName . '.OK', \gg\combinePathFile(CFG_TEMPDIR, 'ok.txt'), FTP_BINARY) == FTP_FAILED) {
                        return $this->fail('Error at FTP(nativ): no OK-file transfer');
                    }
                }
                ftp_close($ch);
            }
            usleep($this->ftpStandardSleep);
        }
        $this->info[] = 'OK - ' . count($this->fileArray) . ' file(s) transfered';
        $this->transFiles = $this->fileArray;

        if ($this->mail_to) {
            if (!$this->email()) {
                return false;
            }
        }
        return true;
    }

    private function process_ftps() {
        foreach ($this->fileArray as $fName => $fPath) {
            if ($this->sendOptVal('useCurl')) {
                $this->addCurlOpt(CURLOPT_PROTOCOLS, CURLPROTO_FTP, true);
                $this->addCurlOpt(CURLOPT_FTP_SSL, CURLOPT_FTPSSLAUTH, true);

                if (!$this->sendCurl($fName, $fPath)) {
                    return false;
                }
            } elseif ($this->sendOptVal('useCmdCurl')) {
                if (!$this->sendCmdCurl($fName, $fPath)) {
                    return false;
                }
            } else {
                $ch = ftp_ssl_connect($this->urlComponents['host'], $this->urlComponents['port']);
                if ($ch == false) {
                    return $this->fail('Error at FTPS(nativ): no connection');
                }
                if (!ftp_login($ch, $this->user, $this->pass)) {
                    return $this->fail('Error at FTPS(nativ): Login fail');
                }
                if ($this->sendOptVal('passiveFTP')) {
                    ftp_pasv($ch, true);
                }
                if ($this->urlComponents['path']) {
                    if (!ftp_chdir($ch, substr($this->urlComponents['path'], 1))) {
                        // Fallback chdir
                        $this->fallback = 'Error at FTPS(nativ): chdir error, try fallback';
                        if (!ftp_chdir($ch, $this->urlComponents['path'])) {
                            return $this->fail('Error at FTPS(nativ): chdir error');
                        }
                    }
                }
                if (!ftp_put($ch, $fName, $fPath, FTP_BINARY)) {
                    return $this->fail('Error at FTPS(nativ): no transfer');
                }
                if ($this->sendOptVal('okFile')) {
                    if (ftp_nb_put($ch, $fName . '.OK', \gg\combinePathFile(CFG_TEMPDIR, 'ok.txt'), FTP_BINARY) == FTP_FAILED) {
                        return $this->fail('Error at FTPS(nativ): no OK-file-transfer');
                    }
                }
                ftp_close($ch);
            }
            usleep($this->ftpStandardSleep);
        }
        $this->info[] = 'OK - ' . count($this->fileArray) . ' file(s) transfered';
        $this->transFiles = $this->fileArray;

        if ($this->mail_to) {
            if (!$this->email()) {
                return false;
            }
        }
        return true;
    }

    private function process_sftp() {
        foreach ($this->fileArray as $fName => $fPath) {
            if ($this->sendOptVal('useCurl')) {
                $this->addCurlOpt(CURLOPT_PROTOCOLS, CURLPROTO_SFTP, true);

                if (!$this->sendCurl($fName, $fPath)) {
                    return false;
                }
            } elseif ($this->sendOptVal('useCmdCurl')) {
                if (!$this->sendCmdCurl($fName, $fPath)) {
                    return false;
                }
            } else {
                $sftp = new SFTP($this->urlComponents['host'], $this->urlComponents['port']);
                if (!$sftp->login($this->user, $this->pass)) {
                    return $this->fail('Error at SFTP(Net): Login fail');
                }
                if ($this->urlComponents['path']) {
                    if (!$sftp->chdir(substr($this->urlComponents['path'], 1))) {
                        // Fallback chdir
                        $this->fallback = 'Error at SFTP(Net): chdir error, try fallback';
                        if (!$sftp->chdir($this->urlComponents['path'])) {
                            return $this->fail('Error at SFTP(Net): chdir error');
                        }
                    }
                }
                if (!$sftp->put($fName, file_get_contents($fPath))) {
                    return $this->fail('Error at SFTP(Net): no transfer: ' . $sftp->getLastSFTPError());
                }
                if ($this->sendOptVal('okFile')) {
                    if (!$sftp->put($fName . '.OK', ':-)')) {
                        return $this->fail('Error at SFTP(Net): no OK-file-transfer');
                    }
                }
            }
            usleep($this->ftpStandardSleep);
        }
        $this->info[] = 'OK - ' . count($this->fileArray) . ' file(s) transfered';
        $this->transFiles = $this->fileArray;

        if ($this->mail_to) {
            if (!$this->email()) {
                return false;
            }
        }
        return true;
    }

    private function process_sftpkey() {
        if (!file_exists($this->privKeyFile)) {
            return $this->fail('Error at SFTP-KEY: not found PrivateKey-file');
        }

        foreach ($this->fileArray as $fName => $fPath) {
            if ($this->sendOptVal('useCurl')) {
                $this->addCurlOpt(CURLOPT_PROTOCOLS, CURLPROTO_SFTP, true);
                $this->addCurlOpt(CURLOPT_SSH_AUTH_TYPES, CURLSSH_AUTH_PUBLICKEY, true);
                $this->addCurlOpt(CURLOPT_SSH_PRIVATE_KEYFILE, $this->privKeyFile, true);
                $this->addCurlOpt(CURLOPT_SSH_PUBLIC_KEYFILE, $this->pubKeyFile, true);

                if (!$this->sendCurl($fName, $fPath)) {
                    return false;
                }
            } elseif ($this->sendOptVal('useCmdCurl')) {
                if (!$this->sendCmdCurl($fName, $fPath)) {
                    return false;
                }
            } else {
                $sftp = new SFTP($this->urlComponents['host'], $this->urlComponents['port']);
                $key = new RSA();
                if ($this->passPhrase) {
                    $key->setPassword($this->passPhrase);
                }
                $privKey = file_get_contents($this->privKeyFile);
                if ($privKey === false) {
                    return $this->fail('Error at SFTP-KEY(Net): PrivateKey-File could not be opened');
                }
                $key->loadKey($privKey);
                if (!$sftp->login($this->user, $key)) {
                    return $this->fail('Error at SFTP-KEY(Net): Login fail');
                }
                if ($this->urlComponents['path']) {
                    if (!$sftp->chdir(substr($this->urlComponents['path'], 1))) {
                        // Fallback chdir
                        $this->fallback = 'Error at SFTP-KEY(Net): chdir error, try fallback';

                        if (!$sftp->chdir($this->urlComponents['path'])) {
                            return $this->fail('Error at SFTP-KEY(Net): chdir Fehler');
                        }
                    }
                }
                if (!$sftp->put($fName, file_get_contents($fPath))) {
                    return $this->fail('Error at SFTP-KEY(Net): no transfer: ' . $sftp->getLastSFTPError());
                }
                if ($this->sendOptVal('okFile')) {
                    if (!$sftp->put($fName . '.OK', ':-)')) {
                        return $this->fail('Error at SFTP-KEY(Net): no OK-file-transfer');
                    }
                }
            }
            usleep($this->ftpStandardSleep);
        }
        $this->info[] = 'OK - ' . count($this->fileArray) . ' file(s) transfered';
        $this->transFiles = $this->fileArray;

        if ($this->mail_to) {
            if (!$this->email()) {
                return false;
            }
        }
        return true;
    }

    private function process_pportal() {
        // only message via  email
        if ($this->mail_to) {
            if (!$this->email()) {
                return false;
            }
        } else {
            return $this->fail('no mailto found');
        }
        return true;
    }

    private function process_scp() {
        // not implemented yet
        return false;
    }

    private function process_httpput() {
        // not implemented yet
        return false;
    }

    private function process_httppost() {
        // not implemented yet
        return false;
    }

    private function process_webdav() {
        // not implemented yet
        return false;
    }

    private function process_rsync() {
        // not implemented yet
        return false;
    }

    // -----------------------------------------------
    private function getUrlComponents() {
        $url = trim($this->url);
        $stdScheme = '';

        if (isset($this->sendSchemes[$this->sendMethod][0]) && $this->sendSchemes[$this->sendMethod][0]) {
            $stdScheme = $this->sendSchemes[$this->sendMethod][0];
            if (stripos($url, $stdScheme . '://') !== 0) {
                $url = $stdScheme . '://' . $url;
            }
        }
        $this->urlComponents = array_replace($this->urlComponents, parse_url($url));

        if (isset($this->sendSchemes[$this->sendMethod][1]) && $this->sendSchemes[$this->sendMethod][1]) {
            if (!$this->urlComponents['port']) {
                $this->urlComponents['port'] = $this->sendSchemes[$this->sendMethod][1];
            }
        }
        if ($this->urlComponents['port']) {
            $this->urlComponents[':port'] = ':' . $this->urlComponents['port'];
        }
    }

    private function prepareFiles($zipForce = false) {
        if (!empty($this->fileArray)) {
            if ($this->zipFileName || $zipForce) {
                if (!$this->zip()) {
                    return false;
                }
            }
            return true;
        } elseif ($this->sendMethod == 'pportal') {
            return true;
        }
        return $this->fail('no files for transfer');
    }

    private function sendCurl($fName, $fPath) {
        $u = ($this->user && !$this->pass) ? $this->user . ':@' : '';
        $this->addCurlOpt(CURLOPT_URL, $this->urlComponents['scheme'] . '://'
                . $u
                . $this->urlComponents['host']
                . \gg\combinePathFile($this->urlComponents['path'], $fName), true);
        $fh = fopen($fPath, 'r');
        $this->addCurlOpt(CURLOPT_INFILE, $fh, true);
        $this->addCurlOpt(CURLOPT_INFILESIZE, filesize($fPath), true);
        $this->addCurlOpt(CURLOPT_SSL_VERIFYHOST, false, true);
        $this->addCurlOpt(CURLOPT_SSL_VERIFYPEER, false, true);
        $this->addCurlOpt(CURLOPT_PORT, $this->urlComponents['port'], true);
        if ($this->user && $this->pass) {
            $this->addCurlOpt(CURLOPT_USERPWD, $this->user . ':' . $this->pass, true);
        }
        $ch = curl_init();

        if (!curl_setopt_array($ch, $this->curlOpts)) {
            $ret = $this->fail('Error at ' . strtoupper($this->sendMethod) . '(Curl), not set option: ' . curl_error($ch));
        } elseif (curl_exec($ch)) {
            $this->info[] = curl_getinfo($ch);
            $ret = true;
        } else {
            $ret = $this->fail('Error at ' . strtoupper($this->sendMethod) . '(Curl): ' . curl_error($ch));
        }

        curl_close($ch);
        fclose($fh);

        return $ret;
    }

    private function sendCmdCurl($fName, $fPath) {
        $cmd = 'curl --insecure --anyauth -u ' . escapeshellarg($this->user) . ':' . escapeshellarg($this->pass) . ' -T ' . escapeshellarg($fPath) . ' '
                . escapeshellarg($this->urlComponents['scheme']) . '://' . escapeshellarg($this->urlComponents['host']) . escapeshellarg($this->urlComponents[':port'])
                . escapeshellarg(\gg\combinePathFile($this->urlComponents['path'], $fName)) . ' 2>&1';
        exec($cmd, $out, $ret);

        if ($ret > 0) {
            $t = array_pop($out);
            $tarr = explode(':', $t, 100);
            $t = array_pop($tarr);
            return $this->fail('Error at ' . strtoupper($this->sendMethod) . '(CurlCmd): Error-Code: ' .$ret . ', message: ' . $t);
        }
        return true;
    }

    private function email($sendAttachments = false) {
        if ($this->sendOptVal('directMail')) {
            $objEmail = new email();
        } else {
            $this->enableSendOpt('filesRemain');
            $objEmail = new emailToQueue();
        }
        $objEmail->addTo($this->mail_to);
        $objEmail->addBCC($this->mail_bcc);
        if ($sendAttachments && !empty($this->fileArray)) {
            foreach ($this->fileArray as $k => $v) {
                $objEmail->addAttachment($v, $k, !$this->sendOptVal('filesRemain'));
            }
        }
        if ($objEmail->send($this->mail_subj, $this->mail_body, $this->sendOptVal('htmlMail'))) {
            return true;
        } else {
            return $this->fail('Email not sent: ' . $objEmail->getInfo());
        }
    }

    private function zip() {
        if ($this->sendMethod != 'upload') { // ignore zip setting at upload
            $zpName = ($this->zipFileName) ? $this->zipFileName : 'files.zip';
            if (strtolower(substr($zpName, -4)) != '.zip') {
                $zpName .= '.zip';
            }
            if ($this->sendOptVal('use7z')) {
                $zp = \gg\combinePathFile(CFG_TEMPDIR, uniqid() . '_' . $zpName);
                $tmpDir = \gg\combinePathFile(CFG_TEMPDIR, 'zipTmpDir_' . uniqid());
                mkdir($tmpDir);
                $files = '';
                foreach ($this->fileArray as $fname => $path) {
                    copy($path, \gg\combinePathFile($tmpDir, $fname));
                }
                $pass = ($this->zipPass) ? '-p' . $this->zipPass . ' -mem=AES256 ' : '';
                exec('7z a ' . $pass . ' -tzip "' . escapeshellarg($zp) . '" "' . escapeshellarg($tmpDir) . '/*"', $ret, $errcode);
                exec('rm -rf "' . escapeshellarg($tmpDir) . '"');
                if ($errcode !== 0 || (!is_file($zp))) {
                    return $this->fail('7z Zip file fail: ' . $ret);
                }
            } else {
                $zp = createZip($this->fileArray, false, $this->zipPass);
                if (!zp) {
                    return $this->fail('Zip file fail');
                }
            }
            if ($this->zipFileName) {
                $this->delFiles(true);
            }
            $this->fileArray = array($zpName => $zp);
        }
        return true;
    }

    private function delFiles($force = false) {
        if ((!$this->sendOptVal('filesRemain') || $force) && $this->sendMethod != 'upload') {
            foreach ($this->fileArray as $path) {
                if (is_file($path)) {
                    @unlink($path);
                }
            }
        }
    }

    private function sendOptVal($option) {
        if (isset($this->sendOpts[$option])) {
            return $this->sendOpts[$option];
        }
        return false;
    }

    private function fail($msg) {
        $this->info[] = $msg;
        $this->delFiles();
        return false;
    }
}
