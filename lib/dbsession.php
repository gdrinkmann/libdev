<?php
namespace gg\lib;

/**
 * ggLib CORE
 */

// todo (from ggmod):

function sess_open($sess_path, $sess_name) {
	return true;
}
function sess_close() {
	return true;
}
function sess_read($sess_id) {
	global $ggmylink, $gg_sessvalfield, $gg_sesstbl, $gg_sessidfield, $gg_sesstsfield;
	$sql = "SELECT ".$gg_sessvalfield." FROM ".$gg_sesstbl." WHERE ".$gg_sessidfield." = '".mysql_real_escape_string($sess_id)."';";
	$result = @mysql_query($sql, $ggmylink);
	$CurrentTime = time();
	if (!mysql_num_rows($result)) {
		@mysql_query("INSERT INTO ".$gg_sesstbl." (".$gg_sessidfield.", ".$gg_sesstsfield.") VALUES ('".$sess_id."', ".$CurrentTime.");", $ggmylink);
		return '';
	} else {
		@mysql_query("UPDATE ".$gg_sesstbl." SET ".$gg_sesstsfield." = ".$CurrentTime." WHERE ".$gg_sessidfield." = '".$sess_id."';", $ggmylink);
		$row = mysql_fetch_assoc($result);
		return $row[$gg_sessvalfield];
	}
}
function sess_write($sess_id, $data) {
	global $ggmylink, $gg_sesstbl, $gg_sessidfield, $gg_sesstsfield, $gg_sessvalfield;
	$CurrentTime = time();
	@mysql_query("UPDATE ".$gg_sesstbl." SET ".$gg_sessvalfield." = '".$data."', ".$gg_sesstsfield." = ".$CurrentTime." WHERE ".$gg_sessidfield." = '".$sess_id."';", $ggmylink);
	return true;
}
function sess_destroy($sess_id) {
	global $ggmylink, $gg_sesstbl, $gg_sessidfield;
	@mysql_query("DELETE FROM ".$gg_sesstbl." WHERE ".$gg_sessidfield." = '".$sess_id."';", $ggmylink);
	return true;
}
function sess_gc($sess_maxlifetime) {
	global $ggmylink, $gg_sesstbl, $gg_sesstsfield;
	$CurrentTime = time();
	@mysql_query("DELETE FROM ".$gg_sesstbl." WHERE (".$gg_sesstsfield." + ".$sess_maxlifetime.") < ".$CurrentTime.";", $ggmylink);
	return true;
}

session_set_save_handler('sess_open', 'sess_close', 'sess_read', 'sess_write', 'sess_destroy', 'sess_gc');


