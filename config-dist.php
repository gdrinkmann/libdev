<?php
namespace gg\lib;
/**
 * gglib CORE
 * @author Gerd
 * 
 * configuration for prototyp project LibDev
 * (rename this file to config.php)
 */

// geneneral behaviour
const CFG_DEVMODUS = true;

const CFG_MAINTENANCE = false;
const CFG_MAINTENANCEEXCEPTIONS = array(); // array of allowed IPs

// project settings
const CFG_PROJECTNAME = 'LibDev';
const CFG_INITLANGUAGE = 'de'; // see class gg/str
const CFG_STARTPAGE = 'start';
const CFG_LOGINPAGE = 'core/login';
const CFG_AUTOLOGOUTTIME = 600; // ensure session.gc_maxlifetime and session.cookie_lifetime are higher
const CFG_BASEURI= 'http://libdev.local';
const CFG_IMAGEURI = CFG_BASEURI.'/image/';

const CFG_WEBROOT = '/var/www/libdev/'; // local test maybe path in docker container
const CFG_TEMPDIR = CFG_WEBROOT.'temp/';
const CFG_LIBDIR = CFG_WEBROOT.'lib/';
const CFG_STRDIR = CFG_WEBROOT.'str/';
const CFG_LOGDIR = CFG_WEBROOT.'log/';
const CFG_TASKDIR = CFG_WEBROOT.'task/';

// MVC
const CFG_MODELDIR = CFG_WEBROOT . 'model/';
const CFG_CONTROLLERDIR = CFG_WEBROOT . 'controller/';
const CFG_VIEWDIR = CFG_WEBROOT . 'view/';
const CFG_TEMPLATEDIR = CFG_WEBROOT.'template/';

// database
const CFG_DBHOST = '';
const CFG_DBPORT = 3306;
const CFG_DBNAME = '';
const CFG_DBUSER = '';
const CFG_DBPW = '';
const CFG_DBCHARSET = 'utf8';
const CFG_PRIMARYKEYNAME = 'id';
const CFG_NAMEFIELD = 'name';
const CFG_DELETEDFIELD = 'deleted';

// email
const CFG_SMTPSERVER = ''; // local test maybe 'host.docker.internal' or 'localhost'
const CFG_SMTPPORT = 25;
const CFG_SMTPUSER = '';
const CFG_SMTPPW = '';
const CFG_SMTPFROM = 'noreply@libdev.local';
const CFG_SYSSIGNATURE = "John Doe\r\nCastrop-Rauxel"; // linebreak \r\n
const CFG_WEBMASTEREMAIL = 'j.doe@example.com';

// php ini settings
if (CFG_DEVMODUS) {
    error_reporting(E_ALL);
} else {
    error_reporting(E_ALL ^ (E_NOTICE | E_STRICT | E_DEPRECATED));
}
set_time_limit(120);
ini_set('memory_limit', '500M');
session_set_cookie_params([
    'lifetime' => 0,
    'path' => '/',
    'domain' => null,
    'secure' => !CFG_DEVMODUS,
    'httponly' => true,
    'samesite' => 'lax']);
