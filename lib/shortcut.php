<?php
namespace gg;

/**
 * ggLib CORE
 * shortcut functions in gg namespace
 * 
 * standard included in index.php
 * 
 * gglib shortcut functions
 * @author Gerd
 */

/**
 * shortcut for str::get
 * @param string $key
 * @param string $scope
 * @param array $replaceArray
 * @param string $tmplang
 * @return string
 */
function _t($key, $scope = null, $replaceArray = null, $tmplang = null) {
    return lib\str::get($key, $scope, $replaceArray, $tmplang);
}

/**
 * shortcut for request::redirect
 * @param string $redirect raw path without extension
 * @param array $queryArr
 * @param string $fragment
 */
function _re($redirect, $queryArr = array(), $fragment = '') {
    lib\request::redirect($redirect, $queryArr, $fragment);
}

/**
 * shortcut for request::getRequest($_GET,..)
 * @param string $key
 * @param array $ruleArr
 * @param mixed $defaultValue if set request is optional
 * @return mixed
 */
function _get($key, $ruleArr, $defaultValue = null) {
    return lib\request::getRequest($_GET, $key, $ruleArr, $defaultValue);
}

/**
 * shortcut for request::getRequest($_POST,..)
 * @param string $key
 * @param mixed $defaultValue if set request is optional
 * @return mixed
 */
function _post($key, $defaultValue = null) {
    return lib\request::getRequest($_POST, $key, null, $defaultValue);
}

/**
 * shortcut for request::buildUrl
 * @param string $pageName
 * @param array $queryArr
 * @param string $fragment
 * @return mixed url
 */
function _u($pageName, $queryArr = array(), $fragment = '') {
    return lib\request::buildUrl($pageName, $queryArr, $fragment);
}

/**
 * shortcut for page::setMessage
 * @param string $message
 * @param int $prio const lib\PRIO_*
 * @param bool $clearBefore
 */
function _m($message, $prio = 0, $clearBefore = false) {
    lib\page::setMessage($message, $prio, $clearBefore);
}

/**
 * shortcut for log::toSAPI
 * @param string $message
 */
function _trc($message) {
    lib\log::toSAPI($message);
}

/**
 * shortcut for str::getLang
 * @return string
 */
function _lang() {
    return lib\str::getLang();
}

/**
 * shortcut for print_r
 * @param mixed $variable
 */
function _pr($variable) {
    echo '<pre>' . print_r($variable, true) . '</pre>';
}