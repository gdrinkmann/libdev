<?php
namespace gg;
/**
 * ggLib sample
 * Bootstrap 5
 */
?>
<!-- START nav.include -->
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            
            <div class="container-fluid">

            <a id="navbar-brand" class="navbar-brand" href="<?= lib\CFG_BASEURI ?>">
                <img src="<?= lib\CFG_IMAGEURI ?>logo-libdev-192x192.png" alt="Logo" style="width:35px;" />
            </a>

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a id="page_index" class="nav-link" href="<?= lib\CFG_BASEURI ?>">
                        <?= lib\CFG_PROJECTNAME ?> 
                    </a>
                </li>
            </ul>
                
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav w-100 justify-content-center">
                    
                    <li class="nav-item">
                        <a class="nav-link <?= ($PAGE->getName() == lib\CFG_STARTPAGE) ? 'active' : '' ?>" <?= ($PAGE->getName() == lib\CFG_STARTPAGE) ? 'aria-current="page"' : '' ?> href="<?= lib\CFG_BASEURI ?>">
                            <i class="bi bi-house-door-fill"></i> <?= _t('start', 'nav') ?> 
                        </a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle <?= (in_array($PAGE->getName(), ['showcase/transferfiles'])) ? 'active' : '' ?>" <?= (in_array($PAGE->getName(), ['showcase/transferfiles'])) ? 'aria-current="page"' : '' ?> href="#" id="navbarDropdown3" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="bi bi-wrench"></i> Showcase
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown3">
                            <li>
                                <a class="dropdown-item" href="<?= lib\CFG_BASEURI ?>/showcase/transferfiles.html">
                                    Transfer Files
                                </a>
                            </li>
                        </ul>
                    </li>
                    
<?php if (true || in_array('admin', $PAGE->getRoles())): ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle <?= (in_array($PAGE->getName(), ['core/listuser'])) ? 'active' : '' ?>" <?= (in_array($PAGE->getName(), ['core/listuser'])) ? 'aria-current="page"' : '' ?> href="#" id="navbarDropdown4" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="bi bi-gear-fill"></i> <?= _t('admin', 'nav') ?> 
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown4">
                            <li>
                                <a class="dropdown-item" href="<?= lib\CFG_BASEURI ?>/core/listuser.html">
                                    <?= _t('listuser', 'nav') ?> 
                                </a>
                            </li>
                        </ul>
                    </li>
<?php endif ?>

<?php if (lib\getCurrentUser()): ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle <?= (in_array($PAGE->getName(), ['profile'])) ? 'active' : '' ?>" <?= (in_array($PAGE->getName(), ['profile'])) ? 'aria-current="page"' : '' ?> href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="bi bi-person-fill"></i> <em><?= lib\getCurrentUser()->name ?></em>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li>
                                <a class="dropdown-item" href="<?= lib\CFG_BASEURI ?>/core/changepw.html">
                                    <?= _t('changepw', 'nav') ?> 
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="<?= lib\CFG_BASEURI ?>/core/profile.html">
                                    <?= _t('profile', 'nav') ?> 
                                </a>
                            </li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li>
                                <a class="dropdown-item" href="<?= lib\CFG_BASEURI ?>/core/logout.html">
                                    <?= _t('logout', 'nav') ?> 
                                </a>
                            </li>
                        </ul>
                    </li>
<?php else: ?>
                    <li class="nav-item">
                        <a class="nav-link <?= ($PAGE->getName() == lib\CFG_LOGINPAGE) ? 'active' : '' ?>"  <?= ($PAGE->getName() == lib\CFG_LOGINPAGE) ? 'aria-current="page"' : '' ?> href="<?= lib\CFG_BASEURI ?>/<?= lib\CFG_LOGINPAGE ?>.html">
                            <i class="bi bi-person-fill"></i> <?= _t('login', 'nav') ?> 
                        </a>
                    </li>
<?php endif ?>
                </ul>
                
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="bi bi-translate"></i> <?= _t('language', 'general') ?> 
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown2">
                            <li>
                                <a class="dropdown-item" href="<?= _u($PAGE->getName(), array_merge($_GET, ['lang'=>'de'])) ?>">
                                    <?= _t('lang_de', 'general', null, 'de') ?><?php if(_lang() === 'de'): ?> <i class="bi bi-check"></i><?php endif ?> 
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="<?= _u($PAGE->getName(), array_merge($_GET, ['lang'=>'en'])) ?>">
                                    <?= _t('lang_en', 'general', null, 'en') ?><?php if(_lang() === 'en'): ?> <i class="bi bi-check"></i><?php endif ?> 
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            </div>
        </nav>
<!-- END nav.include -->
