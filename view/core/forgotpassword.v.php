<?php
namespace gg;
/**
 * ggLib sample
 * Bootstrap 5
 */
?>
<h5 class="card-header d-flex">
    <?= _t('head_forgotpassword', 'user') ?> <a href="<?= lib\CFG_BASEURI ?>" class="card-link ms-auto"><i class="bi bi-house-door-fill"></i></a>
</h5>
<div class="card-body">
    <p class="card-text"><?= _t('txt_resetpasswordmail', 'user') ?></p>
<?= $PAGE->form()->getTotalHtml('A') ?>
    <hr>
    <p class="card-text">
        <small><a href="/core/register.html" class="card-link"><i class="bi bi-pencil-fill"></i> <?= _t('lnk_register', 'user') ?></a></small>
        <br>
        <small><a href="/core/login.html" class="card-link"><i class="bi bi-person-fill"></i> <?= _t('lnk_login', 'user') ?></a></small>
    </p>
</div>
