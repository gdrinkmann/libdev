<?php
namespace gg;
/**
 * ggLib sample
 * Bootstrap 5
 */
?>
<!-- START carousel.include  -->
        <div class="container ps-0 pe-0">
            <div id="carousel1" class="carousel slide" data-bs-ride="carousel" data-bs-interval="2000">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carousel1" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carousel1" data-bs-slide-to="1" aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carousel1" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    <button type="button" data-bs-target="#carousel1" data-bs-slide-to="3" aria-label="Slide 4"></button>
                    <button type="button" data-bs-target="#carousel1" data-bs-slide-to="4" aria-label="Slide 5"></button>
                </div>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="<?= lib\CFG_IMAGEURI ?>carousel/20200501_161344kk.jpg" class="d-block w-100" alt="Ringgleis Hinweis">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Ringgleis 1</h5>
                            <p>Bodenhinweis TU Campus Nord</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="<?= lib\CFG_IMAGEURI ?>carousel/20190721_110301kk.jpg" class="d-block w-100" alt="Fahrrad am Westbahnhof">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Ringgleis 2</h5>
                            <p>Fahrrad am Westbahnhof</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="<?= lib\CFG_IMAGEURI ?>carousel/20190721_105155kk.jpg" class="d-block w-100" alt="Ladelehre">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Ringgleis 3</h5>
                            <p>Ladelehre bzw. Profilgalgen</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="<?= lib\CFG_IMAGEURI ?>carousel/20190721_110650kk.jpg" class="d-block w-100" alt="Drehscheibe">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Ringgleis 4</h5>
                            <p>Drehscheibe und Wege</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="<?= lib\CFG_IMAGEURI ?>carousel/20190721_105822kk.jpg" class="d-block w-100" alt="Weichenlaterne">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Ringgleis 5</h5>
                            <p>Gleis und Weichenlaterne</p>
                        </div>
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carousel1" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carousel1" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
<!-- END carousel.include  -->
