<?php
/**
 * ggLib sample
 */

// scope USER

// messages ----------------------
$str['msg_authrequired'] = array(
    'de' => 'Anmeldung erforderlich',
    'en' => 'Login required'
);

$str['msg_rolerequired'] = array(
    'de' => 'Rolle {{{rolename}}} erforderlich',
    'en' => 'Role {{{rolename}}} required'
);

$str['msg_autologgedout'] = array(
    'de' => 'Neuanmeldung erforderlich',
    'en' => 'New login required'
);

$str['msg_loggedout'] = array(
    'de' => 'Jetzt abgemeldet',
    'en' => 'Now logged out'
);

$str['msg_wrongcredentials'] = array(
    'de' => 'Falsche Zugangsdaten!',
    'en' => 'Wrong Credentials!'
);

$str['msg_pwforgotmsgsent'] = array(
    'de' => 'E-Mail zum Zurücksetzen des Passwortes versendet',
    'en' => 'Reset-password-e-mail sent'
);

$str['msg_registermsgsent'] = array(
    'de' => 'E-Mail mit Bestätigungslink zum Registrieren versendet',
    'en' => 'E-mail with confirmation link for registration sent'
);

$str['msg_wrongtoken'] = array(
    'de' => 'Fehler bei Passwort Vergabe',
    'en' => 'Password edit error'
);

$str['msg_savepwfail'] = array(
    'de' => 'Fehler beim Speichern des Passwortes!',
    'en' => 'Password save error!'
);

$str['msg_savepwsuccess'] = array(
    'de' => 'Neues Passwort gespeichert',
    'en' => 'New password saved'
);

// heads ----------------------
$str['head_login'] = array(
    'de' => 'Anmelden',
    'en' => 'Login'
);

$str['head_register'] = array(
    'de' => 'Als neuer Benutzer registrieren',
    'en' => 'Register as a new user'
);

$str['head_forgotpassword'] = array(
    'de' => 'Neues Passwort anfordern',
    'en' => 'Request a new password'
);

$str['head_newpassword'] = array(
    'de' => 'Neues Passwort',
    'en' => 'New password'
);

// texts ----------------------
$str['txt_register'] = array(
    'de' => 'Bitte Ihre Personen-Daten eintragen.<br>Sie erhalten eine E-Mail mit einem Bestätigungslink, der bitte kurzfristig anzuwählen ist.',
    'en' => 'Please insert your personal data.<br>You will receive an E-mail with a confirmation link kindly to be clicked shortly.'
);

$str['txt_resetpasswordmail'] = array(
    'de' => 'Bitte Ihre E-Mail-Adresse eintragen.<br>Sie erhalten in Kürze eine E-Mail mit Hinweisen zum Zurücksetzen Ihres Passwortes.',
    'en' => 'Please insert your E-mail address.<br>You will receive an E-mail shortly with information about resetting your password.'
);

$str['txt_resetpassword'] = array(
    'de' => 'Wählen Sie hier Ihr neues Passwort für Benutzer {{{username}}}.',
    'en' => 'Choose your new password here for user {{{username}}}.'
);

// labels ----------------------
$str['lbl_username'] = array(
    'de' => 'Benutzername',
    'en' => 'Username'
);

$str['lbl_newusername'] = array(
    'de' => 'Neuer Benutzername',
    'en' => 'New username'
);

$str['lbl_firstname'] = array(
    'de' => 'Vorname',
    'en' => 'First name'
);

$str['lbl_lastname'] = array(
    'de' => 'Nachname',
    'en' => 'Last name'
);

$str['lbl_email'] = array(
    'de' => 'E-Mail',
    'en' => 'E-mail'
);

$str['lbl_oldpassword'] = array(
    'de' => 'Gegenwärtiges Passwort',
    'en' => 'Current Password'
);

$str['lbl_password'] = array(
    'de' => 'Passwort',
    'en' => 'Password'
);

$str['lbl_repeatpassword'] = array(
    'de' => 'Passwort wiederholen',
    'en' => 'Repeat password'
);

// links ----------------------
$str['lnk_register'] = array(
    'de' => 'Als neuer Benutzer registrieren',
    'en' => 'Register as new user'
);

$str['lnk_forgotpassword'] = array(
    'de' => 'Passwort vergessen',
    'en' => 'Forgot password'
);

$str['lnk_login'] = array(
    'de' => 'Bereits registriert?',
    'en' => 'Already registered?'
);

// input errors ----------------------
$str['err_firstnameRequired'] = array(
    'de' => 'Bitte Vornamen eingeben',
    'en' => 'Please insert firstname'
);

$str['err_lastnameRequired'] = array(
    'de' => 'Bitte Nachnamen eingeben',
    'en' => 'Please insert lastname'
);

$str['err_emailRequired'] = array(
    'de' => 'Bitte E-Mail eingeben',
    'en' => 'Please insert E-mail'
);

$str['err_usernameRequired'] = array(
    'de' => 'Bitte Benutzernamen eingeben',
    'en' => 'Please insert username'
);

$str['err_passwordRequired'] = array(
    'de' => 'Bitte Passwort eingeben',
    'en' => 'Please insert password'
);

$str['err_passwordNotEqual'] = array(
    'de' => 'Bitte Passwort wiederholen',
    'en' => 'Please repeat password'
);

// buttons
$str['btn_login'] = array(
    'de' => 'Anmelden',
    'en' => 'Login'
);

$str['btn_register'] = array(
    'de' => 'Registrieren',
    'en' => 'Register'
);

$str['btn_send'] = array(
    'de' => 'E-Mail versenden',
    'en' => 'Send E-mail'
);

$str['btn_newpassword'] = array(
    'de' => 'Passwort speichern',
    'en' => 'Save password'
);