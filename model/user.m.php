<?php
namespace gg;
/**
 * ggLib sample
 */

class user extends lib\db {
    
    public static function getPwHash($plainPassword) {
        return password_hash($plainPassword, PASSWORD_BCRYPT, ["cost" => 9]);
    }
    
    public static function verifyPw($plainPw, $hashedPw) {
        return password_verify($plainPw, $hashedPw);
    }
    
    public function verifyUsername($username) {
        $this->init(null, true);
        $this->f->username = $username;
        $deltedfield = lib\CFG_DELETEDFIELD;
        $this->f->$deltedfield = 0;
        $userRecords = $this->getRecords();
        if (!empty($userRecords) && count($userRecords) > 1) {
            die('class gg\user: username duplicate');
        }
        return (empty($userRecords)) ? null : $userRecords[0];
    }
    
    public function verifyLogin($username, $plainPassword) {
        $userObj = $this->verifyUsername($username);
        if ($userObj) {
            if (self::verifyPw($plainPassword, $userObj->f->password)) {
                return $userObj;
            }
        }
        return false;
    }
    
    public function saveWithPw() {
        if (isset($this->f->password)) {
            $this->f->password = self::getPwHash($this->f->password);
        }
        $this->save();
    }
    
    public static function getAllAvailableRoles() {
        // todo, depends on role implementation
        return [];
    }
    
    public static function getUserRoles($userObj) {
        // todo, depends on role implementation
        return [];
    }
    
    public static function setUserRole($userObj, $roleId) {
        // todo, depends on role implementation
    }
    
    public static function hasUserRole($userObj, $roleKeynameArray) {
        // todo, depends on role implementation
        return true;
    }
    
    public static function getRoleDisplayName($roleKeyname, $lang) {
        // todo, depends on role implementation
        return '[[not set]]';
    }
    
    public static function getUserByToken($token) {
        // todo
        // token renew validity 
        return null;
    }
    
    public static function cleanUnconfirmedUsers() {
        // todo
    }
    
    public function getLang() {
        return $this->f->preflang ?? null;
    }
    
    public static function getCurrentUserLang() {
        $me = lib\getCurrentUser();
        return $me ? $me->getLang() : null;
    }
    
}

