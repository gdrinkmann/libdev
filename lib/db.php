<?php
namespace gg\lib;

/**
 * ggLib CORE
 * 
 * standard included in index.php
 * 
 * handling:
 * for every table in db create <modelname = tablename>.m.php in model directory
 * with model class:
 * class <modelname = tablename> extends lib\db {
 * ...
 * }
 * 
 * Requirement: database rules of libdev (look at readme.md)
 * class gg\lib\db
 * @author Gerd
 */
class db {
    
    const HISTORY_TABLE = 'history';
    const HISTORY_METHOD = 'logChange';
    
    protected $dbObj;
    
    protected $stmtObj;
    protected $resultObj;
    protected $hookdata;
    
    protected $tableName;
    protected $lang;

    protected $fieldList;
    protected $fieldTypes;
    protected $referenceTo;
    protected $referenceFrom;
    protected $hasPrimarykey;
    protected $hasDelete;
    protected $nameFields;
    protected $datasetLimit = 10000;
    
    public $f; // dataset object
    
    public static $tables;

    public function __construct($id = null) {
        $this->dbObj = new \mysqli(CFG_DBHOST, CFG_DBUSER, CFG_DBPW, CFG_DBNAME, CFG_DBPORT);
        if ($this->dbObj->connect_errno) {
            $this->handleError('connect');
        }
        $this->dbObj->set_charset(CFG_DBCHARSET);
        
        $this->tableName = str_replace('gg\\', '', get_class($this));
        $this->lang = str::getLang();
        
        if (!isset(self::$tables)) {
            $res = $this->dbObj->query('SHOW TABLES');
            while ($row = $res->fetch_array(MYSQLI_NUM)) {
                $this->storeTableinfo($row[0]);
            }
            foreach (self::$tables as $tablename => $tableinfo) {
                foreach ($tableinfo['referenceTo'] as $tname => $fknames) {
                    if (!isset(self::$tables[$tname]['referenceFrom'][$tablename])) {
                        self::$tables[$tname]['referenceFrom'][$tablename] = $fknames;
                    }
                }
            }
        }
        
        $this->fieldList = self::$tables[$this->tableName]['fieldList'];
        $this->fieldTypes = self::$tables[$this->tableName]['fieldTypes'];
        $this->referenceTo = self::$tables[$this->tableName]['referenceTo'];
        $this->referenceFrom = self::$tables[$this->tableName]['referenceFrom'];
        $this->hasPrimarykey = self::$tables[$this->tableName]['hasPrimarykey'];
        $this->hasDelete = self::$tables[$this->tableName]['hasDelete'];
        $this->nameFields = self::$tables[$this->tableName]['nameFields'];
        
        $this->init($id);
    }
    
//    public function __destruct() {
//        $this->dbObj->close();
//    }
    
    private function storeTableinfo($tablename) {
        $tableinfo = [];
        
        $res = $this->dbObj->query('SHOW COLUMNS FROM `' . $tablename . '`');
        $tableinfo['fieldList'] = [];
        $tableinfo['nameFields'] = [];
        while ($row = $res->fetch_assoc()) {
            preg_match('/^(\S+?)(\((.+)\))?$/', $row['Type'], $matches);
            $tableinfo['fieldList'][$row['Field']]['type'] = $matches[1];
            $tableinfo['fieldList'][$row['Field']]['para'] = $matches[3] ?? null;
            $tableinfo['fieldList'][$row['Field']]['null'] = $row['Null'] == 'YES' ? true : false;
            $tableinfo['fieldList'][$row['Field']]['default'] = $row['Default'];

            if (preg_match('/^' . CFG_NAMEFIELD . '_(\S{2,3})$/', $row['Field'], $found)) {
                $tableinfo['nameFields'][$found[1]] = $row['Field'];
            }
        }
        
        // get foreign keys
        $fkquery = 'SELECT `COLUMN_NAME` `fk_field`, `REFERENCED_TABLE_NAME` `ref_table`, `REFERENCED_COLUMN_NAME` `ref_field` FROM `information_schema`.`KEY_COLUMN_USAGE` WHERE `TABLE_SCHEMA` = "'.CFG_DBNAME.'" AND `TABLE_NAME` = "'.$tablename.'" AND `REFERENCED_COLUMN_NAME` IS NOT NULL';
        $res = $this->dbObj->query($fkquery);
        $tableinfo['referenceTo'] = [];
        while ($row = $res->fetch_assoc()) {
//            if (class_exists($row['ref_table'])) {
                $tableinfo['referenceTo'][$row['ref_table']][] = ['fkey' => $row['fk_field'], 'refkey' => $row['ref_field']];
//            }
        }

        $tableinfo['fieldTypes'] = [];
        foreach ($tableinfo['fieldList'] as $fieldname => $fieldprops) {
            $tableinfo['fieldTypes'][$fieldprops['type']][] = $fieldname;
        }

        $tableinfo['hasPrimarykey'] = isset($tableinfo['fieldList'][CFG_PRIMARYKEYNAME]);
        $tableinfo['hasDelete'] = isset($tableinfo['fieldList'][CFG_DELETEDFIELD]);
        
        $tableinfo['nameFields']['pure'] = isset($tableinfo['fieldList'][CFG_NAMEFIELD]) ? CFG_NAMEFIELD : null;
        
        $tableinfo['referenceFrom'] = [];
        
        self::$tables[$tablename] = $tableinfo;
    }
    
    private function handleError($type, $addings = null) {
        switch ($type) {
            case 'connect':
                if (CFG_DEVMODUS) {
                    die('class gg\lib\db: connection error: ' . $this->dbObj->connect_error);
                } else {
                    page::setMessage(['msg_nodatabase', 'general'], PRIO_ERROR, true);
                    http_response_code(500);
                    request::redirect(CFG_STARTPAGE);
                }
                break;
                
            case 'paramserror':
                die('class gg\lib\db: stmt params error');
                break;
            
            case 'executestmt':
                die('class gg\lib\db: stmt error' . (CFG_DEVMODUS ? ': ' . $this->stmtObj->error : ''));
                break;
            
            case 'selectmsqli':
                die('class gg\lib\db: mysqli error' . (CFG_DEVMODUS ? ': ' . $this->dbObj->error : ''));
                break;
            
            case 'wrongpk':
                die('class gg\lib\db: wrong primary key type');
                break;
            
            case 'fieldtoolong':
                die('class gg\lib\db: field value too long for ' . ($addings ?? ''));
                break;
            
            case 'save':
                die('class gg\lib\db: dataset not inserted/updated');
                break;
            
            case 'limitreached':
                die('class gg\lib\db: dataset limit count reached');
                break;
        }
    }
    
    /**
     * set one field or several fields
     * @param string|array $fieldname
     * @param mixed $value
     */
    public function setField($fieldname, $value = null) {
        if (!$this->f) {
            $this->init();
        }
        if (is_array($fieldname)) {
            foreach ($fieldname as $fname => $val) {
                $this->f->$fname = $val;
            }
        } else {
            $this->f->$fieldname = $value;
        }
    }
    
    /**
     * get field value
     * @param string $fieldname
     * @return mixed
     */
    public function getField($fieldname) {
        return $this->f->$fieldname ?? null;
    }
    
    /**
     * init the dataset object
     * @param int $id
     * @param bool $onlyDataset
     */
    public function init($id = null, $onlyDataset = false) {
        if (!$onlyDataset) {
            unset($this->stmtObj, $this->resultObj, $this->hookdata);
        }
        $this->f = new \stdClass();
        if (!is_null($id)) {
            $pkn = CFG_PRIMARYKEYNAME;
            $this->f->$pkn = (int)$id;
        }
    }
    
    private function parseFields($forSave, &$fields, &$values, &$pkincluded) {
        $pkn = CFG_PRIMARYKEYNAME;
        
        $this->beforeParse();
        
        $fields = [];
        $values = [];
        $pkincluded = isset($this->f->$pkn);
        foreach ($this->fieldList as $fieldname => $fieldinfo) {
            if ($fieldname !== $pkn) {
                $fieldname = str_replace('`', '', $fieldname);
                $fullfield = explode('.', $fieldname);
                $fullfield = '`' . implode('`.`', $fullfield) . '`';
                if (isset($this->f->$fieldname)) {
                    $fields[] = $fullfield . ' = ?';
                    $values[] = $this->getRevisedValue($fieldname, $this->f->$fieldname);
                } elseif (!$pkincluded && $forSave) {
                    if (!$fieldinfo['null'] && is_null($fieldinfo['default'])) {
                        $fields[] = $fullfield . ' = ?';
                        $values[] = $this->getRevisedValue($fieldname, null, true);
                    }
                }
            }
        }
        // primary key must be the last item
        if ($pkincluded) {
            if (is_integer($this->f->$pkn)) {
                $fields[] = '`' . $pkn . '` = ?';
                $values[] = $this->f->$pkn;
            } else {
                $this->handleError('wrongpk');
            }
        }
    }
    
    private function getRevisedValue($fieldname, $value, $getDefault = false) {
        switch ($this->fieldList[$fieldname]['type']) {
            case 'date':
                $value = ($getDefault) ? '0000-00-00' : \gg\getDate('toDBdate', $value);
                break;
            case 'datetime':
                $value = ($getDefault) ? '0000-00-00 00:00:00' : \gg\getDate('toDBdatetime', $value);
                break;
            case 'decimal':
            case 'float';
            case 'double';
            case 'real';
                $value = ($getDefault) ? 0.0 : \gg\getNumber($value);
                break;
            case 'int':
            case 'tinyint':
            case 'smallint':
            case 'mediumint':
            case 'bigint':
                $value = ($getDefault) ? 0 : \gg\getNumber($value);
                break;
            case 'varchar':
                if ($getDefault) {
                    $value = '';
                } elseif (strlen($value) > $this->fieldList[$fieldname]['para']) {
                    if (CFG_DEVMODUS) {
                        $this->handleError('fieldtoolong', $this->fieldList[$fieldname]);
                    } else {
                        $value = \gg\shorten($value, $this->fieldList[$fieldname]['para'], false);
                        page::setMessage(str::get('field shortened', 'db') . str::get($fieldname, 'db'), PRIO_WARN);
                    }
                }
                break;
            case 'text':
                if ($getDefault) {
                    $value = '';
                }
        }
        return $value;
    }
    
    private function parseValues($values) {
        $types = '';
        foreach ($values as $value) {
            if (is_string($value)) {
                $types .= 's';
            } elseif (is_integer($value)) {
                $types .= 'i';
            } elseif (is_float($value)) {
                $types .= 'd';
            } else {
                $types .= 'b';
            }
        }
        return $types;
    }
    
    /**
     * test query
     * @param string $query
     * @return bool
     */
    public function isSavequery($query) {
        $l = strtolower($query);
        return strpos($l, 'update') === 0 || strpos($l, 'insert') === 0 || strpos($l, 'replace') === 0 || strpos($l, 'delete') === 0;
    }
    
    /**
     * test query
     * @param string $query
     * @return bool
     */
    public function isSelectquery($query) {
        $l = strtolower($query);
        return strpos($l, 'select') === 0 || strpos($l, 'show') === 0;
    }
    
    /**
     * execute prepared statement and fill result object
     * @param string $query with ? as placeholders
     * @param array $params array of values with correct datatype
     * @return bool|int success or affected rows
     */
    public function execute($query, $params = []) {
        $cntquestionmarks = array_count_values(str_split($query))['?'] ?? 0;
        if ($cntquestionmarks !== count($params)) {
            $this->handleError('paramserror');
        }
        $this->stmtObj = $this->dbObj->prepare($query);
        if (!empty($params)) {
            $types = $this->parseValues($params);
            $this->stmtObj->bind_param($types, ...$params);
        }
        $this->stmtObj->execute();
        $result = $this->stmtObj->get_result();
        if ($result === false) {
            if ($this->stmtObj->errno) {
                $this->handleError('executestmt');
            } else {
                $ret = $this->stmtObj->affected_rows;
            }
        } else {
            $ret = true;
        }
        $this->resultObj = $result;
        
        return $ret;
    }
    
    /**
     * execute general select query and get result array
     * @param string $selectQuery with ? as placeholders for params
     * @param array $params array of corresponding values with correct datatype
     * @return array result as array of models
     */
    public function getSqlRecords($selectQuery, $params = []) {
        if ($this->isSelectquery($selectQuery)) {
            $this->execute($selectQuery, $params);
            $resultArr = [];
            if ($this->resultObj !== false) {
                $i = 0;
                while ($obj = $this->resultObj->fetch_object()) {
                    $m = get_class($this);
                    $model = new $m();
                    $model->f = $obj;
                    $model->parseResult();
                    $resultArr[] = $model;
                    $i++;
                    if ($i >= $this->datasetLimit) {
                        $this->handleError('limitreached');
                        break;
                    }
                }
                $this->resultObj->free();
            }
            return $resultArr;
        } else {
            return null;
        }
    }
    
    /**
     * set result values to appropriate type
     */
    public function parseResult() {
        foreach ($this->f as $fieldname => $value) {
            if (isset($this->fieldList[$fieldname])) {
                switch ($this->fieldList[$fieldname]['type']) {
                    case 'int':
                    case 'tinyint':
                    case 'smallint':
                    case 'mediumint':
                    case 'bigint':
                        $this->f->$fieldname = (int)$value;
                        break;
                    case 'decimal';
                    case 'float';
                    case 'double';
                    case 'real';
                        $this->f->$fieldname = (float)$value;
                        break;
                }
            }
        }
    }
    
    /**
     * get records with condition set by model settings
     * @param string $add
     * @param bool $withForeignModels
     * @return array result as array of models
     */
    public function getRecords($add = '', $withForeignModels = false) {
        $this->parseFields(false, $fields, $params, $pkincluded);
        $where = empty($fields) ? '' : ' WHERE ' . implode(' AND ', $fields);
        $selectQuery = 'SELECT * FROM `' . $this->tableName . '`' . $where . ' ' . $add;
        return $this->getSqlRecords($selectQuery, $params);
    }
    
    /**
     * find model object
     * @param integer $id
     */
    public function getRecord($id) {
        $query = 'SELECT * FROM `' . $this->tableName . '` WHERE `' . CFG_PRIMARYKEYNAME . '` = ' . (int)$id . ' LIMIT 1;';
        $this->resultObj = $this->dbObj->query($query);
        if ($this->resultObj === false) {
            $this->handleError('selectmsqli');
        } else {
            $rowObj = $this->resultObj->fetch_object() ?? new \stdClass();
        }
        $this->f = $rowObj;
        $this->parseResult();
        $this->resultObj->free();
    }
    
    /**
     * save current dataset object and optionally fill the primary key
     * @return int|false affected rows (should be 1)
     */
    public function save() {
        $pkn = CFG_PRIMARYKEYNAME;
        
        $this->parseFields(true, $fields, $params, $pkincluded);
        
        if ($pkincluded) {
            array_pop($fields);
            $query = 'UPDATE `' . $this->tableName .'` SET ' . implode(', ', $fields) . ' WHERE `' . CFG_PRIMARYKEYNAME . '` = ? LIMIT 1';
        } else {
            $query = 'INSERT INTO ' . $this->tableName . ' SET ' . implode(', ', $fields);
        }
        
        $this->hookdata['query'] = $query;
        $this->hookdata['params'] = $params;
        $this->beforeSave();
        $query = $this->hookdata['query'];
        $params = $this->hookdata['params'];
        
        $affectedrows = $this->execute($query, $params);
        if (!$affectedrows) {
             $this->handleError('save');
        }
        if (!$pkincluded) {
            $this->f->$pkn = $this->stmtObj->insert_id;
        }
        $this->saveHistory();
        
        $this->afterSave();
        unset($this->hookdata);
        
        return $affectedrows;
    }
    
    /**
     * @param null|int $id
     * @param bool $physically
     * @return int affected rows (0 or 1)
     */
    public function delete($id = null, $physically = false) {
        $pkn = CFG_PRIMARYKEYNAME;
        $dfn = CFG_DELETEDFIELD;
        
        $idd = $id ?? ($this->f->$pkn ?? 0);
        
        if (!$physically && isset($this->fieldList[$dfn])) {
            $query = 'UPDATE `' . $this->tableName . '` SET `' . $dfn . '` = 1 WHERE `' . $pkn . '` = ? LIMIT 1';
            if ($cnt = $this->execute($query, [$idd])) {
                $this->init($idd);
                $this->f->$dfn = 1;
                $this->saveHistory();
            }
        } else {
            $query = 'DELETE FROM `' . $this->tableName . '` WHERE `' . $pkn . '` = ? LIMIT 1';
            $cnt = $this->execute($query, [$idd]);
        }
        if ($cnt && is_null($id) && $physically) {
            $this->init();
        }
        return $cnt;
    }
    
    /**
     * get the name attribute of a dataset
     * @param string $lang
     * @return string|null
     */
    public function getName($lang = null) {
        $pkn = CFG_PRIMARYKEYNAME;
        
        $lang = $lang ?? $this->lang;
        $flang = $this->nameFields[$lang] ?? null;
        $fpure = $this->nameFields['pure'] ?? null;
        $pk = $this->f->$pkn ?? null;
        
        if ($flang) {
            if (isset($this->f->$flang)) {
                return $this->f->$flang;
            } elseif ($pk) {
                $q = 'SELECT `' . $flang . '` FROM `' . $this->tableName . '` WHERE `' . $pkn . '` = ? LIMIT 1';
                $ret = $this->getSqlRecords($q, [(int)$pk]);
                $this->f->$flang = $ret[0]->$flang;
                return $this->f->$flang;
            }
        } elseif ($fpure) {
            if (isset($this->f->$fpure)) {
                return $this->f->$fpure;
            } elseif ($pk) {
                $q = 'SELECT `' . $fpure . '` FROM `' . $this->tableName . '` WHERE `' . $pkn . '` = ? LIMIT 1';
                $ret = $this->getSqlRecords($q, [(int)$pk]);
                $this->f->$fpure = $ret[0]->$fpure;
                return $this->f->$fpure;
            }
        } elseif ($pk) {
            return $this->tableName . '.' . $pkn . '_' . $pk;
        } else {
            return null;
        }
    }
    
    private function saveHistory() {
        $datasettolog = new \stdClass();
        $all = $this->historyFields() === '*'; 
        foreach ($this->f as $savefield => $value) {
            if ($all || in_array($savefield, $this->historyFields())) {
                $datasettolog->$savefield = $value;
            }
        }
        $class = self::HISTORY_TABLE;
        $method = self::HISTORY_METHOD;
        if (method_exists('\\gg\\' . $class, $method) && !empty((array)$datasettolog)) {
            $class::$method($this->tableName, $datasettolog);
        }
    }
    
    /**
     * list of fields for history
     * @return array fieldnames
     */
    public function historyFields() {
        // should be overwritten in childclass ('*' means All)
        return [];
    }
    
    /**
     * hook function processed before query parsing
     */
    public function beforeParse() {
        // you may manipulate $this->f
        // something important here in childclass
    }
    
    /**
     * hook function processed before save dataset
     */
    public function beforeSave() {
        // you may manipulate $this->hookdata['query'] and  $this->hookdata['params']
        // something important here in childclass
    }
    
    /**
     * hook function processed after save dataset
     */
    public function afterSave() {
        // something important here in childclass
    }
}
