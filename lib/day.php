<?php
namespace gg\lib;

/**
 * ggLib CORE
 * Calculate number of workdays between two dates
 * or adding workdays to a given date
 * considering local official holidays
 * 
 * class gg\lib\day
 * @author Gerd
 */
class day {

    private $datumTS1 = false; // Berechnungsgrundlage Timestamp Datum 1
    private $datumTS2 = false; // Berechnungsgrundlage/Resultat Timestamp Datum 2
    private $anzahlWerkTage = false; // Berechnungsgrundlage/Resultat Anzahl Werktage
    private $lastFeiertag = ""; // letzter berechneter Feiertagsname
    private $lastWochentag = ""; // letzter ermittelter Wochentagname
    private $error = "";
    private $feiertagArr = array(); // im Konstrukor aufgebautes Array
    private $wochenArbTage; // Wochentage, die Werktage sind
    private $land = ""; // vorerst D, A, CH
    private $wochenTagNamen = array(
        "D" => array("Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"),
        "A" => array("Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"),
        "CH" => array("Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag")
    );

    /**
     * constructor
     *
     * @param array $woArbTage Wochenarbeitstage array(date("w"),...) default Mo-Fr
     * @param string $land D,A,CH (oder . falls keine Feiertage berücksichtigt werden sollen)
     * @param string $bundesland (oder . falls keine Feiertage berücksichtigt werden sollen)
     */
    public function __construct($woArbTage = "default", $land = ".", $bundesland = ".") {
        
        // Stand 02.2020
        // D: https://de.wikipedia.org/wiki/Gesetzliche_Feiertage_in_Deutschland#%C3%9Cbersicht_aller_gesetzlichen_Feiertage
        // A: https://de.wikipedia.org/wiki/Feiertage_in_%C3%96sterreich
        // CH: https://de.wikipedia.org/wiki/Feiertage_in_der_Schweiz
        
        $ft = array( // Feiertage mit Erstellcode und länderspezifische Bezeichnungen
                0   => array("01-01",	array("D"=>"Neujahrstag", "A"=>"Neujahr", "CH"=>"Neujahrstag")),
                5   => array("01-02",	array("CH"=>"Berchtoldstag")),
                10  => array("01-06",	array("D"=>"Heilige Drei Könige", "A"=>"Heilige Drei Könige", "CH"=>"Heilige Drei Könige")),
                13  => array("01-08",	array("D"=>"Frauentag")),
                15  => array("03-19",	array("A"=>"Josefstag", "CH"=>"Josefstag")),
                20  => array("E-2",	array("D"=>"Karfreitag", "A"=>"Karfreitag", "CH"=>"Karfreitag")),
                30  => array("E1",	array("D"=>"Ostermontag", "A"=>"Ostermontag", "CH"=>"Ostermontag")),
                40  => array("05-01",	array("D"=>"Maifeiertag", "A"=>"Staatsfeiertag", "CH"=>"Tag der Arbeit")),
                45  => array("05-04",	array("A"=>"Florian")),
                50  => array("E39",	array("D"=>"Christi Himmelfahrt", "A"=>"Christi Himmelfahrt", "CH"=>"Auffahrt")),
                60  => array("E50",	array("D"=>"Pfingstmontag", "A"=>"Pfingstmontag", "CH"=>"Pfingstmontag")),
                70  => array("E60",	array("D"=>"Fronleichnam", "A"=>"Fronleichnam", "CH"=>"Fronleichnam")),
                75  => array("08-01",	array("CH"=>"Bundesfeier")),
                80  => array("08-15",	array("D"=>"Mariä Himmelfahrt", "A"=>"Mariä Himmelfahrt", "CH"=>"Mariä Himmelfahrt")),
                85  => array("09-24",	array("A"=>"Rupertikirtag")),
                90  => array("10-03",	array("D"=>"Tag der Deutschen Einheit")),
                93  => array("10-10",	array("A"=>"Tag der Volksabstimmung")),
                97  => array("10-26",	array("A"=>"Nationalfeiertag")),
                100 => array("10-31",	array("D"=>"Reformationstag")),
                110 => array("11-01",	array("D"=>"Allerheiligen", "A"=>"Allerheiligen", "CH"=>"Allerheiligen")),
                113 => array("11-11",	array("A"=>"Martinstag")),
                117 => array("11-15",	array("A"=>"Leopolditag")),
                120 => array("bb",	array("D"=>"Buß- und Bettag",)),
                123 => array("12-08",	array("A"=>"Mariä Empfängnis", "CH"=>"Mariä Empfängnis")),
                127 => array("12-24",	array("A"=>"Heiliger Abend")),
                130 => array("12-25",	array("D"=>"1. Weihnachtsfeiertag", "A"=>"Christtag", "CH"=>"Weihnachtstag")),
                140 => array("12-26",	array("D"=>"2. Weihnachtsfeiertag", "A"=>"Stefanitag", "CH"=>"Stephanstag")),
                145 => array("stmo",	array("CH"=>"Stephanstag")), // Stephanstag nur Montag
                150 => array("12-31",	array("A"=>"Silvester"))
        );

        $fa["."]["."] = array();

        $fa["D"]["badenwuerttemberg"] 			= array(0, 10,     20, 30, 40, 50, 60, 70,     90,      110,      130, 140);
        $fa["D"]["bayern"] 				= array(0, 10,     20, 30, 40, 50, 60, 70, 80, 90,      110,      130, 140);
        $fa["D"]["berlin"] 				= array(0,     13, 20, 30, 40, 50, 60,         90,                130, 140);
        $fa["D"]["brandenburg"] 			= array(0,         20, 30, 40, 50, 60,         90, 100,           130, 140);
        $fa["D"]["bremen"] 				= array(0,         20, 30, 40, 50, 60,         90, 100,           130, 140);
        $fa["D"]["hamburg"] 				= array(0,         20, 30, 40, 50, 60,         90, 100,           130, 140);
        $fa["D"]["hessen"] 				= array(0,         20, 30, 40, 50, 60, 70,     90,                130, 140);
        $fa["D"]["mecklenburgvorpommern"] 		= array(0,         20, 30, 40, 50, 60,         90, 100,           130, 140);
        $fa["D"]["niedersachsen"] 			= array(0,         20, 30, 40, 50, 60,         90, 100,           130, 140);
        $fa["D"]["nordrheinwestfalen"] 			= array(0,         20, 30, 40, 50, 60, 70,     90,      110,      130, 140);
        $fa["D"]["rheinlandpfalz"] 			= array(0,         20, 30, 40, 50, 60, 70,     90,      110,      130, 140);
        $fa["D"]["saarland"] 				= array(0,         20, 30, 40, 50, 60, 70, 80, 90,      110,      130, 140);
        $fa["D"]["sachsen"] 				= array(0,         20, 30, 40, 50, 60,         90, 100,      120, 130, 140);
        $fa["D"]["sachsenanhalt"] 			= array(0, 10,     20, 30, 40, 50, 60,         90, 100,           130, 140);
        $fa["D"]["schleswigholstein"] 			= array(0,         20, 30, 40, 50, 60,         90, 100,           130, 140);
        $fa["D"]["thueringen"] 				= array(0,         20, 30, 40, 50, 60,         90, 100,           130, 140);

        $fa["A"]["burgenland"] 				= array(0, 10,     20, 30, 40,     50, 60, 70, 80,         97, 110, 113,      123, 127, 130, 140, 150);
        $fa["A"]["kaernten"] 				= array(0, 10, 15, 20, 30, 40,     50, 60, 70, 80,     93, 97, 110,           123, 127, 130, 140, 150);
        $fa["A"]["niederoesterreich"] 			= array(0, 10,     20, 30, 40,     50, 60, 70, 80,         97, 110,      117, 123, 127, 130, 140, 150);
        $fa["A"]["oberoesterreich"] 			= array(0, 10,     20, 30, 40, 45, 50, 60, 70, 80,         97, 110,           123, 127, 130, 140, 150);
        $fa["A"]["salzburg"] 				= array(0, 10,     20, 30, 40,     50, 60, 70, 80, 85,     97, 110,           123, 127, 130, 140, 150);
        $fa["A"]["steiermark"] 				= array(0, 10, 15, 20, 30, 40,     50, 60, 70, 80,         97, 110,           123, 127, 130, 140, 150);
        $fa["A"]["tirol"] 				= array(0, 10, 15, 20, 30, 40,     50, 60, 70, 80,         97, 110,           123, 127, 130, 140, 150);
        $fa["A"]["vorarlberg"] 				= array(0, 10, 15, 20, 30, 40,     50, 60, 70, 80,         97, 110,           123, 127, 130, 140, 150);
        $fa["A"]["wien"] 				= array(0, 10,     20, 30, 40,     50, 60, 70, 80,         97, 110,      117, 123, 127, 130, 140, 150);

        $fa["CH"]["zuerich"] 				= array(0, 5,         20, 30, 40, 50, 60,     75,               130, 140);
        $fa["CH"]["bern"] 				= array(0, 5,         20, 30,     50, 60,     75,               130, 140);
        $fa["CH"]["luzern"] 				= array(0, 5,     15, 20, 30,     50, 60, 70, 75, 80, 110, 123, 130, 140);
        $fa["CH"]["uri"] 				= array(0,    10, 15, 20, 30,     50, 60, 70, 75, 80, 110, 123, 130, 140);
        $fa["CH"]["schwyz"] 				= array(0,    10, 15, 20, 30,     50, 60, 70, 75, 80, 110, 123, 130, 140);
        $fa["CH"]["obwalden"] 				= array(0, 5,         20, 30,     50, 60, 70, 75, 80, 110, 123, 130, 140);
        $fa["CH"]["nidwalden"] 				= array(0,        15, 20, 30,     50, 60, 70, 75, 80, 110, 123, 130, 140);
        $fa["CH"]["glarus"] 				= array(0, 5,         20, 30,     50, 60,     75,     110,      130, 140);
        $fa["CH"]["zug"] 				= array(0, 5,     15, 20, 30,     50, 60, 70, 75, 80, 110, 123, 130, 140);
        $fa["CH"]["freiburg"] 				= array(0, 5,     15, 20, 30, 40, 50, 60, 70, 75, 80, 110, 123, 130, 140);
        $fa["CH"]["solothurn"] 				= array(0, 5,     15, 20, 30, 40, 50, 60, 70, 75, 80, 110, 123, 130, 140);
        $fa["CH"]["baselstadt"] 			= array(0,            20, 30, 40, 50, 60,     75,               130, 140);
        $fa["CH"]["basellandschaft"]			= array(0,            20, 30, 40, 50, 60, 70, 75, 80,           130, 140);
        $fa["CH"]["schaffhausen"] 			= array(0, 5,         20, 30, 40, 50, 60,     75,               130, 140);
        $fa["CH"]["appenzellausserrhoden"] 		= array(0,            20, 30,     50, 60,     75,               130, 140);
        $fa["CH"]["appenzellinnerrhoden"] 		= array(0,            20, 30,     50, 60, 70, 75, 80, 110, 123, 130, 140);
        $fa["CH"]["stgallen"] 				= array(0,            20, 30,     50, 60,     75,     110,      130, 140);
        $fa["CH"]["graubuenden"] 			= array(0, 5, 10, 15, 20, 30,     50, 60, 70, 75, 80, 110, 123, 130, 140);
        $fa["CH"]["aargau"] 				= array(0, 5,         20, 30, 40, 50, 60, 70, 75, 80, 110, 123, 130, 140);
        $fa["CH"]["thurgau"] 				= array(0, 5,         20, 30, 40, 50, 60,     75,               130, 140);
        $fa["CH"]["tessin"] 				= array(0,    10, 15,     30, 40, 50, 60, 70, 75, 80, 110, 123, 130, 140);
        $fa["CH"]["waadt"] 				= array(0, 5,         20, 30,     50, 60,     75,               130     );
        $fa["CH"]["wallis"] 				= array(0,        15,             50,     70, 75, 80, 110, 123, 130     );
        $fa["CH"]["neuenburg"] 				= array(0, 5,         20, 30, 40, 50, 60, 70, 75,               130, 145);
        $fa["CH"]["genf"] 				= array(0, 5,         20, 30,     50, 60,     75,               130     );
        $fa["CH"]["jura"] 				= array(0, 5,         20, 30, 40, 50, 60, 70, 75, 80, 110,      130     );

        if ($land == "." || $bundesland == ".") {
            $land = ".";
            $bundesland = ".";
        }

        $this->land = strtoupper($land);

        $bl = strtolower(str_replace("ä", "ae", $bundesland));
        $bl = strtolower(str_replace("ö", "oe", $bl));
        $bl = strtolower(str_replace("ü", "ue", $bl));
        $bl = str_replace(" ", "", $bl);
        $bl = str_replace("-", "", $bl);

        if (isset($fa[$this->land][$bl])) {
            foreach ($fa[$this->land][$bl] as $v) {
                $this->feiertagArr[] = $ft[$v];
            }
        } else {
            $this->error = "Land/Bundesland nicht vorhanden";
        }

        if (is_array($woArbTage)) {
            $this->wochenArbTage = $woArbTage;
        } else {
            $this->wochenArbTage = array(1, 2, 3, 4, 5); // date("w")
        }
    }

    // ------------- Main methods -------------------
    /**
     * Berechnung in Abhängigkeit von den gesetzten Ausgangsvariablen
     * 
     * @return boolean Erfolg
     */
    public function calculate() {
        if ($this->error) {
            return false;
        }
        if (($this->datumTS1 !== false) && ($this->datumTS2 !== false)) {
            $this->countWorkdays();
        } elseif (($this->datumTS1 !== false) && ($this->anzahlWerkTage !== false)) {
            $this->addWorkdays();
        } elseif (($this->datumTS1 !== false)) {
            $this->isWorkday();
        } else {
            $this->error = "keine Berechnungsgrundlage vorhanden";
            return false;
        }
        return true;
    }

    // ------------- Setter -------------------
    /**
     * 1. Datum setzen
     * 
     * @param int|string $ts timestamp oder Datumstring
     */
    public function setDate1($ts) {
        if (!is_int($ts)) {
            $ts = strtotime($ts);
        }
        $this->datumTS1 = $ts;
    }

    /**
     * 2. Datum setzen (optional)
     * 
     * @param int|string $ts timestamp oder Datumstring
     */
    public function setDate2($ts) {
        if (!is_int($ts)) {
            $ts = strtotime($ts);
        }
        $this->datumTS2 = $ts;
    }

    /**
     * Anzahl der Werktage zwischen 1. Datum und 2. Datum setzen (optional)
     * 
     * @param int $anz
     */
    public function setWorkdaysCount($anz) {
        $this->anzahlWerkTage = (int) $anz;
    }

    // ------------- Getter -------------------
    /**
     * berechnetes 2. Datum
     * 
     * @param boolean|string $dateformat false für timestamp oder date-format-string
     * 
     * @return int|string timestamp oder Datumstring
     */
    public function getDate2($dateformat = false) {
        $ret = ($dateformat) ? date($dateformat, $this->datumTS2) : $this->datumTS2;
        return $ret;
    }

    /**
     * berechnete Anzahl der Werktage zwischen 1. Datum und 2. Datum
     * 
     * @return int
     */
    public function getWorkdaysCount() {
        return $this->anzahlWerkTage;
    }

    /**
     * letzter Feiertagsname in der Berechnung
     * 
     * @return string
     */
    public function getCurrentHoliday() {
        return $this->lastFeiertag;
    }

    /**
     * letzter Wochentagname in der Berechnung
     * 
     * @return string
     */
    public function getCurrentWeekday() {
        return $this->lastWochentag;
    }

    /**
     * Fehlermeldung
     * 
     * @return string
     */
    public function getError() {
        return $this->error;
    }

    // ---------------- private methods ------------
    private function isWorkday() {
        $this->anzahlWerkTage = $this->isWorkdayH($this->datumTS1);
    }

    private function countWorkdays() {
        $ts1 = min($this->datumTS1, $this->datumTS2);
        $ts2 = max($this->datumTS1, $this->datumTS2);
        $zaehler = 0;
        while ($ts1 <= $ts2) {
            if ($this->isWorkdayH($ts1)) {
                $zaehler++;
            }
            $ts1 = $this->addOneDay($ts1);
        }
        $this->anzahlWerkTage = $zaehler;
    }

    private function addWorkdays() {
        $ts = $this->datumTS1;
        $zaehler = 0;
        while ($zaehler < $this->anzahlWerkTage) {
            $ts = $this->addOneDay($ts);
            if ($this->isWorkdayH($ts)) {
                $zaehler++;
            }
        }
        $this->datumTS2 = $ts;
    }

    private function getBussBettag($year) {
        return date('m-d', mktime(0, 0, 0, 11, 26 + (7 - date('w', mktime(0, 0, 0, 11, 26, $year))) - 11, $year));
    }

    private function addOneDay($ts) {
        return strtotime("+ 1 day", $ts);
    }

    private function getWeekday($ts) {
        $ret = date("w", $ts);
        $this->lastWochentag = $this->wochenTagNamen[$this->land][$ret];
        return $ret;
    }

    private function isWorkdayH($ts) {
        $aktdat = date("m-d", $ts);
        $aktwotag = $this->getWeekday($ts);
        $aktyear = date("Y", $ts);

        $osonTm = easter_date((integer) $aktyear);

        if (in_array((integer) $aktwotag, $this->wochenArbTage)) {
            for ($i = 0; $i < count($this->feiertagArr); $i++) {
                // explitites Feiertagsdatum
                if ($this->feiertagArr[$i][0] == $aktdat) {
                    $this->lastFeiertag = $this->feiertagArr[$i][1][$this->land];
                    return 0;
                }
                // Easterday-Regel
                elseif (substr($this->feiertagArr[$i][0], 0, 1) == "E") {
                    $easterTs = mktime(3, 0, 0, date("n", $osonTm), date("j", $osonTm) + substr($this->feiertagArr[$i][0], 1), $aktyear);
                    if ($aktdat == date("m", $easterTs) . "-" . date("d", $easterTs)) {
                        $this->lastFeiertag = $this->feiertagArr[$i][1][$this->land];
                        return 0;
                    }
                }
                // Sonderfall Buß-und Bettag
                elseif ($this->feiertagArr[$i][0] == "bb") {
                    if ($aktdat == $this->getBussBettag($aktyear)) {
                        $this->lastFeiertag = $this->feiertagArr[$i][1][$this->land];
                        return 0;
                    }
                }
                // Sonderfall Stephanstag am Montag
                elseif ($this->feiertagArr[$i][0] == "stmo") {
                    if ($aktwotag == 1 && $aktdat == "12-26") {
                        $this->lastFeiertag = $this->feiertagArr[$i][1][$this->land];
                        return 0;
                    }
                }
            }
        } else
            return 0;
        return 1;
    }

}
