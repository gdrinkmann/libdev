<?php
namespace gg\lib;

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * ggLib CORE
 * fills excel template with data array
 * https://github.com/PHPOffice/PhpSpreadsheet
 * 
 * class gg\lib\excel
 * @author Gerd
 * 
 */
class excel {

    private $error = false;

    /**
     * @var $excelObj for extern handling with PhpSpreadsheet methods
     */
    public $excelObj;

    /**
     * constructor
     * @param string $template
     */
    public function __construct($template) {

        try {
            $templatePath = \gg\combinePathFile(CFG_TEMPLATEDIR, $template);
            if (is_file($templatePath)) {
                $this->excelObj = \PhpOffice\PhpSpreadsheet\IOFactory::load($templatePath);
                return true;
            } else {
                throw new Exception("ggexcel-read: template not found");
            }
        } catch (Exception $e) {
            $this->error = "ggexcel-load: " . $e->getMessage();
        }
    }

    /**
     * helper to convert date(time) to exceltime
     * @param object|int|string $datetime
     * @return int
     */
    public function getExcelDatetime($datetime) {
        return \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($datetime);
    }

    /**
     * getter error
     * @return bool/string
     */
    public function getError() {
        return $this->error;
    }

    /**
     * Main method
     * @param array $data
     * @param $sheetindex int sheetindex to fill in
     * @param $hasletters bool use of column letters in the data array
     * @return string|false filename full path
     */
    public function saveExcel($data, $sheetindex = 0, $hasletters = true) {
        if (!$this->fill($data, $sheetindex, $hasletters)) {
            return false;
        }
        return $this->write();
    }

    /**
     * fill the desired sheet with data
     * @param array $data array [row1=>[col1=>data, col2=>data,...], row2=>[...], ...] with $row=1,2,... and $col=A,B,...
     * @param int $sheetindex
     * @param bool $hasletters letters in data array
     * @return bool
     */
    public function fill($data, $sheetindex = 0, $hasletters = true) {
        if (!$this->error) {
            if (is_array($data)) {
                try {
                    $this->excelObj->setActiveSheetIndex($sheetindex);
                    $worksheet = $this->excelObj->getActiveSheet();
                    foreach ($data as $rowno => $row) {
                        if (!is_array($row)) {
                            throw new Exception("ggexcel-fill: wrong data format 2");
                        }
                        foreach ($row as $colno => $celldata) {
                            if (!$hasletters) {
                                $colno = $this->num2alpha($colno);
                            }
                            $worksheet->setCellValue($colno . $rowno, excelEscape($celldata));
                        }
                    }
                    return true;
                } catch (Exception $e) {
                    $this->error = "ggexcel-fill: " . $e->getMessage();
                }
            } else {
                $this->error = "ggexcel-fill: wrong data format 1";
            }
        }
        return false;
    }

    /**
     * write Excel file
     * @return string|false filename
     */
    public function write() {
        if (!$this->error) {
            try {
                $uniqueFile = tempnam(CFG_TEMPDIR, 'ggexcel');
                $objWriter = new Xlsx($this->excelObj);
                $objWriter->save($uniqueFile);
                return $uniqueFile;
            } catch (Exception $e) {
                $this->error = "ggexcel-write: " . $e->getMessage();
            }
        }
        return false;
    }

    /**
     * Helper: Converts an integer into the alphabet base (A-Z,AA...)
     * @param int $n the number to convert.
     * @return string The converted number.
     * @author Theriault (from http://php.net/manual/de/function.base-convert.php)
     */
    public function num2alpha($n) {
        $r = '';
        for ($i = 1; $n >= 0 && $i < 10; $i++) {
            $r = chr(0x41 + ($n % pow(26, $i) / pow(26, $i - 1))) . $r;
            $n -= pow(26, $i);
        }
        return $r;
    }

    /**
     * Helper: Converts an alphabetic string into an integer
     * @param int $a the letter(s) to convert
     * @return int The converted alpha.
     * @author Theriault (from http://php.net/manual/de/function.base-convert.php)
     */
    public function alpha2num($a) {
        $r = 0;
        $l = strlen($a);
        for ($i = 0; $i < $l; $i++) {
            $r += pow(26, $i) * (ord($a[$l - $i - 1]) - 0x40);
        }
        return $r - 1;
    }

}
