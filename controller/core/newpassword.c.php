<?php
namespace gg;
/**
 * ggLib sample
 */

$PAGE->setTitle(_t('head_newpassword', 'user'));

$user = new user();

$userArr = getCurrentUser();

$oldpwneeded = false;

$usertoken = _get('usertoken', [['hex']], 0);
if ($usertoken && is_null($userArr)) {
    $userArr = $user->getUserByToken($usertoken);
    if (is_null($userArr)) {
        _m(['msg_wrongtoken'], lib\PRIO_ERROR);
        _re(lib\CFG_STARTPAGE);
    }
} else {
    $PAGE->requiresAuth();
    $oldpwneeded = true;
}

$PAGE->initForm('newpassword');

if ($PAGE->form()->isValidSubmit()) {
    if ($oldpwneeded) {
        $userObj = $userDB->verifyLogin($userObj->username, _post('oldpassword'));
        if (!$userObj) {
            _m(['msg_wrongcredentials'], lib\PRIO_ERROR, true);
        }
    }
    if ($userObj) {
        $userObj->password = _post('password');
        if ($userDB->saveRecord($userObj)) {
            _m(['msg_savepwsuccess'], lib\PRIO_SUCCESS);
        } else {
            _m(['msg_savepwfail'], lib\PRIO_ERROR);
        }
        _re(lib\CFG_STARTPAGE);
    }
}

if ($oldpwneeded) {
    $rules = [['required', _t('err_passwordRequired', 'user')],
              ['length', _t('err_passwordRequired', 'user'), ['min' => 8, 'max' => 50]],
              ['text', _t('err_passwordRequired', 'user')]];
    $PAGE->form()->addPassword('oldpassword', '', $rules, '<i class="bi bi-unlock-fill"></i> '._t('lbl_oldpassword', 'user'));
}

$rules = [['required', _t('err_passwordRequired', 'user')],
          ['length', _t('err_passwordRequired', 'user'), ['min' => 8, 'max' => 50]],
          ['text', _t('err_passwordRequired', 'user')]];
$PAGE->form()->addPassword('password', '', $rules, '<i class="bi bi-unlock-fill"></i> '._t('lbl_password', 'user'));

$rules = [['required', _t('err_passwordRequired', 'user')],
          ['inlist', _t('err_passwordNotEqual', 'user'), ['list' => '{{password}}']]];
$PAGE->form()->addPassword('password2', '', $rules, '<i class="bi bi-unlock-fill"></i> '._t('lbl_repeatpassword', 'user'));

$PAGE->form()->addSubmitButton(_t('btn_newpassword', 'user'));

$PAGE->controllerAdd('userObj', $userObj);