<?php
namespace gg;
/**
 * ggLib sample
 * Bootstrap 5
 * current $PAGE object is available
 */
?>
<!doctype html>
<html lang="<?= _lang() ?>">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?= $PAGE->loadCSS() ?>
        <title><?= lib\CFG_PROJECTNAME . ': ' . $PAGE->getTitle() ?></title>
    </head>
    <body id="page_<?= $PAGE->getName() ?>">
        
<?php include 'nav.include.php' ?>
        
<?php
    if ($PAGE->getName() == 'start') {
        include 'carousel.include.php';
    }
?>
        
        <div class="container ps-0 pe-0 mt-2">
            
<?php include 'alert.include.php' ?>
            
            <div class="card bg-light">

                <?= $PAGE->getViewContent() ?>
                
                <div class="card-footer text-muted">
                    <small><?= lib\CFG_PROJECTNAME ?> by Gerd (ggLib Version <?= getversion() ?>)</small>
                </div>
                
            </div>
        </div>
        <?= $PAGE->loadJS() ?>
    </body>
</html>
