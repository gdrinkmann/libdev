<?php
namespace gg;
/**
 * ggLib sample
 * Bootstrap 5
 */
?>
<h5 class="card-header d-flex">
    <?= _t('head_newpassword', 'user') ?>
    <a href="<?= lib\CFG_BASEURI ?>" class="card-link ms-auto"><i class="bi bi-house-door-fill"></i></a>
</h5>
<div class="card-body">
    <p class="card-text"><?= _t('txt_resetpassword', 'user', ['username' => $PAGE->controller()->userObj->name]) ?></p>
<?= $PAGE->form()->getTotalHtml('A') ?>
</div>
