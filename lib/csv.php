<?php
namespace gg\lib;

/**
 * ggLib CORE
 * Konvertierung von CSV-String zu indiziertem CSV-Array und umgekehrt
 * mit Ausgabe von Prozessinfos
 * 
 * Array-Format:
 * array(	array(field1, field2, ...),
 * 			array(field1, field2, ...),
 * 			...)
 * 
 * Grundlage ist RFC4180 mit folgenden Erweiterungen:
 * 
 *   - als <delimiter> werden wahlweise tabulator, semikolon, comma, space , pipe akzeptiert
 *   - als <enclosure> kann wahlweise singlequote oder doublequote verwendet werden
 *   - als <linebreak> kann CR, LF oder beides verwendet werden
 * 
 *   - die Headline (1. Zeile) bestimmt die Anzahl der Felder
 * 
 *   - Priorität bei der Feld-Erkennung im String haben die <enclosure>.
 * 
 * Beim zeichenweise Fortschreiten innerhalb des CSV-Strings wird der Status
 * "innerhalb" oder "außerhalb von <enclosure>" eingenommen:
 * 
 * Für "innerhalb" gilt:
 *   - <enclosure> wird durch Voranstellen eines zusätzlichen <enclosure> maskiert
 *   - <enclosure> darf nur vor <delimiter> oder <linebreak> stehen
 *   - <delimiter> und <linebreak> werden als literal behandelt
 *   - jedes andere Zeichen gehört zum Feldwert
 *   
 * Für "außerhalb" gilt:
 *   - <enclosure> darf nur hinter <delimiter> oder am Zeilenanfang stehen
 *   - <delimiter> ist Feldtrenner
 *   - <linebreak> markiert das Zeilenende
 *   - jedes andere Zeichen gehört zum Feldwert
 *   
 *   
 * folgende Felder sind demnach fehlerhaft und werden nicht verarbeitet:
 * 
 * ...<delimiter><enclosure>abc<enclosure> <delimiter>...
 * ...<delimiter> <enclosure>abc<enclosure><delimiter>...
 * ...<delimiter>a<enclosure>bc<delimiter>...
 * Zit.2.4.: "Spaces are considered part of a field and should not be ignored."
 * Zit.2.5.: "If fields are not enclosed with double quotes, then
 *            double quotes may not appear inside the fields."
 *  
 * class gg\lib\csv
 * @author Gerd
 */
class csv {

    private $input; // input array oder string
    private $inputisArray; // input-typ 
    private $countLines = 0; // Zeilen-Zähler
    private $maxLines = 0; // Ausgabe Anzahl
    private $delimiter = "\t"; // Feldtrennzeichen (default tabulator)
    private $enclosure = "\""; // Textbegrenzungszeichen (default doublequote)
    private $forceEnclosure = false; // Textbegrenzungszeichen erzeugen auch wenn sie nicht notwendig sind
    private $linebreak = "\n"; // Zeilenumbruchzeichen (default LineFeed, Unix)
    private $charsetIn = "UTF-8"; // Zeichenkodierung input
    private $charsetOut = ""; // Zeichenkodierung output
    private $skipFirstline = false; // erste Zeile bei output überspringen
    private $output; // output string oder array
    private $currentLine = array(); // aktuelle Zeile array
    private $pointer = 0; // Zeichen-Index bei String oder Zeilen-Index bei Array
    private $maxwarnings = 20; // max. Anzahl der Warnungen bevor ein Fehler geworfen wird
    private $symbols = array(array("\t" => "Tabulator", "," => "Komma", ";" => "Semikolon", " " => "Leerzeichen", "|" => "Pipesymbol"),
        array("\"" => "Doppeltes Anführungszeichen", "'" => "Einfaches Anführungszeichen"),
        array("\n" => "LF (Unix)", "\r\n" => "CRLF (Windows)", "\r" => "CR (Mac OS alt)")
    );

    /**
     * @var array Infos zum Prozess
     */
    public $protokoll = array();

    /**
     * @var integer Anzahl der Warnungen für den output
     */
    public $warnings = 0;

    /**
     * constructor
     * 
     * @param string|array $input
     */
    public function __construct($input = false) {
        if ($input !== false) {
            $this->input($input);
        }
    }

    /**
     * Setter für die CSV-Eingabe
     * 
     * @param string|array $p 
     */
    public function input($p) {
        if (gettype($p) == "string") {
            $this->inputisArray = false;
            $this->input = $p;
        } elseif (gettype($p) == "array") {
            $this->inputisArray = true;
            $this->input = $p;
        } else {
            $this->protokoll[] = "Kein geeigneter Input-Typ";
        }
        $this->countLines = 0;
        $this->pointer = 0;
        unset($this->output);
    }

    /**
     * Setter für maxLines
     * Anzahl der Ausgabezeilen (0=alle)
     * 
     * @param integer $p default 0
     */
    public function maxLines($p) {
        $this->maxLines = (int) $p;
    }

    /**
     * Setter für delimiter
     *  \t tabulator
     *  , comma
     *  ; semikolon
     *    space
     *  | pipe
     * 
     * @param string $p default \t
     */
    public function delimiter($p) {
        if (isset($this->symbols[0][$p])) {
            $this->delimiter = $p;
        }
    }

    /**
     * Setter für enclosure
     *  ' singlequote
     *  " doublequote
     * 
     * @param string $p default "
     */
    public function enclosure($p) {
        if (isset($this->symbols[1][$p])) {
            $this->enclosure = $p;
        }
    }

    /**
     * Setter für forceEnclosure
     * true =enclosure überall
     * false=enclosure nur dort wo notwendig
     * 
     * @param bool $p default false
     */
    public function forceEnclosure($p) {
        $this->forceEnclosure = (bool) $p;
    }

    /**
     * Setter für linebreak
     *  \n LF (Unix)
     *  \r CR
     *  \r\n CRLF (Windows)
     * 
     * @param string $p default \n
     */
    public function linebreak($p) {
        if (isset($this->symbols[2][$p])) {
            $this->linebreak = $p;
        }
    }

    /**
     * Setter für Input-Charcterset
     *  z.B. "UTF-8", "ASCII", "Windows-1252", "ISO-8859-15", "ISO-8859-1", "ISO-8859-6", "CP1256"
     * 
     * @param string $p default UTF-8
     */
    public function charsetIn($p) {
        $this->charsetIn = substr($p, 0, 20);
    }

    /**
     * Setter für Output-Characterset
     * 
     * @param string $p default charsetIn
     */
    public function charsetOut($p) {
        $this->charsetOut = substr($p, 0, 20);
    }

    /**
     * Setter für skipFirstline
     * 
     * @param bool $p default false
     */
    public function skipFirstLine($p) {
        $this->skipFirstline = (bool) $p;
    }

    /**
     * Hauptmethode zur Rückgabe des konvertierten CSV-Strings/-Arrays
     * 
     * @return false|string|array
     */
    public function getCsv() {
        if (!$this->charsetOut) {
            $this->charsetOut = $this->charsetIn;
        }
        if (!$this->input) {
            $this->protokoll[] = "Kein Input vorhanden!";
            return false;
        }

        if ($this->inputisArray) {
            $inputLen = count($this->input);
            $this->protokoll[] = "Input: Array mit {$inputLen} Einträgen";
        } else {
            $ze = substr($this->input, -1);
            $ze2 = substr($this->input, -2);
            if ($ze != $this->linebreak && $ze2 != $this->linebreak) {
                $this->input .= $this->linebreak;
            }
            $inputLen = strlen($this->input);
            $this->protokoll[] = "Input: String mit {$inputLen} Zeichen";
        }

        $this->protokoll[] = "Einstellungen: Feldtrenner " . $this->symbols[0][$this->delimiter] . " / Textbegrenzung " . $this->symbols[1][$this->enclosure] . " / Zeilenende " . $this->symbols[2][$this->linebreak];

        // Headline lesen
        if (!$this->getLine()) {
            return false;
        }
        $this->countLines++;

        // Feldanzahl der Headline ermitteln (Referenz)
        $countFields = count($this->currentLine);
        $this->protokoll[] = "Anzahl der Felder in Headerzeile: {$countFields}";

        if ($this->skipFirstline) {
            $this->protokoll[] = "Headerzeile nicht in der Ausgabe";
        } else {
            $this->addLine();
        }

        if ($this->maxLines > 0) {
            $this->protokoll[] = "Begrenzung der Anzahl der ausgegebenen Zeilen auf " . $this->maxLines;
        }

        // weitere Zeilen lesen
        while ($this->pointer < $inputLen && ($this->countLines <= $this->maxLines || $this->maxLines == 0)) {
            if (!$this->getLine()) {
                return false;
            }

            if ($countFields == count($this->currentLine)) {
                $this->addLine();
            } else {
                $this->protokoll[] = "Warnung: " . $this->lineText() . " wegen abweichender Feldanzahl (" . count($this->currentLine) . ") ausgelassen!";
                $this->warnings++;
                $this->countLines++;
            }

            if ($this->warnings > $this->maxwarnings) {
                $this->protokoll[] = "Maximale Anzahl an Warnungen ({$this->maxwarnings}) überschritten";
                return false;
            }
        }

        return $this->output;
    }

    /**
     * holt eine Zeile aus $input und sichert sie als array in $currentLine
     * 
     * manipuliert folgende Eigenschaften:
     *   $currentLine (array)
     *   $pointer (int)
     *   $protokoll (array)
     * 
     */
    protected function getLine() {
        $this->currentLine = array();

        if ($this->inputisArray) {
            $lineArr = $this->input[$this->pointer];

            if (gettype($lineArr) == "array") {
                foreach ($lineArr as $currentField) {
                    if (gettype($currentField) == "string" || gettype($currentField) == "integer" || gettype($currentField) == "double") {
                        $currentField = (string) $currentField;
                        $isenclosured = false;

                        if (preg_match("/^{$this->enclosure}.*{$this->enclosure}$/", $currentField)) {
                            $currentField = substr($currentField, 1, -1);
                            $isenclosured = true;
                        }
                        $currentField = str_replace("{$this->enclosure}{$this->enclosure}", "123456-------654321", $currentField);
                        $currentField = str_replace($this->enclosure, $this->enclosure . $this->enclosure, $currentField, $ecount);
                        if ($ecount > 0) {
                            $this->protokoll[] = "{$ecount} Textbegrenzer maskiert in " . $this->lineText() . ", Feld " . (count($this->currentLine) + 1);
                        }
                        $currentField = str_replace("123456-------654321", $this->enclosure . $this->enclosure, $currentField);

                        if (strpos($currentField, $this->enclosure) != false ||
                                strpos($currentField, $this->linebreak) != false ||
                                strpos($currentField, $this->delimiter) != false ||
                                $this->forceEnclosure ||
                                $isenclosured) {
                            if (!$this->forceEnclosure && !$isenclosured) {
                                if (strpos($currentField, $this->enclosure) != false) {
                                    $this->protokoll[] = "Textbegrenzung hinzugefügt wegen Vorhandensein von Textbegrenzer in " . $this->lineText() . ", Feld " . (count($this->currentLine) + 1);
                                }
                                if (strpos($currentField, $this->linebreak) != false) {
                                    $this->protokoll[] = "Textbegrenzung hinzugefügt wegen Vorhandensein von Zeilenumbruch in " . $this->lineText() . ", Feld " . (count($this->currentLine) + 1);
                                }
                                if (strpos($currentField, $this->delimiter) != false) {
                                    $this->protokoll[] = "Textbegrenzung hinzugefügt wegen Vorhandensein von Feldtrenner in " . $this->lineText() . ", Feld " . (count($this->currentLine) + 1);
                                }
                            }
                            $currentField = $this->enclosure . $currentField . $this->enclosure;
                        }
                        $this->currentLine[] = $this->konv($currentField);
                    } else {
                        $this->protokoll[] = "Falscher Datentyp im Input-Array in " . $this->lineText() . ", Feld " . (count($this->currentLine) + 1);
                        return false;
                    }
                }
            } else {
                $this->protokoll[] = "Falsches Array-Format im Input-Array in " . $this->lineText();
                return false;
            }
            $this->pointer++;
        } else { // input als string
            $currentField = "";
            $char = array();

            $char[0] = $this->input[$this->pointer]; // aktuelles Zeichen
            $char[-1] = ""; // vorheriges Zeichen

            $within = false; // innerhalb oder außerhalb Textbegrenzer

            while ($char[0] !== "") { // bis Ende des Strings oder break
                $char[1] = $this->input[$this->pointer + 1]; // nächstes Zeichen
                $char[2] = $this->input[$this->pointer + 2]; // übernächstes Zeichen

                if ($within) { // ------- innerhalb Textbegrenzung -------------

                    if ($char[0] == $this->enclosure && $char[1] == $this->enclosure) { // ist escapter Textbegrenzer
                        $currentField .= $this->enclosure;
                        $this->pointer++;

                        $this->protokoll[] = "Doppelter Textbegrenzer gefunden in " . $this->lineText() . ", Feld " . (count($this->currentLine) + 1);
                    } elseif ($char[0] == $this->enclosure && $char[1] == $this->delimiter) { // ist schließender Textbegrenzer
                        $this->currentLine[] = $this->konv($currentField);
                        $currentField = "";

                        $within = false;

                        $this->pointer++;
                    } elseif ($char[0] == $this->enclosure && $char[1] == $this->linebreak || $char[1] . $char[2] == $this->linebreak) { // ist schließender Textbegrenzer am Zeilenende
                        $this->currentLine[] = $this->konv($currentField);
                        $currentField = "";

                        $this->pointer += strlen($this->linebreak);
                        break;
                    } elseif ($char[0] == $this->enclosure) { // Textbegrenzer an falscher Stelle
                        $this->protokoll[] = "Falsche Platzierung des Textbegrenzers in " . $this->lineText() . ", Feld " . (count($this->currentLine) + 1);
                        return false;
                    } else {
                        $currentField .= $char[0];

                        if ($char[0] == $this->delimiter) {
                            $this->protokoll[] = "Feldtrenner gefunden innerhalb Textbegrenzung in " . $this->lineText() . ", Feld " . (count($this->currentLine) + 1);
                        } elseif ($char[0] == $this->linebreak || $char[0] . $char[1] == $this->linebreak) {
                            $this->protokoll[] = "Zeilenumbruch gefunden innerhalb Textbegrenzung in " . $this->lineText() . ", Feld " . (count($this->currentLine) + 1);
                        }
                    }
                } else { // ------- außerhalb Textbegrenzung -------------

                    if ($char[0] == $this->enclosure && ($char[-1] == "" || $char[-1] == $this->delimiter)) { // ist öffnender Textbegrenzer
                        $within = true;
                    } elseif ($char[0] == $this->enclosure) { // Textbegrenzer an falscher Stelle
                        $this->protokoll[] = "Falsche Platzierung des Textbegrenzers in " . $this->lineText() . ", Feld " . (count($this->currentLine) + 1);
                        return false;
                    } elseif ($char[0] == $this->delimiter) { // ist Feldbegrenzer
                        $this->currentLine[] = $this->konv($currentField);
                        $currentField = "";
                    } elseif ($char[0] == $this->linebreak || $char[0] . $char[1] == $this->linebreak) { // ist Zeilenende
                        $this->currentLine[] = $this->konv($currentField);
                        $currentField = "";

                        $this->pointer += strlen($this->linebreak);
                        break;
                    } else {
                        $currentField .= $char[0];
                    }
                }

                $char[-1] = $this->input[$this->pointer]; // vorhergehendes Zeichen

                $this->pointer++;

                $char[0] = $this->input[$this->pointer];
            }
        }
        return true;
    }

    /**
     * ergänzt $output mit dem aktuellen Zeileninhalt
     */
    protected function addLine() {
        if ($this->inputisArray) {
            $this->output .= implode($this->delimiter, $this->currentLine) . $this->linebreak;
        } else {
            $this->output[] = $this->currentLine;
        }
        $this->countLines++;
    }

    /**
     * Charset-Konvertierung mithilfe der beiden Eigenschaften
     * $charsetIn und $charsetOut
     * 
     * @param string $t der zu konvertierende String
     * 
     * @return string
     */
    protected function konv($t) {
        if ($this->charsetIn == $this->charsetOut) {
            return $t;
        } else {
            $ret = iconv($this->charsetIn, $this->charsetOut . "//TRANSLIT", $t);

            if ($ret === false) {
                $this->protokoll[] = "Warnung: Konvertierung der Zeichenkodierung gescheitert in " . $this->lineText() . ", Feld " . (count($this->currentLine) + 1);
                $this->warnings++;
                return $t;
            }
            return $ret;
        }
    }

    /**
     * gibt entweder "Headerzeile" oder "Ausgabezeile #" aus
     * 
     * @return string
     */
    protected function lineText() {
        return ($this->countLines == 0) ? "Headerzeile" : ("Ausgabezeile " . $this->countLines);
    }

}
