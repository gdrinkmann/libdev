<?php
namespace gg\lib;

/**
 * ggLib CORE
 * class gg\lib\formItem
 * defines a formItem, usually a html-input
 */
class formItem {
    
    public $initValue;
    public $label;
    public $help;
    public $rules;
    public $options;
    public $attributes;
    public $formId;
    public $itemType;
    public $inputId = '';
    public $inputName = null;
    public $nameIndex = 0;
    public $invalidIndex = false;
    public $ruleResult = '';
    
    /**
     * @return string|array
     */
    protected function getPost() {
        $postValue = null;
        if (!is_null($this->inputName)) {
            preg_match('/^([^\[\]]+)((\[[^\[\]]+?\])*)((\[\])?)$/', $this->inputName, $match);
            $postFirstKey = $match[1];
            $postMiddleBrackets = explode('][', substr($match[2], 1, -1));
            $postLastBrackets = !!$match[4];
            if (isset($_POST[$postFirstKey])) {
                $postValue = $_POST[$postFirstKey];
                foreach ($postMiddleBrackets as $key) {
                    $postValue = (isset($postValue[$key])) ? $postValue[$key] : $postValue;
                }
                if (!is_array($this->initValue) && $postLastBrackets) {
                    $postValue = $postValue[$this->nameIndex];
                }
            }
        }
        return $postValue;
    }
    
    /**
     * normalize the input name with []
     * and extend the input id if necessary
     */
    protected function normalizeName() {
        if (isset($this->inputName)) {
            if (is_array($this->initValue) && substr($this->inputName, -2) !== '[]') {
                $this->inputName .= '[]';
            } elseif (substr($this->inputName, -2) === '[]') {
                $this->inputId .= '-' . $this->nameIndex;
            }
        }
    }
    
    /**
     * normalize the rule array to [[ruleName, invalidText, [option1 => value, ..]], ...]
     */
    protected function normalizeRules() {
        $ruleArr = $this->rules;
        $normArr = array();
        foreach ($ruleArr as $rule) {
            $normRule = null;
            if (is_array($rule) && is_string($rule[0])) {
                switch (count($rule)) {
                    case 1:
                        $normRule = [strtolower($rule[0]), '', []];
                        break;
                    case 2:
                        if (is_array($rule[1])) {
                            $normRule = [strtolower($rule[0]), '', array_change_key_case(array_map('strtolower', $rule[1]))];
                        } else {
                            $normRule = [strtolower($rule[0]), $rule[1], []];
                        }
                        break;
                    case 3:
                        if (!is_array($rule[1]) && is_array($rule[2])) {
                            $normRule = [strtolower($rule[0]), $rule[1], array_change_key_case(array_map('strtolower', $rule[2]))];
                        } elseif (is_array($rule[1]) && !is_array($rule[2])) {
                            $normRule = [strtolower($rule[0]), $rule[2], array_change_key_case(array_map('strtolower', $rule[1]))];
                        }
                }
            } else {
                $normRule = [strtolower($rule), '', []];
            }
            if (!is_null($normRule)) {
                $normArr[] = $normRule;
            }
        }
        $this->rules = $normArr;
    }
    
    /**
     * normalize item with attributes
     */
    protected function normalizeItem() {
        $attrArray = $this->attributes;
        $ruleArray = $this->rules;
        
        foreach ($ruleArray as $rule) {
            $rulename = strtolower($rule[0]);
            $ruleOptions = $rule[2];
            
            if ($rulename === 'required') {
                if (!isset($ruleOptions['min']) || $ruleOptions['min'] > 0) {
                    $attrArray['required'] = 'required';
                }
            }
            if ($rulename === 'length') {
                if (isset($ruleOptions['min'])) {
                    $attrArray['minlength'] = $ruleOptions['min'];
                }
                if (isset($ruleOptions['max'])) {
                    $attrArray['maxlength'] = $ruleOptions['max'];
                }
            }
            if ($rulename === 'limit') {
                if (isset($ruleOptions['min'])) {
                    $attrArray['min'] = $ruleOptions['min'];
                }
                if (isset($ruleOptions['max'])) {
                    $attrArray['max'] = $ruleOptions['max'];
                }
                if (isset($ruleOptions['step'])) {
                    $attrArray['step'] = $ruleOptions['step'];
                } else {
                    $attrArray['step'] = 'any';
                }
            }
            if ($rulename === 'pattern') {
                if (isset($ruleOptions['pattern'])) {
                    $attrArray['pattern'] = substr($ruleOptions['pattern'], 1, -1);
                }
            }
            if ($rulename === 'file') {
                if (isset($ruleOptions['types'])) {
                    $attrArray['accept'] = $ruleOptions['types'];
                }
            }
            if (isset($ruleOptions['multiple']) && $ruleOptions['multiple']) {
                $attrArray['multiple'] = 'multiple';
            }
        }
        $this->attributes = $attrArray;
        
        // rule -> attribute:
        // accept:types -> accept=types (file)
        // accept:multiple -> multiple (file)
        // limit:max -> max (numeric types)
        // length:max -> maxlength (textarea, password, search, tel, text, url)
        // ...
        
        // attributes:
        // required
        // min max : number, range <number>
        //           date yyyy-mm-dd
        //           datetime-local yyyy-mm-ddThh:mm
        //           month yyyy-mm
        //           time hh:mm
        //           week yyyy-W##
        // step :    number, range (1)
        //           date yyyy-mm-dd (1 day)
        //           datetime-local yyyy-mm-ddThh:mm (1 day)
        //           month yyyy-mm (1 month)
        //           time hh:mm (60 sec)
        //           week yyyy-W## (1 week)
        //           or 'any'
        // accept :  file
        // minlength maxlength pattern
        // 
        // types: color, date, datetime-local, email, month, number, range, tel, time, url, week, file
        // characters to escape |:= (escape character #)
    }
    
    /**
     * store rules in session
     */
    protected function storeRules() {
        if (!is_null($this->inputName)) {
            $_SESSION['rules'][$this->formId][$this->inputName][$this->nameIndex] = $this->rules;
        }
    }
    
    /**
     * @param string $str
     * @return string
     */
    protected function escRule($str) {
        return str_replace(['|',':','='], ['#|','#:','#='], (string)$str);
    }
    
    /**
     * @return string
     */
    protected function rulesToStr() {
        $ruleArr = $this->rules;
        $rArr = array();
        foreach ($ruleArr as $rule) {
            $oArr = array($rule[0]);
            $options = $rule[2];
            foreach ($options as $option => $val) {
                $oArr[] = $this->escRule($option) . '=' . $this->escRule($val);
            }
            $rArr[] = implode(':', $oArr);
        }
        return implode('|', $rArr);
    }
    
    protected function indent($str, $tabCount) {
        $addStr = str_repeat(' ', $tabCount * 4);
        $ret = str_replace("\n", "\n".$addStr, $str);
        return $addStr . preg_replace('/'.$addStr.'$/', '', $ret);
    }
    
    /**
     * get the formItem HTML
     */
    public function get($isSubmitted, $layoutCode, $layoutGrid, $floatLabel) {
        die('gg: not a valid formItem class');
    }
}



