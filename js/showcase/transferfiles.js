/**
 * gg sample
 */

var sendoptions = [
    'filesRemain',
    'downloadForce',
    'htmlMail',
    'directMail',
    'useCurl',
    'useCmdCurl',
    'passiveFTP',
    'okFile',
    'use7z'
];
// sendmethods with corresponding input sections and available sendoptions
var sendmethods = {
    upload  : ['.specUfile', [2,3]],
    download: ['.specFile, .specZip', [0,1,2,3,8]],
    move    : ['.specFile, .specUrl, .specZip', [0,2,3,8]],
    emailatt: ['.specFile, .specZip', [0,2,3,8]],
    ftp     : ['.specFile, .specUrl, .specUrlAuth, .specZip', [0,2,3,4,5,6,7,8]],
    ftps    : ['.specFile, .specUrl, .specUrlAuth, .specZip', [0,2,3,4,5,6,7,8]],
    sftp    : ['.specFile, .specUrl, .specUrlAuth, .specZip', [0,2,3,4,5,7,8]],
    sftpkey : ['.specFile, .specUrl, .specKey, .specZip', [0,2,3,4,5,7,8]],
    scp     : ['.specFile, .specUrl, .specUrlAuth, .specZip', []],
    pportal : ['', []],
    httpPut : ['.specFile, .specUrl, .specUrlAuth, .specZip', []],
    httpPost: ['.specFile, .specUrl, .specUrlAuth, .specZip', []],
    webDav  : ['.specFile, .specUrl, .specUrlAuth, .specZip', []],
    rsync   : ['.specFile, .specUrl, .specUrlAuth, .specKey, .specZip', []]
};

var notYetImplemented = ['scp', 'httpPut', 'httpPost', 'webDav', 'rsync'];

$(function() {

    $('#gg-transfer-sendmethod').change(function(e) {
        $('#ggform-transfer').ggResetInputValidity();
        // init all inputs by disabling
        $('.sendspec').ggItemState('disable');
        // enable the submit button
        $('#gg-transfer-submitbutton').ggItemState('enable',{title:'<b>Now</b> you can <i>try</i> to submit',html:true,placement:'bottom'});
        // enable the sendoptions select and mail section
        $('#gg-transfer-sendoptions, .specMail').ggItemState('enable');

        var currentSendmethod = $(this).val();
        $(sendmethods[currentSendmethod][0]).ggItemState('enable');

        // init all sendoptions in the sendoptions select by disabling
        $('#gg-transfer-sendoptions option').prop('disabled', true).prop('selected', false);
        $.each(sendmethods[currentSendmethod][1], function(i,v) {
            $('#gg-transfer-sendoptions option').filter('[value="'+sendoptions[v]+'"]').prop('disabled', false);
        });
    });

    $.each(notYetImplemented, function(i,v) {
        $('#gg-transfer-sendmethod option').filter('[value="'+v+'"]').prop('disabled', true);
    });
    $('.sendspec').ggItemState('disable');
    $('#gg-transfer-submitbutton').ggItemState('disable', {title:'Choose a transfer method'});
    //$('#gg-transfer-sendmethod').change();
    $('#gg-transfer-sendmethod').first().focus();
});