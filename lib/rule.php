<?php
namespace gg\lib;

/**
 * ggLib CORE
 * rule class
 * is used by clientside input validation via ajax AND serverside validation
 * 
 * there is two factor validation implemented:
 * first level is users comfort level via javascript
 * second level is security level for preventing invalid post injections via php
 * 
 * standard included in index.php
 * 
 * class gg\lib\rule
 * @author Gerd
 */
class rule {
    
    private $value;
    private $options;
    private $ruleresult; // optional additional result, provided by the last processed rule
    private $error;
    
    /**
     * @param string|array $value to test against the given rules
     * @param array $rules [[ruleName, [optionName => optionValue, ...]], ...]
     * @return array ['invalid'=>false|index, 'ruleresult'=>mixed, 'error'=>bool]
     */
    public function _validate($value, $rules) {
        $invalid = false;
        
        $this->ruleresult = '';
        $this->error = false;
        
        $valArray = (array)$value;
        $ruleArray = (array)$rules;
        
        foreach ($ruleArray as $index => $rule) {
            $rulename = strtolower($rule[0]);
            $n = count($rule) - 1 ;
            $this->options = $rule[$n] ?? null;
            
            if ($rulename === 'required') { // meta rule 'required' with options 'min', 'max' and 'if'
                $this->value = $valArray;
                $valid = $this->$rulename();
                if (!$valid) {
                    $invalid = $index;
                    break;
                }
            } elseif (method_exists(__CLASS__, $rulename)) {
                
                // general option 'or'
                // isset 'or' means only one value item has to be valid,
                // otherwise (default) all value items must be valid
                $rulevalid = isset($this->options['or']) ? false : true;
                
                foreach ($valArray as $val) {
                    if ($rulename !== 'required') {
                        if (is_null($val) || $val === '') {
                            continue;
                        }
                    }
                    $this->value = $val;
                    if ($rulevalid) {
                        $rulevalid = $rulevalid && $this->$rulename();
                    } else {
                        $rulevalid = $rulevalid || $this->$rulename();
                    }
                }
                if (!$rulevalid) {
                    $invalid = $index;
                    break;
                }
            } else {
                $invalid = 0;
                $this->error = true;
                break;
            }
        }
        $resultArray = array();
        $resultArray['invalid'] = $invalid; // false = valid / index of ruleArray = invalid
        $resultArray['ruleresult'] = $this->ruleresult;
        $resultArray['error'] = $this->error;
        
        return $resultArray;
    }
    
    
    // ########### HERE ARE THE RULES ############
    // Every rule has to return bool and may optionally set this->ruleresult or in case of error this->error=true
    // - you may append additional rules here (and only here!)
    // - rule names and options are lowercase
    
    /** required (meta rule i.e. for use with multiple select)
     * option 'min' (positive integer default 1)
     * option 'max' (positive integer)
     * option 'if' meant to fill with placeholder {{inputName}}
     */
    private function required() {
        $min = $this->options['min'] ?? 1;
        $max = $this->options['max'] ?? null;
        $if = $this->options['if'] ?? true;

        $count = 0;
        foreach ($this->value as $val) {
            if (!is_null($val) && $val !== '') {
                $count++;
            }
        }
        $mincond = $count >= (int)$min;
        
        $ifcond = !((bool)$if && !$mincond);

        $maxcond = true;
        $this->ruleresult = $count;
        if (!is_null($max)) {
            $maxcond = $count <= (int)$max;
            $this->ruleresult = (int)$max - $count;
        }
        return $ifcond && $maxcond; 
    }
    /** length
     * option 'min' (positive integer default 1)
     * option 'max' (positive integer)
     * ruleresult: strLength or diff to max
     * @return bool
     */
    private function length() {
        $min = $this->options['min'] ?? 1;
        $max = $this->options['max'] ?? null;
        
        $len = strlen((string)$this->value);
        
        $maxcond = true;
        $this->ruleresult = $len;
        if (!is_null($max)) {
            $maxcond = $len <= (int)$max;
            $this->ruleresult = (int)$max - $len;
        }
        return $len >= (int)$min && $maxcond;
    }
    /** limit
     * option 'min' (number)
     * option 'max' (number)
     * option 'step' (number default 'any')
     * option 'initValue' (number)
     * ruleresult: value minus min or max minus value
     * @return bool
     */
    private function limit() {
        $min = $this->options['min'] ?? null;
        $max = $this->options['max'] ?? null;
        $step = $this->options['step'] ?? 'any';
        $ival = $this->options['initvalue'] ?? 0;
        // base for step: min or initValue or 0
        if ($step === 'any') {
            $stepCond = true;
            if (is_null($min)) {
                $minCond = true;
            } else {
                $minCond = (float)$this->value >= (float)$min;
            }
        } else {
            if ((float)$step === 0.0) {
                $this->error = true;
                return false;
            }
            if (is_null($min)) {
                $stepCond = fmod((float)$this->value - (float)$ival, (float)$step) === 0.0;
                $minCond = true;
            } else {
                $stepCond = fmod((float)$this->value - (float)$min, (float)$step) === 0.0;
                $minCond = (float)$this->value >= (float)$min;
                $this->ruleresult = (float)$this->value - (float)$min;
            }
        }
        if (is_null($max)) {
            $maxCond = true;
        } else {
            $maxCond = (float)$this->value <= (float)$max;
            $this->ruleresult = (float)$max - (float)$this->value;
        }
        return $stepCond && $minCond && $maxCond;
    }
    /** contains
     * option 'in' (string)
     * option 'start' (string)
     * option 'end' (string)
     * @return bool
     */
    private function contains() {
        $in = $this->options['in'] ?? null;
        $start = $this->options['start'] ?? null;
        $end = $this->options['end'] ?? null;
        if (!is_null($in)) {
            if (strpos((string)$this->value, (string)$in) === false) {
                return false;
            }
        }
        if (!is_null($start)) {
            if (strpos((string)$this->value, (string)$start) !== 0) {
                return false;
            }
        }
        if (!is_null($end)) {
            if (strpos(strrev((string)$this->value), strrev((string)$end)) !== 0) {
                return false;
            }
        }
        return true;
    }
    /** inlist
     * option 'list' (array|string comma separated list)
     * @return bool
     */
    private function inlist() {
        $list = $this->options['list'] ?? '';
        if (!is_array($list)) {
            $list = explode(',', (string)$list);
        }
        if (!in_array((string)$this->value, $list)) {
            return false;
        }
        return true;
    }
    /** pattern
     * option 'pattern' (string regex)
     * ruleresult: searchresult (considering parentheses in pattern)
     * @return bool
     */
    private function pattern() {
        $match = false;
        $rx = $this->options['pattern'] ?? '/.*/';
        if (preg_match((string)$rx, (string)$this->value, $m) === 1) {
            $match = true;
        }
        $this->ruleresult = json_encode($m);
        return $match;
    }
    /** bool
     * no option
     * @return bool
     */
    private function bool() {
        if (!in_array((string)$this->value, array('0', '1', 'false', 'true', 'no', 'yes'), true)) {
            return false;
        }
        return true;
    }
    /** numeric
     * no option
     * @return bool
     */
    private function numeric() {
        if (strlen((string)$this->value) > 0 && !is_numeric((string)$this->value)) {
            return false;
        }
        return true;
    }
    /** integer
     * no option
     * @return bool
     */
    private function integer() {
        if (strlen((string)$this->value) > 0 && (!is_numeric((string)$this->value) || (int)$this->value != (string)$this->value)) {
            return false;
        }
        return true;
    }
    /** divisible
     * option 'divisor' (int|float default 1)
     * @return bool
     */
    private function divisible() {
        $divisor = $this->options['divisor'] ?? '1';
        if ((float)$divisor === 0.0) {
            $this->error = true;
            return false;
        }
        if (strlen((string)$this->value) > 0 && (!is_numeric((string)$this->value) || fmod((float)$this->value, (float)$divisor) !== 0.0)) {
            return false;
        }
        return true;
    }
    /** digits
     * no option
     * @return bool
     */
    private function digits() {
        if (preg_match('/^[[:digit:]]*$/', (string)$this->value) === 0) {
            return false;
        }
        return true;
    }
    /** hex
     * no option
     * @return bool
     */
    private function hex() {
        if (preg_match('/^[[:xdigit:]]*$/', (string)$this->value) === 0) {
            return false;
        }
        return true;
    }
    /** alpha
     * no option
     * @return bool
     */
    private function alpha() {
        if (preg_match('/^[[:alpha:]]*$/', (string)$this->value) === 0) {
            return false;
        }
        return true;
    }
    /** alphanum
     * no option
     * @return bool
     */
    private function alphanum() {
        if (preg_match('/^[[:alnum:]]*$/', (string)$this->value) === 0) {
            return false;
        }
        return true;
    }
    /** datetime
     * option 'format' (string datetime default 'Y-m-d H:i:s')
     * @return bool
     */
    private function datetime() {
        $format = $this->options['format'] ?? 'Y-m-d H:i:s';
        $d = DateTime::createFromFormat($format, (string)$this->value);
        return $d && $d->format($format) === (string)$this->value;
    }
    /** email
     * option 'multiple' (bool default false)
     * @return bool
     */
    private function email() {
        $multi = $this->options['multiple'] ?? false;
        $mails = (string)$this->value;
        if ($multi) {
            $valArr = explode(',', $mails);
        } else {
            $valArr = (array)$mails;
        }
        foreach ($valArr as $email) {
            if (filter_var(trim($email), FILTER_VALIDATE_EMAIL) === false) {
                return false;
            }
        }
        return true;
    }
    /** url
     * no option
     * @return bool
     */
    private function url() {
        return (bool)filter_var((string)$this->value, FILTER_VALIDATE_URL);
    }
    /** ip
     * no option
     * @return bool
     */
    private function ip() {
        return (bool)filter_var((string)$this->value, FILTER_VALIDATE_IP);
    }
    /** color
     * no option
     * @return bool
     */
    private function color() {
        return preg_match('/^#[0-9a-f]{6}$/', (string)$this->value) === 1;
    }
    /** tel
     * no option
     * @return bool
     */
    private function tel() {
        return preg_match('/^[+0-9][0-9 \-\/(\)]+$/', (string)$this->value) === 1;
    }
    /** file
     * for <input type=file> only
     * value must be an array of file objects with properties name and size
     * option 'types' (string) comma separated list of extensions with .
     * option 'filesize' (int) in kB (less is valid)
     * option 'multiple' (bool)
     * @return bool
     */
    private function file() {
        if (!is_array($this->value)) {
            $this->error = true;
            return false;
        }
        $vArr = $this->value;
        if (!isset($this->options['multiple']) && count($vArr) > 1) {
            return false;
        }
        $types = $this->options['types'] ?? '';
        $size = $this->options['filesize'] ?? null;
        $tarray = explode(',', $types);
        foreach ($vArr as $fileobj) {
            if (!is_null($size)) {
                if (((int)$fileobj->size / 1000) > $size) {
                    return false;
                }
            }
            foreach ($tarray as $type) {
                if (!fnmatch('*'.$type, $fileobj->name)) {
                    return false;
                }
            }
        }
        return true;
    }
    /** json
     * no option
     * @return bool
     */
    private function json() {
        return substr((string)$this->value, 0, 1) === '[' || substr((string)$this->value, 0, 1) === '{';
    }
    /** text
     * no option
     * @return bool
     */
    private function text() {
        return stripos((string)$this->value, '<script') === false && stripos((string)$this->value, '<iframe') === false;
    }
    
}
