<?php
namespace gg;
/**
 * ggLib sample
 */

class history extends lib\db {
    
    const HistoryName = 'history';
    const UseridName = 'user_id';
    const CreatetimeName = 'createtime';
    const TableName = 'tablename';
    const FieldName = 'fieldname';
    const RefidName = 'refid';
    const SequenceName = 'sequence';
    const NewvalueName = 'newvalue';
    
    /**
     * log field changes in db
     * @param string $tablename
     * @param object $newFieldsObj
     */
    public static function logChange($tablename, $newFieldsObj) {
        
        $pkn = lib\CFG_PRIMARYKEYNAME;
        
        $vhn = self::HistoryName;
        $vun = self::UseridName;
        $vcn = self::CreatetimeName;
        $vtn = self::TableName;
        $vfn = self::FieldName;
        $vrn = self::RefidName;
        $vsn = self::SequenceName;
        $vnn = self::NewvalueName;
        
        $time = date('Y-m-d H:i:s');
        
        if ($me = getCurrentUser() && $tablename !== __CLASS__ && isset($newFieldsObj->$pkn)) {
            
            $cs = self::getLastValues($tablename, $newFieldsObj->$pkn);
            $histCollection = [];
            foreach ($cs as $c) {
                $histCollection[$c->f->fn] = ['lastval' => $c->f->nv, 'lastseq' => $c->f->ms];
            }
            $m = __NAMESPACE__ . '\\' . $vhn;
            $hist = new $m();
            foreach($newFieldsObj as $fieldname => $newval) {
                if (!isset($histCollection[$fieldname]) || $newval !== $histCollection[$fieldname]['lastval']) {
                    $hist->init();
                    $hist->f->$vun = $me->f->$pkn;
                    $hist->f->$vcn = $time;
                    $hist->f->$vtn = $tablename;
                    $hist->f->$vfn = $fieldname;
                    $hist->f->$vrn = $newFieldsObj->$pkn;
                    $hist->f->$vsn = ($histCollection[$fieldname]['lastseq'] ?? 0) + 1;
                    $hist->f->$vnn = substr($newval, 0, $hist->fieldList[$vnn]['para']);
                    $hist->save();
                }
            }
        }
    }
    
    public static function getLastValues($tablename, $id) {
        $queryBAK = <<<EOTBAK
            SELECT
                `h1`.`fieldname` `fn`,
                `h3`.`maxseq` `ms`,
                `h1`.`newvalue` `nv`
            FROM `history` `h1`
            LEFT JOIN
            (
            SELECT
                `h2`.`tablename` `tn`,
                `h2`.`refid` `ri`,
                `h2`.`fieldname` `field`,
                MAX(`h2`.`sequence`) `maxseq`
            FROM `history` `h2`
                WHERE
                    `h2`.`tablename` = 'user' AND
                    `h2`.`refid` = 2
            GROUP BY `field`
            ) `h3`
            ON
                `h1`.`tablename` = `h3`.`tn` AND
                `h1`.`refid` = `h3`.`ri` AND
                `h1`.`fieldname` = `h3`.`field` AND
                `h1`.`sequence` = `h3`.`maxseq`
            WHERE
                `h3`.`field` IS NOT NULL
            EOTBAK;
        
        // show create table 'history';
        $tablecreateBAK = <<<EOTTBAK
            CREATE TABLE `history` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `user_id` int(11) NOT NULL,
              `createtime` datetime NOT NULL,
              `tablename` varchar(150) NOT NULL,
              `fieldname` varchar(150) NOT NULL,
              `refid` int(11) NOT NULL,
              `sequence` int(11) NOT NULL COMMENT 'regarding tablename, fieldname and refid',
              `newvalue` varchar(255) DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `IX_USER` (`user_id`),
              KEY `IX_REFID` (`refid`) USING BTREE,
              KEY `IX_FIELD` (`tablename`,`fieldname`,`refid`) USING BTREE,
              CONSTRAINT `history_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4
            EOTTBAK;
        
        $vhn = self::HistoryName;
        $vtn = self::TableName;
        $vfn = self::FieldName;
        $vrn = self::RefidName;
        $vsn = self::SequenceName;
        $vnn = self::NewvalueName;
        $query = <<<EOT
            SELECT
                `h1`.`$vfn` `fn`,
                `h3`.`maxseq` `ms`,
                `h1`.`$vnn` `nv`
            FROM `$vhn` `h1`
            LEFT JOIN
            (
            SELECT
                `h2`.`$vtn` `tn`,
                `h2`.`$vrn` `ri`,
                `h2`.`$vfn` `field`,
                MAX(`h2`.`$vsn`) `maxseq`
            FROM `$vhn` `h2`
                WHERE
                    `h2`.`$vtn` = ? AND
                    `h2`.`$vrn` = ?
            GROUP BY `field`
            ) `h3`
                ON
                    `h1`.`$vtn` = `h3`.`tn` AND
                    `h1`.`$vrn` = `h3`.`ri` AND
                    `h1`.`$vfn` = `h3`.`field` AND
                    `h1`.`$vsn` = `h3`.`maxseq`
            WHERE
                `h3`.`field` IS NOT NULL
            EOT;
        $params = [$tablename, $id];

        $m = __NAMESPACE__ . '\\' . $vhn;
        $hist = new $m();
        return $hist->getSqlRecords($query, $params);
    }
    
}