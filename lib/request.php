<?php
namespace gg\lib;

/**
 * ggLib CORE
 * webclient request properties and functions
 * _POST and _GET handling
 * 
 * standard included in index.php
 * 
 * class gg\lib\request
 * @author Gerd
 */
class request {
    
    public $url = '';
    
    public $pageName = '';
    
    public $scheme = '';
    public $host = '';
    public $port = '';
    public $user = '';
    public $pass = '';
    public $path = '';
    public $queryArr = array();
    public $query = '';
    public $fragment = '';
    
    public $dirname = '';
    public $basename = '';
    public $extension = '';
    public $filename = '';
    
    public $formRules = null;
    public $submittedFormId = null;
    public $submitInvalid = null; // null|false|int
    public $ggpost = null;
    
    /**
     * set pageName property
     */
    private function setPageName() {
        $name = self::pathCombine($this->dirname, $this->filename);
        if (!$name) {
            $name = CFG_STARTPAGE;
        } elseif (is_null($this->extension) || strtolower($this->extension) !== 'html') {
            $name = '__page_with_invalid_extension__';
        }
        $this->pageName = self::clearLeadingSlash($name);
    }
    
    /**
     * get page name
     * @return string
     */
    public function getPageName() {
        return $this->pageName;
    }
    
    /**
     * set url- and path-properties and set pageName
     */
    public function readUrl() {
        $urlArr = parse_url($this->url);
        $this->scheme = $urlArr['scheme'] ?? '';
        $this->host = $urlArr['host'] ?? '';
        $this->port = $urlArr['port'] ?? '';
        $this->user = $urlArr['user'] ?? '';
        $this->pass = $urlArr['pass'] ?? '';
        $this->path = $urlArr['path'] ?? '/';
        $this->query = $urlArr['query'] ?? '';
        parse_str($this->query, $this->queryArr);
        $this->fragment = $urlArr['fragment'] ?? null;
        
        $pathArr = pathinfo($this->path);
        $this->dirname = $pathArr['dirname'] ?? '';
        $this->basename = $pathArr['basename'] ?? '';
        $this->extension = $pathArr['extension'] ?? null;
        $this->filename = $pathArr['filename'] ?? '';
        
        $this->setPageName();
    }
    
    /**
     * read current url
     * and fill $_GET
     */
    public function readCurrentUrl() {
        $this->url = self::pathCombine(CFG_BASEURI, $_SERVER['REQUEST_URI']);
        $this->readUrl();
        parse_str($this->query, $_GET);
    }
    
    /**
     * used properties:
     * mandatory: pageName
     * optional: scheme, user, pass, host, port, extension, query, queryArr, fragment
     */
    public function getUrl() {
        $auth = ($this->user && $this->pass) ? $this->user . ':' . $this->pass . '@' : '';
        
        $webroot = ($this->scheme && $this->host) ? $this->scheme . '://' . $auth . $this->host : CFG_BASEURI;
        $webroot .= ($this->port) ? ':' . $this->port : '';
        
        $this->url = $this->pathCombine($webroot, $this->pageName);
        $this->url .= ($this->extension) ? '.' . $this->extension : '.html';
        
        $para = (!empty($this->queryArr)) ? http_build_query($this->queryArr) : $this->query;
        
        $this->url .= ($para) ? '?' . $para : '';
        $this->url .= ($this->fragment) ? '#' . $this->fragment : '';
    }
    
    /**
     * store this as referer in session
     * @param string $fragment optional
     */
    public function storeReferer($fragment = '') {
        $_SESSION['referer']['name'] = $this->pageName;
        $_SESSION['referer']['query'] = $this->queryArr;
        $_SESSION['referer']['fragment'] = $fragment;
    }
    
    /**
     * @param string ...$parts
     * @return string
     */
    public static function pathCombine(...$parts) {
        return \gg\combine('/', $parts);
    }
    
    /**
    * add leading slash if necessary
    * @param string $str
    * @return string
    */
    public static function addLeadingSlash($str) {
        return (substr($str, 0, 1) === '/') ? $str : '/' . $str;
    }
    
    /**
    * clear leading slash if nessecary
    * @param string $str
    * @return string
    */
    public static function clearLeadingSlash($str) {
        return (substr($str, 0, 1) === '/') ? substr($str, 1) : $str;
    }
    
    /**
     * @param string $pageName
     * @param array $queryArr
     * @param string $fragment
     * @return string
     */
    public static function buildUrl($pageName, $queryArr = array(), $fragment = '') {
        $obj = new self();
        $obj->pageName = (string)$pageName;
        $obj->queryArr = (array)$queryArr;
        $obj->fragment = (string)$fragment;
        $obj->getUrl();
        return $obj->url;
    }
    
    /**
     * @param string $redirect raw path without extension
     * @param array $queryArr
     * @param string $fragment
     */
    public static function redirect($redirect, $queryArr = array(), $fragment = '') {
        $redirect = self::buildUrl($redirect, $queryArr, $fragment);
        header("Location: " . $redirect);
        exit();
    }
    
    /**
     * redirect to referer (if defined) or to startpage
     * @param array $queryArr additional optional parameter
     */
    public static function redirectToReferer($queryArr = array()) {
        if (isset($_SESSION['referer'])) {
            $query = array_merge($_SESSION['referer']['query'], $queryArr);
            self::redirect($_SESSION['referer']['name'], $query, $_SESSION['referer']['fragment']);
        } else {
            self::redirect(CFG_STARTPAGE, $queryArr);
        }
    }
    
    /**
     * pseudo reload the page
     * @param array $queryArr additional parameters
     */
    public function reload($queryArr = array()) {
        $query = array_merge($_GET, $queryArr);
        self::redirect($this->pageName, $query);
    }
    
    /**
     * retrieve rules after submit from gg input definitions from session
     */
    public function retrieveRules() {
        if (isset($_SESSION['rules']) && is_array($_SESSION['rules'])) {
            $this->formRules = $_SESSION['rules'];
        } else {
            $this->formRules = null;
        }
        unset($_SESSION['rules']);
    }
    
    public static function ggSubmittedValue($ggInputName, $nameIndex) {
        $postValue = null;
        if (!is_null($this->inputName)) {
            preg_match('/^([^\[\]]+)((\[[^\[\]]+?\])*)((\[\])?)$/', $ggInputName, $match);
            $postFirstKey = $match[1];
            $postMiddleBrackets = explode('][', substr($match[2], 1, -1));
            $postLastBrackets = !!$match[4];
            if (isset($_POST[$postFirstKey])) {
                $postValue = $_POST[$postFirstKey];
                foreach ($postMiddleBrackets as $key) {
                    $postValue = (isset($postValue[$key])) ? $postValue[$key] : $postValue;
                }
                if ($nameIndex && $postLastBrackets) {
                    $postValue = $postValue[$nameIndex];
                }
            }
        }
        // todo: validate
        return $postValue;
    }
    
    /**
     * validate submit and set $this->ggpost
     */
    public function processPost() {
        $this->submittedFormId = form::getSubmittedFormId();
        if (!is_null($this->submittedFormId) && !is_null($this->formRules)) {
            checkCSRF();
            $currentRules = $this->formRules[$this->submittedFormId] ?? array();
            
//            foreach($currentRules as $inputName => $inputRules) {
//                
//            }
            
            foreach ($_POST as $inputName => $inputValue) {
                if ($inputName !== STOKEN_NAME && $inputName !== FORMIDENT_NAME) {
                    
                }
            }
        }
        
        $this->ggpost = $_POST;
    }
    
    /**
     * @param array $globalVar $_POST or $_GET or ...
     * @param string $key
     * @param array $ruleArr
     * @param mixed $defaultValue, if set request is optional
     * @return mixed
     */
    public static function getRequest($globalVar, $key, $ruleArr = null, $defaultValue = null) {
        if (is_null($ruleArr)) {
            $val = $globalVar[$key] ?? null;
        } else {
            // todo validation #################### !!!!!!!!!!!!1
            $val = $globalVar[$key] ?? null;
            // $val = self::cleanVar($globalVar, $key, $ruleArr);
        }
        if (is_null($val)) {
            if (is_null($defaultValue)) {
                die('class gg\request: required request variable missing');
            } else {
                $val = $defaultValue;
            }
        }
        return $val;
    }
}