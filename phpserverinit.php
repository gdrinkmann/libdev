<?php

// for php-webserver in dev environment you can type in cli
// cd projectname
// php -S localhost:8000 phpserverinit.php

$gg_requesturipathinfoextension = strtolower(pathinfo(
        parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH)
        )['extension'] ?? 'html');
if ($gg_requesturipathinfoextension === 'html') {
    include_once 'index.php';
} else {
    return false;
}
