<?php
namespace gg\lib;

const FORMIDENT_NAME = 'formIdent';
const STOKEN_NAME = 'stoken';

const FORMITEM_CLASS = 'formItem_BS5';

/**
 * ggLib CORE
 * defines a form
 * doc in formLayout.pdf
 * 
 * standard included in index.php
 * 
 * class gg\lib\form
  * @author Gerd
 */
class form {
    
    public $formId; // must be unique within the whole project
    
    private $formItems = array(); // array of formItem objects
    private $nameIndexes = array();
    private $itemPointer = 0;
    private $attrArray = array();
    
    /**
     * @param string $formId projectwide unique form ID
     */
    public function __construct($formId) {
        require_once CFG_LIBDIR . 'formItem.php'; // basic form item class
        require_once CFG_LIBDIR . FORMITEM_CLASS . '.php';
        
        $this->formId = $formId;
        
        // $this->addHtml('    <!-- Do not communicate this stoken number! -->');
        $this->addHidden(STOKEN_NAME, getSToken());
        $this->addHidden(FORMIDENT_NAME, $formId);

        $this->setAttributes();
    }
    
    /**
     * @param array $attrArray
     */
    public function setAttributes($attrArray = array()) {
        $defaultArray = array(
            'action' => request::pathCombine(CFG_BASEURI, $_SERVER['REQUEST_URI']),
            '_actionRaw' => '',
            '_queryArr' => array(),
            '_fragment' => '',
            'method' => 'post',
            'class' => 'gg-dovalidate',
            'enctype' => 'multipart/form-data',
            'novalidate' => 'novalidate'
        );
        $aA = array_merge($defaultArray, $attrArray);
        if ($aA['_actionRaw']) {
            $aA['action'] = request::buildUrl($aA['_actionRaw'], $aA['_queryArr'], $aA['_fragment']);
        }
        unset($aA['_actionRaw'], $aA['_queryArr'], $aA['_fragment']);
        $this->attrArray = $aA;
    }
    
    /**
     * @return null|string
     */
    public static function getSubmittedFormId() {
        return $_POST[FORMIDENT_NAME] ?? null;
    }
    
    /**
     * @param string $formId
     * @return bool
     */
    public function isSubmitted($formId = null) {
        $formId = $formId ?? $this->formId;
        return (self::getSubmittedFormId() === $formId);
    }
    
    /**
     * uses class rule for validation of submitted post variables
     * @param string $formId
     * @return boolean
     */
    public function isValidSubmit($formId = null) {
        if ($this->isSubmitted($formId)) {
            
            // todo fill item->invalidIndex and item->ruleResult for all items
            
            return true;
        } else {
            return false;
        }
    }

    /**
     * general method for adding a gg form item
     * is used by add<itemtype> shorthands
     * @param string $itemType html,hidden,fieldset,text,password,color,date,email,number,range,tel,time,url,textarea,select,discreteselect,file,image,submit,button
     * @param mixed $initValue
     * @param string $inputName "xy" "xy[]" "xy[ab]" "xy[ab][]"
     * @param string $label
     * @param string $help
     * @param array $rules  [[ruleName, invalidtext, [option1 => val, ...]], ...] see rule.php
     * @param array $options
     * @param array $attributes
     */
    private function defineItem($itemType, $initValue = null, $inputName = null, $label = null, $help = null, $rules = null, $options = null, $attributes = null) {
        $formItemClass = '\\' . __NAMESPACE__ . '\\' . FORMITEM_CLASS;
        $newItem = new $formItemClass;
        if ($inputName) {
            if (!isset($this->nameIndexes[$inputName])) {
                $this->nameIndexes[$inputName] = 0;
            }
            $newItem->nameIndex = $this->nameIndexes[$inputName]++;
            $newItem->inputId = 'gg-' . $this->formId . '-' . $inputName;
            $newItem->inputName = $inputName;
        }
        $newItem->formId = $this->formId;
        $newItem->itemType = strtolower($itemType);
        $newItem->initValue = $initValue;
        $newItem->label = $label;
        $newItem->help = $help;
        $newItem->rules = (array)$rules;
        $newItem->options = (array)$options;
        $newItem->attributes = (array)$attributes;
        
        $this->formItems[] = $newItem;
    }
    
    // ##### shorthands for defineItem(itemType) #####
    /**
     * @param string $html
     */
    public function addHtml($html) {
        $this->defineItem('html', null, null, $html);
    }
    /**
     * add <hr>
     */
    public function addHr() {
        $this->defineItem('html', null, null, '<hr>');
    }
    /**
     * @param string $legend
     * @param array $attributes
     */
    public function addFieldsetOpen($legend, $attributes = null) {
        $this->defineItem('fieldset', null, null, $legend, null, null, null, $attributes);
    }
    /**
     * close fieldset
     */
    public function addFieldsetClose() {
        $label = '</fieldset>'."\n";
        $label .= '<!-- - - - - - gg FIELDSET END - - - - - -->';
        $this->defineItem('html', null, null, $label);
    }
    /**
     * @param string $inputName
     * @param string $initValue
     * @param array $rules
     * @param array $attributes
     */
    public function addHidden($inputName, $initValue, $attributes = null) {
        $this->defineItem('hidden', $initValue, $inputName, null, null, null, null, $attributes);
    }
    /**
     * @param string $inputName
     * @param string $initValue
     * @param array $rules
     * @param string $label
     * @param string $help
     * @param array $attributes
     */
    public function addText($inputName, $initValue, $rules, $label = '', $help = null, $attributes = null) {
        $this->defineItem('text', $initValue, $inputName, $label, $help, $rules, null, $attributes);
    }
    /**
     * @param string $inputName
     * @param string $initValue
     * @param array $rules
     * @param string $label
     * @param string $help
     * @param array $attributes
     */
    public function addTextarea($inputName, $initValue, $rules, $label = '', $help = null, $attributes = null) {
        $this->defineItem('textarea', $initValue, $inputName, $label, $help, $rules, null, $attributes);
    }
    /**
     * @param string $inputName
     * @param string $initValue
     * @param array $rules
     * @param string $label
     * @param string $help
     * @param array $attributes
     */
    public function addPassword($inputName, $initValue, $rules, $label = '', $help = null, $attributes = null) {
        $this->defineItem('password', $initValue, $inputName, $label, $help, $rules, null, $attributes);
    }
    /**
     * @param string $inputName
     * @param string $initValue
     * @param array $rules
     * @param string $label
     * @param string $help
     * @param array $attributes
     */
    public function addColor($inputName, $initValue, $rules, $label = '', $help = null, $attributes = null) {
        $this->defineItem('color', $initValue, $inputName, $label, $help, $rules, null, $attributes);
    }
    /**
     * @param string $inputName
     * @param string $initValue
     * @param array $rules
     * @param string $label
     * @param string $help
     * @param array $attributes
     */
    public function addDate($inputName, $initValue, $rules, $label = '', $help = null, $attributes = null) {
        $this->defineItem('date', $initValue, $inputName, $label, $help, $rules, null, $attributes);
    }
    /**
     * @param string $inputName
     * @param string $initValue
     * @param array $rules
     * @param string $label
     * @param string $help
     * @param array $attributes
     */
    public function addEmail($inputName, $initValue, $rules, $label = '', $help = null, $attributes = null) {
        $this->defineItem('email', $initValue, $inputName, $label, $help, $rules, null, $attributes);
    }
    /**
     * @param string $inputName
     * @param string $initValue
     * @param array $rules
     * @param string $label
     * @param string $help
     * @param array $attributes
     */
    public function addTel($inputName, $initValue, $rules, $label = '', $help = null, $attributes = null) {
        $this->defineItem('tel', $initValue, $inputName, $label, $help, $rules, null, $attributes);
    }
    /**
     * @param string $inputName
     * @param string $initValue
     * @param array $rules
     * @param string $label
     * @param string $help
     * @param array $attributes
     */
    public function addTime($inputName, $initValue, $rules, $label = '', $help = null, $attributes = null) {
        $this->defineItem('time', $initValue, $inputName, $label, $help, $rules, null, $attributes);
    }
    /**
     * @param string $inputName
     * @param string $initValue
     * @param array $rules
     * @param string $label
     * @param string $help
     * @param array $attributes
     */
    public function addUrl($inputName, $initValue, $rules, $label = '', $help = null, $attributes = null) {
        $this->defineItem('url', $initValue, $inputName, $label, $help, $rules, null, $attributes);
    }
    /**
     * @param string $inputName
     * @param int|float $initValue
     * @param array $rules
     * @param string $label
     * @param string $help
     * @param array $attributes
     */
    public function addNumber($inputName, $initValue, $rules, $label = '', $help = null, $attributes = null) {
        $this->defineItem('number', $initValue, $inputName, $label, $help, $rules, null, $attributes);
    }
    /**
     * @param string $inputName
     * @param int|float $initValue
     * @param int|float $min
     * @param int|float $max
     * @param int|float $step
     * @param array $rules
     * @param string $label
     * @param string $help
     * @param array $attributes
     */
    public function addRange($inputName, $initValue = 5, $min = 0, $max = 10, $step= 1, $rules = [], $label = '', $help = null, $attributes = null) {
        $rules[] = ['limit', ['initvalue'=>(float)$initValue, 'min'=>(float)$min, 'max'=>(float)$max, 'step'=>(float)$step]];
        $this->defineItem('range', $initValue, $inputName, $label, $help, $rules, null, $attributes);
    }
    /**
     * @param string $inputName
     * @param array $optiontextValues
     * @param mixed $selectedValues if is_array item is multiple
     * @param array $rules
     * @param string $label
     * @param string $help
     * @param array $attributes
     * @param bool $discrete true for checkboxes or radiobuttons
     */
    public function addSelect($inputName, $optiontextValues, $selectedValues, $rules = null, $label = '', $help = null, $attributes = null, $discrete = false) {
        if ($discrete) {
            $selectType = 'discreteselect';
        } else {
            $selectType = 'select';
            if (is_array($selectedValues)) {
                $attributes = array_merge((array)$attributes, array('multiple' => 'multiple'));
                if (!isset($attributes['size'])) {
                    $attributes = array_merge((array)$attributes, array('size' => max(1, min(20, count($optiontextValues)))));
                }
            }
        }
        $this->defineItem($selectType, $selectedValues, $inputName, $label, $help, $rules, $optiontextValues, $attributes);
    }
    /**
     * @param string $inputName
     * @param array $labelValues
     * @param mixed $checkedValues if is_array checkboxes otherwise radiobuttons
     * @param array $rules
     * @param string $legend (used in Layout Codes C / D)
     * @param string $help
     * @param array $attributes
     */
    public function addDiscreteSelect($inputName, $labelValues, $checkedValues, $rules = null, $legend = '', $help = null, $attributes = null) {
        $this->addSelect($inputName, $labelValues, $checkedValues, $rules, $legend, $help, $attributes, true);
    }
    /**
     * @param string $inputName
     * @param array $rules
     * @param string $label
     * @param string $help
     * @param array $attributes
     */
    public function addUpload($inputName, $rules, $label = '', $help = null, $attributes = null) {
        $this->defineItem('file', null, $inputName, $label, $help, $rules, null, $attributes);
    }
    /**
     * @param string $imageSource
     * @param string $alternativeTxt
     * @param array $attributes
     */
    public function addSubmitImage($imageSource, $alternativeTxt, $attributes = null) {
        $attributes = array_merge((array)$attributes, array('src' => $imageSource, 'alt' => $alternativeTxt));
        $this->defineItem('image', null, 'submitimage', null, null, null, null, $attributes);
    }
    /**
     * @param string $buttonLabel
     * @param string $secondButtonLabel
     * @param string $class
     * @param string $secondClass
     * @param array $attributes
     */
    public function addSubmitButton($buttonLabel, $secondButtonLabel = null, $class = 'btn-primary', $secondClass = 'btn-secondary', $attributes = null) {
        $options = null;
        if ($secondButtonLabel) {
            $options = array('secondButton' => $secondButtonLabel, 'secondClass' => $secondClass);
        }
        if ($class) {
            $attributes = array_merge((array)$attributes, array('class' => $class));
        }
        $this->defineItem('submit', null, 'submitbutton', $buttonLabel, null, null, $options, $attributes);
    }
    /**
     * @param string $buttonLabel
     * @param string $class
     * @param array $attributes
     */
    public function addButton($buttonLabel, $class = 'btn-dark', $attributes = null) {
        if ($class) {
            $attributes = array_merge((array)$attributes, array('class' => $class));
        }
        $this->defineItem('button', null, 'button', $buttonLabel, null, null, null, $attributes);
    }
    // ##### shorthands end #####
    
    /**
     * get item HTML for use in view
     * @param string $currentLayout see formLayout.pdf (letter only)
     * @param int $grid
     * @return string
     */
    public function getNextItemHtml($currentLayout, $grid) {
        if (isset($this->formItems[$this->itemPointer])) {
            $isSubmitted = $this->isSubmitted();
            $itemObj = $this->formItems[$this->itemPointer];
            $this->itemPointer++;
            
            return $itemObj->get($isSubmitted, $currentLayout, $grid);
        }
        die('class gg\form: next item fail');
    }
    
    /**
     * get the whole form HTML for use in view
     * @param string $layoutCode see formLayout.pdf (letter and optional digits for grid, e.g. E4_5)
     * @param bool $floatLabel
     */
    public function getTotalHtml($layoutCode = 'A', $floatLabel = false) {
        $noGridTypes = array(   'E' => array('html', 'fieldset', 'hidden', 'file', 'image', 'submit', 'button'),
                                'F' => array('html', 'fieldset', 'hidden', 'file', 'image', 'submit', 'button', 'discreteselect'));
        
        if (!preg_match('/^(R|A|B|[CD]\d|[EF]\d(_\d)*)$/', $layoutCode)) {
            die('class gg\form: layoutCode fail');
        }
        $currentLayout = substr($layoutCode, 0, 1);
        $isGridLayout = isset($noGridTypes[$currentLayout]);
        $currentLayoutgridArray = explode('_', substr($layoutCode, 1));
        if ($isGridLayout) {
            $restGrid = 12 - array_sum($currentLayoutgridArray);
            if ($restGrid) {
                $currentLayoutgridArray[] = $restGrid;
            }
        }
        
        $isSubmitted = $this->isSubmitted();
        
        $attrStr = '';
        foreach ($this->attrArray as $attr => $attrVal) {
            $attrStr .= ' ' . $attr . '="' . $attrVal . '"';
        }
        
        $formHtml = "\n".'<!-- - - - - - gg FORM BLOCK START id "' . $this->formId . '" - - - - - -->'."\n";
        $formHtml .= '<form id="ggform-' . $this->formId . '"' . $attrStr . '>'."\n";
        
        $currentColPointer = 0;
        foreach ($this->formItems as $formItem) {
            
            if ($isGridLayout) {
                $noGridItem = in_array($formItem->itemType, $noGridTypes[$currentLayout]);
                if ($currentColPointer === 0 && !$noGridItem) {
                    $formHtml .= '<div class="row">'."\n";
                    
                } elseif ($currentColPointer > 0 && $noGridItem) {
                    $formHtml .= '</div>'."\n";
                    $currentColPointer = 0;
                }
            }
            
            $formHtml .= $formItem->get($isSubmitted, $currentLayout, (int)$currentLayoutgridArray[$currentColPointer++], $floatLabel);
            
            if ($isGridLayout) {
                if ($currentColPointer >= count($currentLayoutgridArray) && !$noGridItem) {
                    $formHtml .= '</div>'."\n";
                    
                } elseif ($noGridItem) {
                    $currentColPointer = 0;
                }
            }
            
            if ($currentColPointer >= count($currentLayoutgridArray)) {
                $currentColPointer = 0;
            }
        }
            
        $formHtml .= '</form>'."\n".'<!-- - - - - - gg FORM BLOCK END - - - - - -->'."\n\n";
        
        echo $formHtml;
    }
    
    /**
     * @param string $globalVar $_POST or $_GET
     * @param string $key
     * @param int|string $dataType constant or regex
     * @return null|string|array
     */
//    public static function cleanVar($globalVar, $key, $dataType) {
//        if (isset($globalVar[$key])) {
//            $var = (array)$globalVar[$key];
//            array_walk_recursive($var, function(&$item, $key, $dataType) {
//                switch($dataType) {
//                    case \gg\DATATYPE_BOOL:
//                        $item = (bool)$item;
//                        break;
//                    case \gg\DATATYPE_INT:
//                        $item = (int)$item;
//                        break;
//                    case \gg\DATATYPE_FLOAT:
//                        $item = (float)$item;
//                        break;
//                    case \gg\DATATYPE_TXT:
//                        $item = strip_tags((string)$item);
//                        break;
//                    case \gg\DATATYPE_RAWTXT:
//                        $item = (string)$item;
//                        break;
//                    case \gg\DATATYPE_JSON:
//                        // not correct but sufficient in most cases
//                        if (strpos($item, '[') !== 0 && strpos($item, '{') !== 0) {
//                            $item = '';
//                        }
//                        break;
//                    default:
//                        if (preg_match('/^(\/|#).+(\/|#)$/', $dataType)) {
//                            if (!preg_match($dataType, $item)) {
//                                $item = null;
//                            }
//                        }
//                }
//            }, $dataType);
//            if (!is_array($globalVar[$key])) {
//                $var = current($var);
//            }
//            return $var;
//        } else {
//            return null;
//        }
//    }
    
    /**
     * @param array $globalVar $_POST or $_GET or ...
     * @param string $key
     * @param int|string $dataType
     * @param mixed $defaultValue if set request is optional
     * @return mixed
     */
//    public static function getRequest($globalVar, $key, $dataType = null, $defaultValue = null) {
//        if (is_null($dataType)) {
//            $val = $globalVar[$key] ?? null;
//        } else {
//            $val = self::cleanVar($globalVar, $key, $dataType);
//        }
//        if (is_null($val)) {
//            if (is_null($defaultValue)) {
//                die('class gg\form: required request variable missing');
//            } else {
//                $val = $defaultValue;
//            }
//        }
//        return $val;
//    }
    
    /**
     * @param string $key
     * @param int|string $dataType
     * @param mixed $defaultValue if set request is optional
     * @return mixed
     */
//    public static function getPost($key, $dataType = null, $defaultValue = null) {
//        return self::getRequest($_POST, $key, $dataType, $defaultValue);
//    }
    /**
     * @param string $key
     * @param int|string $dataType
     * @param mixed $defaultValue if set request is optional
     * @return mixed
     */
//    public static function getGet($key, $dataType, $defaultValue = null) {
//        return self::getRequest($_GET, $key, $dataType, $defaultValue);
//    }
    
    /**
     * clean $_POST
     */
//    public static function cleanPost() {
//        $submittedFormID = self::getSubmittedFormId();
//        if (!is_null($submittedFormID) && isset($_SESSION['postTypes'][$submittedFormID])) {
//            $posttmp = array();
//            foreach ($_POST as $postkey => $postvalue) {
//                $datatype = $_SESSION['postTypes'][$submittedFormID][$postkey] ?? null;
//                if ($datatype) {
//                    $posttmp[$postkey] = self::cleanVar($_POST, $postkey, $datatype);
//                } else {
//                    $posttmp[$postkey] = $postvalue;
//                }
//            }
//            $_POST = $posttmp;
//            checkCSRF();
//        }
//    }
}


