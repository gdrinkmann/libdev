<?php
namespace gg\lib;

use \PHPMailer\PHPMailer\PHPMailer;
use \PHPMailer\PHPMailer\Exception;

/**
 * ggLib CORE
 * wrapper for PHPMailer
 * https://github.com/PHPMailer/PHPMailer
 * (hint: in local dev environment use https://github.com/Nilhcem/FakeSMTP
 *   with 'host.docker.internal' or 'localhost')
 * 
 * class gg\lib\email
 * @author Gerd
 */
class email {

    private $filesToDel = array();
    private $debugLog = '';
    private $logTo = array();
    private $logCC = array();
    private $logBCC = array();
    private $logRepl = array();
    private $logAttach = array();
    private $logDestination = array(false, false);
    private $logCategory = '';

    /**
     * @var $mailObj for extern handling with PHPMailer methods
     */
    public $mailObj;

    public function __construct() {
                
        $this->mailObj = new PHPMailer(true);
        $this->mailObj->isSMTP();
        $this->mailObj->Host = CFG_SMTPSERVER;
        $this->mailObj->Port = CFG_SMTPPORT;
        $this->mailObj->SMTPAuth = true;
        $this->mailObj->Username = CFG_SMTPUSER;
        $this->mailObj->Password = CFG_SMTPPW;
        $this->mailObj->SMTPAutoTLS = true;
        $this->mailObj->Timeout = 10;
        $this->mailObj->SetLanguage(str::getLang());
        
        $res = $this->parseAddress(CFG_SMTPFROM);
        $this->mailObj->setFrom($res['address'], $res['name']);
    }
    
    /**
     * @param bool $toFile
     * @param bool $toDB
     * @param string $category
     */
    public function setLog($toFile, $toDB, $category = 'email') {
        $this->logDestination = array($toFile, $toDB);
        $this->logCategory = $category;
    }

    private function parseAddress($address) {
        $ret = array('name' => '', 'address' => '');
        if (is_array($address)) {
            if (isset($address['name']) && isset($address['address'])) {
                $ret = $address;
            }
        } elseif (preg_match('/^(.+)<(.+)>/', $address, $matches)) {
            $ret['name'] = trim(str_replace(['"', "'"], '', $matches[1]));
            $ret['address'] = trim($matches[2]);
        } else {
            $ret['name'] = '';
            $ret['address'] = trim($address);
        }
        return $ret;
    }

    private function parseAddresses($addresses) {
        $retArray = array();
        if (!is_array($addresses)) {
            $addresses = preg_split('/[,;]+/', $addresses);
        }
        foreach ($addresses as $address) {
            $retArray[] = $this->parseAddress($address);
        }
        return $retArray;
    }

    /**
     * @param string|array $to
     */
    public function addTo($to) {
        $resArr = $this->parseAddresses($to);
        foreach ($resArr as $res) {
            $this->mailObj->addAddress($res['address'], $res['name']);
            $this->logTo[] = $res['address'];
        }
    }

    /**
     * @param string|array $cc
     */
    public function addCC($cc) {
        $resArr = $this->parseAddresses($cc);
        foreach ($resArr as $res) {
            $this->mailObj->addCC($res['address'], $res['name']);
            $this->logCC[] = $res['address'];
        }
    }

    /**
     * @param string|array $bcc
     */
    public function addBCC($bcc) {
        $resArr = $this->parseAddresses($bcc);
        foreach ($resArr as $res) {
            $this->mailObj->addBCC($res['address'], $res['name']);
            $this->logBCC[] = $res['address'];
        }
    }

    /**
     * @param string|array $replyTo
     */
    public function addReplyTo($replyTo) {
        $resArr = $this->parseAddresses($replyTo);
        foreach ($resArr as $res) {
            $this->mailObj->AddReplyTo($res['address'], $res['name']);
            $this->logRepl[] = $res['address'];
        }
    }

    /**
     * @param string $path fullpath or string content
     * @param string $filename
     * @param bool $delAfter
     */
    public function addAttachment($path, $filename, $delAfter = false) {
        if (is_file($path)) {
            $this->mailObj->AddAttachment($path, $filename);
            $this->logAttach[] = $filename . '=>' . $path;
            if ($delAfter) {
                $this->filesToDel[] = $path;
            }
        } elseif (trim($path)) {
            $this->mailObj->AddStringAttachment($path, $filename);
            $this->logAttach[] = $filename . '=>string content';
        }
    }
    
    /**
     * set ICS data (build by https://github.com/markuspoerschke/iCal )
     * @param string $ics
     */
    public function setIcal($ics) {
        $this->mailObj->Ical = $ics;
    }

    /**
     * main method
     * @param string $subject
     * @param string $body
     * @param bool $isHTML
     * @param int $wordwrap 0 = no wrap, standard is 78
     * @param bool $debugToLog
     * @return bool
     */
    public function send($subject, $body, $isHTML = false, $wordwrap = 0, $debugToLog = false) {
        $this->mailObj->Subject = $subject;
        $sig = '';
        if (CFG_SYSSIGNATURE) {
            if ($isHTML) {
                $sig = '<br><hr>' . str_replace("\r\n", '<br>', CFG_SYSSIGNATURE);
            } else {
                $sig = "\r\n---\r\n" . CFG_SYSSIGNATURE;
            }
        }
        $this->mailObj->Body = $body . $sig;
        $this->mailObj->WordWrap = $wordwrap;
        if ($isHTML) {
            $this->mailObj->MsgHTML($this->mailObj->Body);
        }
        $sent = $this->mailit($debugToLog);
        $this->logmail($sent, $subject, $body);
        return $sent;
    }
    
    private function mailit($debugToLog) {
        if ($debugToLog || CFG_DEVMODUS) {
            $this->mailObj->SMTPDebug = SMTP::DEBUG_SERVER;
            $this->mailObj->Debugoutput = 'echo';
            ob_start();
        }
        $ret = ($this->mailObj->send() && true);
        if ($debugToLog || CFG_DEVMODUS) {
            $this->debugLog = ob_get_clean();
        }
        foreach ($this->filesToDel as $file) {
            @unlink($file);
        }
        return $ret;
    }
    
    private function logmail($sent, $subject, $body) {
        if ($this->logCategory) {
            $logstr = '';
            if (!$sent) {
                $logstr .= 'Email sent failed: ' . $this->getInfo() . "\n";
            }
            if (!empty($this->logRepl)) {
                $logstr .= 'Reply: ' . implode(', ', $this->logRepl) . "\n";
            }
            if (!empty($this->logAttach)) {
                $logstr .= 'Attachments: ' . implode(', ', $this->logAttach) . "\n";
            }
            $logstr .= 'Subject: ' . $subject . "\n";
            $logstr .= 'Body: ' . $body . "\n";
            
            $logObj = new log($this->logDestination[0], $this->logDestination[1], false);
            $logObj->addUserId();
            $logObj->write($this->logCategory, $logstr, implode(',', $this->logTo), implode(',', $this->logCC), implode(',', $this->logBCC));
        }
    }

    /**
     * @param bool $getLog 
     * @return string
     */
    public function getInfo($getLog = false) {
        if ($getLog) {
            return $this->debugLog;
        } else {
            return $this->mailObj->ErrorInfo;
        }
    }

}

/**
 * child mail class for saving mails in mail queue
 * cron job required to process queue
 */
class emailToQueue extends email {
    
    private function mailit($dummy) {
        
        return \gg\queue::insertItem('email', $this);
    }
}
