<?php
namespace gg;
/**
 * ggLib CORE
 * ajax clientside validation controller for gglib inputs
 * 
 * ajax query data array/object:
 * { inputName : { 'value' : inputValue(string|array),
 *                 'rulesArray'  : [
 *                                    [ruleName,  { optionName : optionValue,
 *                                                  ...
 *                                                }
 *                                    ],
 *                                    ...
 *                                 ]
 *               },
 *   ...
 * }
 * 
 * response array/object:
 * { inputName : { 'invalid' : false|integer,
 *                 'ruleresult' : result,
 *                 'error' : bool
 *               },
 *   ...
 * }
 * 
 */
$inputJson = _get('rulesjson', [['json']]);
$inputArray = (array)json_decode($inputJson, true);

$ruleObj = new lib\rule();

$responseArray = array();
foreach ($inputArray as $inputName => $inputRules) {
    $value = $inputRules['value'];
    $rules = $inputRules['rulesArray'];
    
    $responseArray[$inputName] = $ruleObj->_validate($value, $rules);
}

echo json_encode($responseArray, JSON_FORCE_OBJECT);

// send header and buffer and end script
header("Content-Type: application/json");
ob_end_flush();

exit();
