<?php
namespace gg;
/**
 * ggLib sample
 * Bootstrap 5
 */
?>
<h5 class="card-header">
    <?= _t('head_register', 'user') ?>
</h5>
<div class="card-body">
    <p class="card-text"><?= _t('txt_register', 'user') ?></p>
<?= $PAGE->form()->getTotalHtml('C3') ?>
    <hr>
    <p class="card-text">
        <small><a href="/core/login.html" class="card-link"><i class="bi bi-person-fill"></i> <?= _t('lnk_login', 'user') ?></a></small>
        <br>
        <small><a href="/core/forgotpassword.html" class="card-link"><i class="bi bi-question-circle-fill"></i> <?= _t('lnk_forgotpassword', 'user') ?></a></small>
    </p>
</div>
