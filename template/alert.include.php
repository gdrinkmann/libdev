<?php
namespace gg;
/**
 * ggLib sample
 * Bootstrap 5
 */
?>
<?php foreach (lib\page::getMessages() as $msg): ?>
<!-- START alert.include  -->
            <div class="alert alert-dismissible mb-2 fade show <?= $msg['prop'] ?>">
                <strong class="l-error-header"></strong>
                <p class="mb-0"><?= $msg['text'] ?></p>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
<!-- END alert.include  -->
<?php endforeach; ?>
