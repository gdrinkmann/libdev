<?php
namespace gg;
/**
 * ggLib sample
 * Bootstrap 5
 */
?>
<h5 class="card-header d-flex">
    <?= _t('head_login', 'user') ?> <a href="<?= lib\CFG_BASEURI ?>" class="card-link ms-auto"><i class="bi bi-house-door-fill"></i></a>
</h5>
<div class="card-body">
    <?= $PAGE->form()->getTotalHtml('A') ?>
    <hr>
    <p class="card-text">
        <small><a href="/core/register.html" class="card-link"><i class="bi bi-pencil-fill"></i> <?= _t('lnk_register', 'user') ?></a></small>
        <br>
        <small><a href="/core/forgotpassword.html" class="card-link"><i class="bi bi-question-circle-fill"></i> <?= _t('lnk_forgotpassword', 'user') ?></a></small>
    </p>
</div>
