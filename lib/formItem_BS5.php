<?php
namespace gg\lib;

/**
 * ggLib CORE
 * class gg\lib\formItem_BS5
 * defines a formItem, usually a html-input
 */
class formItem_BS5 extends formItem {
    
    /**
     * get the formItem HTML (here Bootstrap 5)
     * https://getbootstrap.com/docs/5.0/forms/overview/
     * @param bool $isSubmitted
     * @param string $layoutCode see formLayout.pdf
     * @param int $layoutGrid
     * @return string HTML
     */
    public function get($isSubmitted, $layoutCode, $layoutGrid, $floatLabel) {
        
        $this->normalizeRules();
        $this->normalizeItem();
        $this->normalizeName();
        $this->storeRules();
        
        $rulesStr = $this->rulesToStr();
        $rulesArr = $this->rules;
        
        $val = $this->initValue;
        if ($isSubmitted) {
            $val = $this->getPost();
        }
        $invalidIndex = $this->invalidIndex;
        $ruleResult = $this->ruleResult;
        
        $type = $this->itemType;
        $id = $this->inputId;
        $name = $this->inputName;
        $label = $this->label;
        $help = $this->help;
        $opts = $this->options;
        
        $wrapClass = '';
        if ($floatLabel) {
            if ($type !== 'select' || !is_array($val)) {
                $wrapClass = ' form-floating';
                if (!isset($this->attributes['placeholder']) && $type !== 'select') {
                    $this->attributes['placeholder'] = 'dummyplaceholder';
                }
            } else {
                $floatLabel = false;
            }
        }
        $class = '';
        if (isset($this->attributes['class'])) {
            $class = ' ' . $this->attributes['class'];
        }
        $attr = '';
        foreach ($this->attributes as $k => $v) {
            if ($k !== 'class') {
                $attr .= ' ' . $k . '="' . $v . '"';
            }
        }
        
        $hideHelp = $invalidIndex !== false && $rulesArr[(int)$invalidIndex][1];
        $infotxt = ($help) ? '<small class="form-text text-muted ms-1'.($hideHelp?' gg-hide':'').'">' . $help . '</small>'."\n" : '';
        foreach ($rulesArr as $index => $rule) {
            $hide = ($index === $invalidIndex) ? '' : ' gg-hide';
            if ($rule[1]) {
                $infotxt .= '<div class="invalid-feedback ms-1' . $hide . '" data-ruleref="' . $index . '">' . $rule[1] . '</div>'."\n";
            }
        }
        
        $invalidclass = ($invalidIndex !== false) ? ' is-invalid' : '';
        
        switch ($type) {
            case 'html':
                $label = str_replace('{grid}', $layoutGrid, $label);
                $label = str_replace('{gridrest}', 12 - $layoutGrid, $label);
                $html = $label."\n";
                break;
            case 'fieldset':
                $html = "\n".'<!-- - - - - - gg FIELDSET START - - - - - -->'."\n";
                $html .= '<fieldset class="' . $class . '"' . $attr . '>'."\n";
                $html .= '<legend>' . $label . '</legend>'."\n";
                break;
            case 'hidden':
                $html = '    <input name="' . $name . '" type="hidden" id="' . $id . '" class="' . $class . '"' . $attr . ' value="' . $val . '">'."\n";
                break;
            case 'text':
            case 'password':
            case 'color': // no IE 11
            case 'date': // no IE 11
            case 'datetime-local': // no FF
            case 'email':
            case 'month': // no FF
            case 'number':
            case 'range':
            case 'search':
            case 'tel':
            case 'time': // no IE 12
            case 'url':
            case 'week': // no FF
                if ($type == 'range') {
                    $inputclass = 'form-range';
                    $pclass = ' pt-0';
                } else {
                    $inputclass = 'form-control';
                    $pclass = '';
                }
                $labelHtml = $label ? '    <label class="form-label" for="' . $id . '">' . $label . '</label>'."\n" : '';
                if ($type == 'password') {
                    $input = '<div class="input-group' . $invalidclass . '">'."\n";
                    $input .= $floatLabel ? '    <div class="form-floating gg-form-floating-group flex-grow-1">'."\n" : '';
                    $input .= '    <input name="' . $name . '" type="password" id="' . $id . '" class="form-control' . $invalidclass . $class . '"' . $attr . ' value="" autocomplete="off">'."\n";
                    $input .= $floatLabel ? $labelHtml : '';
                    $input .= $floatLabel ? '    </div>'."\n" : '';
                    $input .= '    <button class="btn btn-outline-secondary gg-togglepw" type="button"><i class="bi bi-eye"></i></button>'."\n";
                    $input .= '</div>'."\n";
                    $input .= $infotxt;
                    if ($floatLabel) {
                        $wrapClass = '';
                        $labelHtml = '';
                    }
                } else {
                    $input = '<input name="' . $name . '" type="' . $type . '" id="' . $id . '" class="' . $inputclass . $invalidclass . $class . '"' . $attr . ' value="' . $val . '">'."\n";
                    $input .= $infotxt;
                }
                if ($layoutCode == 'A' || $layoutCode == 'B') {
                    $html = '<div class="gg-input mb-3' . $wrapClass . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $floatLabel ? '' : $labelHtml;
                    $html .= $this->indent($input, 1);
                    $html .= $floatLabel ? $labelHtml : '';
                    $html .= '</div>'."\n";
                } elseif ($layoutCode == 'C' || $layoutCode == 'D') {
                    $html = '<div class="row mb-3">'."\n";
                    $html .= '    <label for="' . $id . '" class="col-sm-' . $layoutGrid . ' col-form-label' . $pclass .'">' . $label . '</label>'."\n";
                    $html .= '    <div class="gg-input col-sm-' . (12 - $layoutGrid) . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $this->indent($input, 2);
                    $html .= '    </div>'."\n";
                    $html .= '</div>'."\n";
                } elseif ($layoutCode == 'E' || $layoutCode == 'F') {
                    $html = '<div class="gg-input col-md-' . $layoutGrid . $wrapClass . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $floatLabel ? '' : $labelHtml;
                    $html .= $this->indent($input, 1);
                    $html .= $floatLabel ? $labelHtml : '';
                    $html .= '</div>'."\n";
                } else {
                    $html = $input;
                }
                $html = $this->indent($html, 1);
                break;
            case 'textarea':
                $textarea = '<textarea name="' . $name . '" id="' . $id . '" class="form-control' . $invalidclass . $class . '"' . $attr . '>' . $val . '</textarea>'."\n";
                $textarea .= $infotxt;
                $labelHtml = $label ? '    <label class="form-label" for="' . $id . '">' . $label . '</label>'."\n" : '';
                if ($layoutCode == 'A' || $layoutCode == 'B') {
                    $html = '<div class="gg-input mb-3' . $wrapClass . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $floatLabel ? '' : $labelHtml;
                    $html .= $this->indent($textarea, 1);
                    $html .= $floatLabel ? $labelHtml : '';
                    $html .= '</div>'."\n";
                } elseif ($layoutCode == 'C' || $layoutCode == 'D') {
                    $html = '<div class="row mb-3">'."\n";
                    $html .= '    <label for="' . $id . '" class="col-sm-' . $layoutGrid . ' col-form-label  pt-0">' . $label . '</label>'."\n";
                    $html .= '    <div class="gg-input col-sm-' . (12 - $layoutGrid) . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $this->indent($textarea, 2);
                    $html .= '    </div>'."\n";
                    $html .= '</div>'."\n";
                } elseif ($layoutCode == 'E' || $layoutCode == 'F') {
                    $html = '<div class="gg-input col-md-' . $layoutGrid . $wrapClass . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $floatLabel ? '' : $labelHtml;
                    $html .= $this->indent($textarea, 1);
                    $html .= $floatLabel ? $labelHtml : '';
                    $html .= '</div>'."\n";
                } else {
                    $html = $textarea;
                }
                $html = $this->indent($html, 1);
                break;
            case 'select':
                $val = (array)$val;
                $selectOptions = '';
                if (empty($val) && isset($this->attributes['placeholder'])) {
                    $selectOptions .= '    <option value="" hidden="hidden" selected="selected" disabled="disabled">' . $this->attributes['placeholder'] . '</option>'."\n";
                }
                $i = 0;
                foreach ($opts as $optiontext => $v) {
                    $selected = (in_array($v, $val)) ? ' selected="selected" ' : '';
                    $selectOptions .= '    <option id="' . $id . '-' . $i++ . '" value="' . $v . '"' . $selected . '>' . $optiontext . '</option>'."\n";
                }
                $select = '<select name="' . $name . '" id="' . $id . '" class="form-select' . $invalidclass . $class . '"' . $attr . '>'."\n";
                $select .= $selectOptions;
                $select .= '</select>'."\n";
                $select .= $infotxt;
                $labelHtml = $label ? '    <label class="form-label" for="' . $id . '">' . $label . '</label>'."\n" : '';
                if ($layoutCode == 'A' || $layoutCode == 'B') {
                    $html = '<div class="gg-input mb-3' . $wrapClass . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $floatLabel ? '' : $labelHtml;
                    $html .= $this->indent($select, 1);
                    $html .= $floatLabel ? $labelHtml : '';
                    $html .= '</div>'."\n";
                } elseif ($layoutCode == 'C' || $layoutCode == 'D') {
                    $html = '<div class="row mb-3">'."\n";
                    $html .= '    <label for="' . $id . '" class="col-sm-' . $layoutGrid . ' col-form-label">' . $label . '</label>'."\n";
                    $html .= '    <div class="gg-input col-sm-' . (12 - $layoutGrid) . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $this->indent($select, 2);
                    $html .= '    </div>'."\n";
                    $html .= '</div>'."\n";
                } elseif ($layoutCode == 'E' || $layoutCode == 'F') {
                    $html = '<div class="gg-input col-md-' . $layoutGrid . $wrapClass . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $floatLabel ? '' : $labelHtml;
                    $html .= $this->indent($select, 1);
                    $html .= $floatLabel ? $labelHtml : '';
                    $html .= '</div>'."\n";
                } else {
                    $html .= $select;
                }
                $html = $this->indent($html, 1);
                break;
            case 'discreteselect':
                if (is_array($val)) {
                    $controltype = 'checkbox';
                } else {
                    $controltype = 'radio';
                }
                if ($layoutCode == 'A' || $layoutCode == 'C') {
                    $controldiv = ' form-switch';
                } else {
                    $controldiv = '';
                }
                $val = (array)$val;
                $inlineclass = ($layoutCode == 'B' || $layoutCode == 'D' || $layoutCode == 'F') ? ' form-check-inline' : '';
                $controls = '';
                $i = 0;
                foreach ($opts as $lbl => $v) {
                    $checked = (in_array($v, $val)) ? ' checked="checked"' : '';
                    $controls .= '<div class="mb-3 form-check' . $controldiv . $invalidclass . $inlineclass . '">'."\n";
                    $controls .= '    <input name="' . $name . '" type="' . $controltype . '" id="' . $id . '-' . $i . '" class="form-check-input' . $invalidclass . $class . '"' . $attr . ' value="' . $v . '"' . $checked . '>'."\n";
                    $controls .= '    <label class="form-check-label" for="' . $id . '-' . $i++ . '">' . $lbl . '</label>'."\n";
                    $controls .= '</div>'."\n";
                }
                $controls .= $infotxt;
                if ($layoutCode == 'C' || $layoutCode == 'D') {
                    $html = '<fieldset>'."\n";
                    $html .= '    <div class="row">'."\n";
                    $html .= '        <legend class="col-form-label col-sm-' . $layoutGrid . ' pt-0">' . $label . '</legend>'."\n";
                    $html .= '        <div class="gg-input col-sm-' . (12 - $layoutGrid) . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $this->indent($controls, 3);
                    $html .= '        </div>'."\n";
                    $html .= '    </div>'."\n";
                    $html .= '</fieldset>'."\n";
                } elseif ($layoutCode == 'E') {
                    $html = '<div class="gg-input col-md-' . $layoutGrid . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $this->indent($controls, 1);
                    $html .= '</div>'."\n";
                } else {
                    $html = '<div class="gg-input" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $this->indent($controls, 1);
                    $html .= '</div>'."\n";
                }
                $html = $this->indent($html, 1);
                break;
            case 'file':
                $inputf = $label ? '<label class="form-label" for="' . $id . '">' . $label . '</label>'."\n" : '';
                $inputf .= '<input name="' . $name . '" type="file" id="' . $id . '" class="form-control' . $invalidclass . $class . '"' . $attr . '>'."\n";
                $inputf .= $infotxt;
                if ($layoutCode == 'C' || $layoutCode == 'D') {
                    $html = '<fieldset>'."\n";
                    $html .= '    <div class="row mb-3">'."\n";
                    $html .= '        <legend class="col-form-label col-sm-' . $layoutGrid . ' pt-0"></legend>'."\n";
                    $html .= '        <div class="gg-input col-sm-' . (12 - $layoutGrid) . '" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $this->indent($inputf, 3);
                    $html .= '        </div>'."\n";
                    $html .= '    </div>'."\n";
                    $html .= '</fieldset>'."\n";
                } else {
                    $html = '<div class="gg-input mb-3" data-rules="' . $rulesStr . '" data-ruleresult="' . $ruleResult . '">'."\n";
                    $html .= $this->indent($inputf, 1);
                    $html .= '</div>'."\n";
                }
                $html = $this->indent($html, 1);
                break;
            case 'image': // todo
                $html = '    <input type="image" id="' . $id . '"' . $attr . '>'; // in $_POST name_x and name_y
                break;
            case 'submit':
                $buttoni = '';
                if ($opts) {
                    $buttoni .= '<button type="button" id=' . $id . '-2nd' . '" class="btn ' . $opts['secondClass'] . '"'. $attr . '>' . $opts['secondButton'] . '</button>'."\n";
                }
                $buttoni .= '<button type="submit" id="'. $id . '" class="btn' . $class . '"' . $attr . '>' . $label . '</button>'."\n";
                if ($layoutCode == 'C' || $layoutCode == 'D') {
                    $html = '<fieldset>'."\n";
                    $html .= '    <div class="row mb-3">'."\n";
                    $html .= '        <legend class="col-form-label col-sm-' . $layoutGrid . ' pt-0"></legend>'."\n";
                    $html .= '        <div class="col-sm-' . (12 - $layoutGrid) . '">'."\n";
                    $html .= $this->indent($buttoni, 3);
                    $html .= '        </div>'."\n";
                    $html .= '    </div>'."\n";
                    $html .= '</fieldset>'."\n";
                } else {
                    $html = '<div class="mb-3">'."\n";
                    $html .= $this->indent($buttoni, 1);
                    $html .= '</div>'."\n";
                }
                break;
            case 'button':
                $buttoni = '<button type="button" id="'. $id . '" class="btn' . $class . '"' . $attr . '>' . $label . '</button>'."\n";
                if ($layoutCode == 'C' || $layoutCode == 'D') {
                    $html = '<fieldset>'."\n";
                    $html .= '    <div class="row">'."\n";
                    $html .= '        <legend class="col-form-label col-sm-' . $layoutGrid . ' pt-0"></legend>'."\n";
                    $html .= '        <div class="col-sm-' . (12 - $layoutGrid) . '">'."\n";
                    $html .= $this->indent($buttoni, 3);
                    $html .= '        </div>'."\n";
                    $html .= '    </div>'."\n";
                    $html .= '</fieldset>'."\n";
                } else {
                    $html = '<div>'."\n";
                    $html .= $this->indent($buttoni, 1);
                    $html .= '</div>'."\n";
                }
        }
        return $html;
    }
}


