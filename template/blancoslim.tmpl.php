<?php
namespace gg;
/**
 * ggLib sample
 * Bootstrap 5
 */
?>
<!doctype html>
<html lang="<?= _lang() ?>">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?= $PAGE->loadCSS() ?>
        <title><?= lib\CFG_PROJECTNAME . ': ' . $PAGE->getTitle() ?></title>
    </head>
    <body id="page_<?= $PAGE->getName() ?>">
        
        <div class="container ps-0 pe-0 mt-4">
            
<?php include 'alert.include.php' ?>
            
            <div class="row">
                <div class="col-3 d-none d-md-block"></div>

                <div class="col">
                    <div class="card bg-light mx-auto">
                        
                        <?= $PAGE->getViewContent() ?>
                    
                        <div class="card-footer text-muted">
                            <small><?= lib\CFG_PROJECTNAME ?> by Gerd (ggLib Version <?= getversion() ?>)</small>
                        </div>
                    </div>
                </div>
                    
                <div class="col-3 d-none d-md-block"></div>
            </div>
        </div>
        <?= $PAGE->loadJS() ?>
    </body>
</html>
