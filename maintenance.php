<?php
namespace gg;
/**
 * ggLib CORE
 * maintenance page sample
 * 
 * @author Gerd
 */

$GGLIB_CLIENTIP = filter_var(trim($_SERVER["REMOTE_ADDR"]), FILTER_VALIDATE_IP);

if (!in_array($GGLIB_CLIENTIP, (array)CFG_MAINTENANCEEXCEPTIONS)) {
    
    http_response_code(503);
    
    die(CFG_PROJECTNAME . ': Sorry! Interruption due to maintenance. We\'ll be back soon. (your IP '.$GGLIB_CLIENTIP.').');
}

